package jltk;


public class Mouse
{

    public Mouse()
    { 
    }

    /**
     * Return the current x-coordinate of the mouse cursor on the current window., no matter if the mouse is pressed or not.
     *
     * @return x-coordinate of the mouse cursor
     */
    public static int getX()
    {
        try
        {
            Thread.sleep(1);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        if (Window.topWindow != null)
            return (int) Window.topWindow.getMouseX();
        else
        {
            return warning();
        }
    }

    /**
     * Return the current y-coordinate of the mouse cursor on the current window., no matter if the mouse is pressed or not.
     *
     * @return y-coordinate of the mouse cursor
     */
    public static int getY()
    {
        try
        {
            Thread.sleep(1);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        if (Window.topWindow != null)
            return (int) Window.topWindow.getMouseY();
        else
        {
            return warning();
        }
    }

    /**
     * Returns true, if the mouse is pressed, just in the moment this method is called.
     *
     * @return true, if mouse is pressed.
     */
    public static boolean isPressed()
    {
        try
        {
            Thread.sleep(1);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        if (Window.topWindow  != null)
            return Window.topWindow.isMouseButtonDown;
        else
        {
            return warning() != -1;
        }
    }

    /**
     * Return true, if the last mouse click was a double click. 
     *
     * @return true, if the last click was a double click.
     */
    public static boolean wasDoubleclick()
    {
        try
        {
            Thread.sleep(1);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        if (Window.topWindow  != null)
            if (Window.topWindow.wasDoubleClick)
            {
                //Window.topWindow.wasDoubleClick = false;
                return true;
            }
            else{
                return false;
            }
        else
        {
            return warning() != -1;
        }
    }

    /**
     * Return the number of the mouse botton, that was pressed last. Mostly 1 is the left button, 2 the middle
     * button an 3 the right button. If no button was pressed, 0 is returned.
     *
     * @return The number of mouse button
     * */
    public static int button(){
        try
        {
            Thread.sleep(1);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        if (Window.topWindow  != null)
            return Window.topWindow.buttonNumber;
        else
        {
            return warning();
        }
    }

    private static int warning(){
        System.err.println("Mouse without Window");
        return -1;
    }

    
    
}
