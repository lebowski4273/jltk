package jltk.gui;
import jltk.*;

/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

/**
 * The Button class represents a button in a gui. A button can be clicked and you can specify a method 
 * which should be invoke, if the button is clicked.
 * 
 * events: clicked
 * 
 * @author M.Schulte    
 * @version 17.05.2022
 */
public class Button extends Labeled
{
    protected javafx.scene.control.Button fxButton;
    private String onButtonClicked ="";
    
    /**
     * Creates the Button
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the button
     * @param pHeight The height of the button
     * @param pText The label of the button
     */
    public Button(double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pText); //default/first window 
    }

    /**
     * Creates the Button
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the button
     * @param pHeight The height of the button
     * @param pWindow The window, the button should be placed
     * @param pText The label of the button
     */
    public Button(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this.fxButton = new javafx.scene.control.Button();
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxButton);
        this.fxButton.setOnAction((event) -> {
                this.runMethodByName(this.onButtonClicked);
            });
        this.setText(pText);
    }
    
    
    /**
     * Set the name of the method, which will be invoked,
     * if the button is clicked.
     */
    public void setOnButtonClicked(String pMethodName){
        this.onButtonClicked = pMethodName;
    }
    
}
