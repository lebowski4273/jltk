package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
import jltk.*;

/**
 * The Video class represents a video in the gui. You can use a video from file or web.
 * 
 * @author M. Schulte
 * @version 22.08.2022
 */
public class Video extends Component
{
    private javafx.scene.media.Media fxMedia;
    private javafx.scene.media.MediaPlayer fxMediaPlayer;
    private javafx.scene.media.MediaView fxMediaView;
    private double width, height;

    /**
     * Creates a video view with the specified size. As video path you can use a 
     * file path or an url.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the Video
     * @param pHeight The height of the Video
     * @param pVideoPath A file or url
     */
    public Video(double pLeft, double pTop, double pWidth, double pHeight, String pVideoPath)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pVideoPath);
    }

    /**
     * Creates a video view with the specified width. The height is scaled to preserve ratio. 
     * As video path you can use a file path or an url.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the Video
     * @param pVideoPath A file or url
     */
    public Video(double pLeft, double pTop, double pWidth, String pVideoPath)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, -1, pVideoPath);
    }

     /**
     * Creates a video view on the specified window with the specified width. The height 
     * is scaled to preserve ratio. As video path you can use a file path or an url.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the Video
     * @param pVideoPath A file or url
     */
    public Video(Window pWindow, double pLeft, double pTop, double pWidth, String pVideoPath)
    {
        this(pWindow, pLeft, pTop, pWidth, -1, pVideoPath);
    }

    /**
     * Creates a video view on the specified window with the specified size. As video path
     * you can use a file path or an url. 
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the Video
     * @param pHeight The height of the Video
     * @param pVideoPath A file or url
     */
    public Video(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pVideoPath)
    {
        this.myWindow = pWindow;    
        try{
            java.net.URI resource = new java.net.URI(pVideoPath);
            // is it a file
            if(!"http".equalsIgnoreCase(resource.getScheme()) && !"https".equalsIgnoreCase(resource.getScheme())){
                pVideoPath = new java.io.File(pVideoPath).toURI().toString();
            }
        }catch(Exception e){
            System.out.println("Video "+pVideoPath+" not found.");
            e.printStackTrace();
        }
        this.fxMedia = new javafx.scene.media.Media(pVideoPath); 
        this.fxMediaPlayer = new javafx.scene.media.MediaPlayer(this.fxMedia);  
        this.fxMediaView = new javafx.scene.media.MediaView(this.fxMediaPlayer);
        super.init(this.fxMediaView);
        this.myWindow.addComponent(this.fxMediaView);
        this.setPosition(pLeft,pTop);
        if(pHeight == -1){// preserve ratio and scale
            this.fxMediaView.setPreserveRatio(true);
            this.setWidth(pWidth);
            pHeight = this.fxMediaView.getBoundsInParent().getHeight();
        }else{
            this.fxMediaView.setPreserveRatio(false);
            this.setSize(pWidth, pHeight);
        }
        

    }
    /**
     * Set the new width of this media to the specified value.
     *
     * @param pWidth The new width of this component
     */
    public void setWidth(double pWidth){
        this.fxMediaView.setFitWidth(pWidth);
        this.width= pWidth;
    }

    /**
     * Set the new height of this media to the specified value.
     *
     * @param pHeight The new height of this component
     */
    public void setHeight(double pHeight){
        this.fxMediaView.setFitHeight(pHeight);
        this.height = pHeight;
    }

    /**
    Sets the new size of the media.
    @param pWidth the new width
    @param pHeight the new height
     */
    public void setSize(double pWidth, double pHeight)
    {
        this.setWidth(pWidth);
        this.setHeight(pHeight);
        this.width = pWidth;
        this.height = pHeight;
    }
    
    
    /**
     * Return the Duration of this video in seconds
     * 
     * @return The video duration in second
     */
    public double getDuration(){
        return this.fxMedia.getDuration().toSeconds();
    }
    
    /**
     * Return the current time in seconds
     *
     * @return The current time
     */
    public double getCurrentTime(){
        return this.fxMediaPlayer.getCurrentTime().toSeconds();
    }
    
    /**
     * Return the current video playback volume.
     *
     * @return The video playback volume
     */
    public double getVolume(){
        return this.fxMediaPlayer.getVolume();
    }
    
    /**
     * Starts playing the video.
     *
     */
    public void play(){
        this.fxMediaPlayer.play();
    }
    
    public void autoplay(){
        this.fxMediaPlayer.setAutoPlay(true);
    }
    
     /**
     * Stops playing the video. (Reset everything to start)
     *
     */
    public void stop(){
        this.fxMediaPlayer.stop();
    }
    
     /**
     * Pauses playing the video.
     *
     */
    public void pause(){
        this.fxMediaPlayer.pause();
    }
    
    // /**
     // * Sets the current time of the video (seek)
     // *
     // * @param pStartTime The current time
     // */
    // public void setTime(double pTime){
        // this.fxMediaPlayer.seek(new javafx.util.Duration(pTime*1000));
    // }
    
    
    /**
     * Set the video playback volume. By default volume is 1.0.
     *
     * @param pVolume The video playback volume
     */
    public void setVolume(double pVolume){
        this.fxMediaPlayer.setVolume(pVolume);
    }
    
    

}
