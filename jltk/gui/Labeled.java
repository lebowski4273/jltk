package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

import javafx.scene.text.*;

/**
 * Abstract class Labled - A class for all components having a label (Button, Checkbox, Label)

 * @author M. Schulte
 * @version 13.05.2022
 */
public abstract class Labeled extends Control
{

    javafx.scene.control.Labeled fxLabledComponent;
    protected Font font = Font.getDefault();

    protected void init(double pLeft, double pTop, double pWidth, double pHeight, jltk.Window pWindow, javafx.scene.control.Labeled pComponent)
    {
        super.init(pLeft, pTop, pWidth, pHeight, pWindow, pComponent);
        this.fxLabledComponent = pComponent;
    }

    /**
     * Set the text of the labled component.
     *
     * @param pText 
     */
    public void setText(String pText){
        this.fxLabledComponent.setText(pText);
    }

    /**
     * Returns the text of the labled component.
     *
     * @return 
     */
    public String getText(){
        return this.fxLabledComponent.getText();
    }

    /**
     * If the parameter ist true, the text of this component will be underline.
     *
     * @param pUnderline true if text should be underline, false if not.
     */
    public void setUnderline(boolean pUnderline){
        this.fxLabledComponent.setUnderline(pUnderline);
    }

    /**
     * Returns wether the component is underline or not.
     *
     * @return 
     */
    public boolean isUnderline(){
        return this.fxLabledComponent.isUnderline();
    }
    
     /**
     * Set the font style of this labled component. 
     *
     * @param pBold true if bold, else false
     * @param pItalic true if italic, else false
     * 
     */
    public void setFontStyle(boolean pBold, boolean pItalic){
        if(pBold && pItalic){
            this.font = Font.font(this.font.getName(),FontWeight.BOLD,FontPosture.ITALIC,this.font.getSize());
        }else{
            // only one or none is true
            if(pBold){
                this.font = Font.font(this.font.getName(),FontWeight.BOLD,this.font.getSize());
            }else{
                if(pItalic){
                    this.font = Font.font(this.font.getName(),FontPosture.ITALIC,this.font.getSize());
                }else{ // normal
                    this.font = Font.font(this.font.getName(),FontWeight.NORMAL,FontPosture.REGULAR,this.font.getSize());
                }
            }
        }
        this.fxLabledComponent.setFont(this.font);
    }

     /**
     * Sets the font family, as parameter you can use e.g. Verdana, Helvetica, Times New Roman, Comic Sans MS,
     * Impact, Lucida Sans Unicode. You can print all available fonts of your system, if you run 
     * jltk.util.Misc.printAvailableFontNames().
     *
     * @param pFont The font family
     */
    public void setFont(String pFontName){
        this.font = Font.font(pFontName);
        this.fxLabledComponent.setFont(this.font);
    }

    /**
     * Returns the name of the font family (also see setFont()).
     *
     * @return The font family
     */
    public String getFont(){
        return this.font.getName();
    }

    /**
     * Sets the size of the text
     *
     * @param pSize The font size
     */
    public void setFontSize(double pSize){
        this.font = Font.font(this.font.getName(),pSize);
        this.fxLabledComponent.setFont(this.font);
    }

    /**
     * Returns the font size.
     *
     * @return the font size.
     */
    public double getFontSize(){
        return this.fxLabledComponent.getFont().getSize();
    }
    
     /**
     * The font color of the component will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setFontColor(double pRed, double pGreen, double pBlue)
    {
        javafx.scene.paint.Color lColor = javafx.scene.paint.Color.color(pRed, pGreen, pBlue);
        this.setFontColor(lColor);
    }
    
   
     /**
     * The font color of the component will be changed to specified color
     *
     * @param The new font color
     */
    public void setFontColor(javafx.scene.paint.Color pColor)
    {
        this.fxLabledComponent.setTextFill(pColor);
    }
    
    /**
    The font color of the component will be changed. As Parameter you can use the color name 
    from the javafx.scene.paint.Color field https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html#field.summary
    For example setFontColor("ALICEBLUE") or setFontColor("blue").
    
    @param pColor the new font color
     */
    public void setFontColor(String pColor)
    {
        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            javafx.scene.paint.Color lColor = (javafx.scene.paint.Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.setFontColor(lColor);
                
            }
        }catch(Exception e){
            System.out.println("setFontColor: "+pColor+" is not a valid color.");
        }
    }
    
    
     
    /**
     * Aligns the text to the specified position
     *
     * @param pAlignment Alignment can be left, center, right
     */
    public void setTextAlignment(String pAlignment){
        javafx.geometry.Pos lAlignment = javafx.geometry.Pos.CENTER_LEFT;
        switch(pAlignment.toLowerCase()){
            case "right": lAlignment = javafx.geometry.Pos.CENTER_RIGHT; break;
            case "center": lAlignment = javafx.geometry.Pos.CENTER; break;
        }
        this.fxLabledComponent.setAlignment(lAlignment);
    }

 

}
