package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

import javafx.scene.text.*;

/**
 * A abstract class for all text components
 * 
 * events: confirmed, selectionChanged, contentChanged
 * 
 * @author M. Schulte
 * @version 20.05.2022
 */
public abstract class TextComponent extends Control
{
    protected javafx.scene.control.TextInputControl fxTextInputControl;
    protected javafx.scene.paint.Color color;
    protected String onContentChanged = "";
    protected String onSelectionChanged = "";
    protected String onConfirmed = "";
    protected String newLine = System.getProperty("line.separator");
    protected Font font = Font.getDefault();

    protected void init(double pLeft, double pTop, double pWidth, double pHeight, jltk.Window pWindow, javafx.scene.control.TextInputControl pComponent)
    {
        this.fxTextInputControl = pComponent;
        super.init(pLeft, pTop, pWidth, pHeight, pWindow, pComponent);
        this.fxTextInputControl.textProperty().addListener((obs, oldText, newText) -> {
                this.runMethodByName(this.onContentChanged);
            });
        this.fxTextInputControl.setOnMouseDragged(event -> {
                this.runMethodByName(this.onSelectionChanged);
            });
        this.fxTextInputControl.setOnKeyPressed(event -> {
                if( event.getCode() == javafx.scene.input.KeyCode.ENTER ) {
                    this.runMethodByName(this.onConfirmed);
                }
                });
    }

    /**
     * The text component gets a new text as content
     *
     * @param pText The new text for the text component.
     */
    public void setContent(String pText){
        this.fxTextInputControl.setText(pText);
    }

    /**
     * The text component gets a new char as content
     *
     * @param pText The new char for the text component.
     */
    public void setContent(char pChar)
    {
        this.setContent("" + pChar);
    }

    /**
     * The text component gets a new integer as content
     *
     * @param pText The new integer for the text component.
     */
    public void setContent(int pNumber)
    {
        this.setContent("" + pNumber);
    }

    /**
     * The text component gets a new long integer as content
     *
     * @param pText The new long integer for the text component.
     */
    public void setContent(long pNumber)
    {
        this.setContent("" + pNumber);
    }

    /**
     * The text component gets a new decimal number (double) as content
     *
     * @param pText The new decimal number for the text component.
     */
    public void setContent(double pNumber)
    {
        this.setContent("" + pNumber);
    }

    /**
     * The text component gets a new decimal number (float) as content
     *
     * @param pText The new decimal number for the text component.
     */
    public void setContent(float pNumber)
    {
        this.setContent("" + pNumber);
    }
    
    /**
     * The text component gets the text representation of the given object (pObject.toString()) as content
     */
    public void setContent(Object pObject)
    {
        this.setContent(pObject.toString());
    }

    /**
     * The text will be append to the end of the text component. 
     *
     * @param pText The text, that will be append.
     */
    public void append(String pText){
        this.fxTextInputControl.appendText(pText);
    }

    /**
     * The character (char) will be append to the end of the text component. 
     *
     * @param pText The character (char), that will be append.
     */
    public void append(char pChar){
        this.fxTextInputControl.appendText(""+pChar);
    }

    /**
     * The integer will be append to the end of the text component. 
     *
     * @param pText The integer, that will be append.
     */
    public void append(int pNumber){
        this.fxTextInputControl.appendText(""+pNumber);
    }

    /**
     * The long integer will be append to the end of the text component. 
     *
     * @param pText The long integer, that will be append.
     */
    public void append(long pNumber){
        this.fxTextInputControl.appendText(""+pNumber);
    }

    /**
     * The decimal number (double) will be append to the end of the text component. 
     *
     * @param pText The double, that will be append.
     */
    public void append(double pNumber){
        this.fxTextInputControl.appendText(""+pNumber);
    }

    /**
     * The float number will be append to the end of the text component. 
     *
     * @param pText The float number, that will be append.
     */
    public void append(float pNumber){
        this.fxTextInputControl.appendText(""+pNumber);
    }
    
    /**
     * The float number will be append to the end of the text component. 
     *
     * @param pText The float number, that will be append.
     */
    public void append(Object pObject){
        this.fxTextInputControl.appendText(pObject.toString());
    }

    /**
     * Check, if content of this text component is an integer.
     * 
     * @return true, if the content is an integer
     */
    public boolean contentIsInteger()
    {
        try { 
            Integer.parseInt(this.getContentAsString()); 
        } catch(NumberFormatException e) { 
            return false; 
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    /**
     * Check, if content of this text component is an long integer.
     * 
     * @return true, if the content is an long integer
     */
    public boolean contentIsLongInteger()
    {
        try { 
            Long.parseLong(this.getContentAsString()); 
        } catch(NumberFormatException e) { 
            return false; 
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    /**
     * Check, if content of this text component is an double.
     * 
     * @return true, if the content is an double
     */
    public boolean contentIsDouble()
    {
        try { 
            Double.parseDouble(this.getContentAsString()); 
        } catch(NumberFormatException e) { 
            return false; 
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    /**
     * Check, if content of this text component is an float.
     * 
     * @return true, if the content is an float
     */
    public boolean contentIsFloat()
    {
        try { 
            Float.parseFloat(this.getContentAsString()); 
        } catch(NumberFormatException e) { 
            return false; 
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    /**
     * Removes the current content of the text component.
     */
    public void clear(){
        this.fxTextInputControl.setText("");
    }

    /**
     * Returns the selected text in this text component
     *
     * @return the selected text
     */
    public String getSelectedText(){
        return this.fxTextInputControl.getSelectedText();
    }

    
    /**
     * Selects the whole text
     *
     */
    public void selectAll(){
        this.fxTextInputControl.selectAll();
    }

    /**
     * Returns the number of characters in this text component.
     */
    public int getLength(){
        return this.fxTextInputControl.getLength();
    }

    /**
     * Returns the index of the character, the selction starts.
     *
     */
    public int getSelectionStart(){
        return this.fxTextInputControl.getSelection().getStart()-1;
    }

    /**
     * Returns the index of the character, the selction ends.
     *
     */
    public int getSelectionEnd(){
        return this.fxTextInputControl.getSelection().getEnd()-1;
    }

    /**
     * Make the text component editable
     *
     */
    public void enableEdit(){
        this.fxTextInputControl.setEditable(true);
    }
    
    /**
     * Make the text component read-only
     *
     */
    public void disableEdit(){
        this.fxTextInputControl.setEditable(false);
    }

    /**
     * Returns true if the textcomponent is editable, else false.
     */
    public boolean isEditable(){
        return this.fxTextInputControl.isEditable();
    }

    /**
     * Returns the content of the text component
     * as string.
     * @return The content of the textcomponent as string
     */
    public String getContentAsString(){
        return this.fxTextInputControl.getText();
    }

    /**
     * Returns the content of the text component as integer.
     * @return The content of the textcomponent as integer
     */
    public int getContentAsInteger() throws ArithmeticException
    {
        if (this.contentIsInteger())
            return Integer.parseInt(this.getContentAsString());
        else
            throw new ArithmeticException("getContentAsInteger: "+this.getContentAsString()+" is not an integer.");
    }

    /**
     * Returns the content of the text component as long integer.
     * @return The content of the textcomponent as long
     */
    public long getContentAsLongInteger() throws ArithmeticException
    {
        if (this.contentIsLongInteger())
            return Long.parseLong(this.getContentAsString());
        else
            throw new ArithmeticException("getContentAsLongInteger: "+this.getContentAsString()+" is not an long integer.");
    }

    /**
     * Returns the content of the text component as double.
     * @return The content of the textcomponent as double
     */
    public double getContentAsDouble() throws ArithmeticException
    {
        if (this.contentIsDouble())
            return Double.parseDouble(this.getContentAsString());
        else
            throw new ArithmeticException("getContentAsDouble: "+this.getContentAsString()+" is not an double.");
    }

    /**
    Returns the content of the text component as float.
    @return The content of the textcomponent as float
     */
    public float getContentAsFloat() throws ArithmeticException
    {
        if (this.contentIsFloat())
            return Float.parseFloat(this.getContentAsString());
        else
            throw new ArithmeticException("getContentAsFloat: "+this.getContentAsString()+" is not an float.");
    }

    public void setFontColor(javafx.scene.paint.Color pColor){
        this.fxTextInputControl.setStyle("-fx-text-fill: "+pColor.toString()+";");
    }

    public void setFontColor(String pColor){
        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            javafx.scene.paint.Color lColor = (javafx.scene.paint.Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.setFontColor(lColor);
            }
        }catch(Exception e){
            System.out.println("setFontColor: "+pColor+" is not a valid color.");
        }
    }

    /**
     * The font color of the text component will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setFontColor(double pRed, double pGreen, double pBlue)
    {
        javafx.scene.paint.Color lColor = javafx.scene.paint.Color.color(pRed, pGreen, pBlue);
        this.setFontColor(lColor);
    }

    /**
     * Sets the font family, as parameter you can use e.g. Verdana, Helvetica, Times New Roman, Comic Sans MS,
     * Impact, Lucida Sans Unicode. You can print all available fonts of your system, if you run 
     * jltk.util.Misc.printAvailableFontNames().
     *
     * @param pFont The font family
     */
    public void setFont(String pFontName)
    {
        this.font = Font.font(pFontName);
        this.fxTextInputControl.setFont(this.font);
    }

    /**
     * Returns the name of the font family (also see. setFont()).
     *
     * @return The font family
     * @see #setFont(String)
     * @see jltk.util.Misc#printAvailableFontNames()
     */
    public String getFont(){
        return this.font.getName();
    }

    /**
     * Sets the size of the text
     *
     * @param pSize The font size
     */
    public void setFontSize(double pSize){
        this.font = Font.font(this.font.getName(),pSize);
        this.fxTextInputControl.setFont(this.font);
    }

    /**
     * Returns the currently used font size.
     *
     * @return The font size.
     */
    public double getFontSize(){
        return this.font.getSize();
    }

     /**
     * Set the font style of this text component. 
     *
     * @param pBold true if bold, else false
     * @param pItalic true if italic, else false
     * 
     */
    public void setFontStyle(boolean pBold, boolean pItalic){
        if(pBold && pItalic){
            this.font = Font.font(this.font.getName(),FontWeight.BOLD,FontPosture.ITALIC,this.font.getSize());
        }else{
            // only one or none is true
            if(pBold){
                this.font = Font.font(this.font.getName(),FontWeight.BOLD,this.font.getSize());
            }else{
                if(pItalic){
                    this.font = Font.font(this.font.getName(),FontPosture.ITALIC,this.font.getSize());
                }else{ // normal
                    this.font = Font.font(this.font.getName(),FontWeight.NORMAL,FontPosture.REGULAR,this.font.getSize());
                }
            }
        }
        this.fxTextInputControl.setFont(this.font);
    }
    
    /**
     * Set the name of the method which will be invoked,
     * if the content of the text component changed .
     */
    public void setOnConfirmed(String pMethodName){
        this.onConfirmed = pMethodName;
    }    
    
    /**
     * Set the name of the method, which will be invoked,
     * it the user confirms change (Enter pressed).
     */
    public void setOnContentChanged(String pMethodName){
        this.onContentChanged = pMethodName;
    }

    /**
     * Set the name of the method, which will be invoked,
     * it the user confirms change (Enter pressed).
     */
    public void setOnSelectionChanged(String pMethodName){
        this.onSelectionChanged = pMethodName;
    }
}
