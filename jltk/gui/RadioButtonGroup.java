package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

/**
 * A radio button group can contain multiple radio buttons. In a radio button group only one 
 * radio button can be selected.
 * 
 * events: <i>none</i>
 * 
 * @author M. Schulte
 * @version 18.05.2022
 */
public class RadioButtonGroup extends javafx.scene.control.ToggleGroup
{

    
    /**
     * Creates a empty Radio button group
     *
     */
    public RadioButtonGroup()
    {
        super();
    }
    
    /**
     * Add a radio button to this group.
     *
     * @param pRadioButton The RadioButton to be added to this group.
     */
    public void add(RadioButton pRadioButton){
        pRadioButton.fxRadioButton.setToggleGroup(this);
    }
    
    /**
     * Remove a radio button from this group.
     *
     * @param pRadioButton The RadioButton to be removed from this group.
     */
    public void remove(RadioButton pRadioButton){
        pRadioButton.fxRadioButton.setToggleGroup(null);
    }


   
}
