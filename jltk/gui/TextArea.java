package jltk.gui;
import jltk.*;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
/**
 * A text area  can contain multi-line text
 * 
 * events: confirmed, selectionChanged, contentChanged
 * 
 * @author M. Schulte
 * @version 23.05.2022
 */
public class TextArea extends TextComponent
{
    protected javafx.scene.control.TextArea fxTextArea;

    
    /**
     * Creates a empty text area on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pHeight The height of the label
     * @param pText The text of the label
     */
    public TextArea(double pLeft, double pTop, double pWidth, double pHeight)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight,""); //default/first window 
    }
    
    /**
     * Creates a text area on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pHeight The height of the label
     * @param pText The text of the label
     */
    public TextArea(double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pText); //default/first window 
    }

    /**
     * Creates a text area on the specified position and window.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pHeight The height of the label
     * @param pWindow The window, the label should be placed
     * @param pText The label of the label
     */
    public TextArea(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this.fxTextArea = new javafx.scene.control.TextArea();
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxTextArea);
        this.setContent(pText);
        
    }
    
    
     /**
     * The string representation (.toString()) of the given array elements  will be append
     * line by line to the textarea
     *
     * @param pText The new text for the text component.
     */
    public void setContent(Object[] pObject){
        this.clear();
        this.append(pObject);
        
    }
    
    /**
     * The string representation (.toString()) of the given array elements will be set as new content
     * of the textarea. Each element in a new line.
     *
     * @param pObject a one dimensional array
     */
    public void append(Object[] pObject){
        for(Object obj : pObject){
            this.append(obj.toString()+this.newLine);
        }
        
    }
    
    /**
     * Returns the content of the text area as String array. Each line of the 
     * textfeld becomes a element in the array.
     *
     * @return The content as String array.
     */
    public String[] getContentAsStringArray(){
        return this.getContentAsString().split(this.newLine,-1);
        
    }
    
    /**
     * Appends a new line to the text area.
     *
     */
    public void newLine(){
        this.append(this.newLine);
    }
    
    
    /**
     * Enable automatic line break, so that the content will fit into the width of the text area.
     *
     */
    public void enableLineBreak(){
        this.fxTextArea.setWrapText(true);
    }
    
    /**
     * Disable automatic line break
     *
     * see: #enableLineBreak()
     */
    public void disableLineBreak(){
        this.fxTextArea.setWrapText(false);
    }

}

