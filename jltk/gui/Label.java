package jltk.gui;
import jltk.*;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

/**
 * 
 * Label is used to show some text.
 * 
 * events: <i>none</i>
 * 
 * @author M. Schulte
 * @version 18.05.2022
 */
public class Label extends Labeled
{
    protected javafx.scene.control.Label fxLabel;
    
    /**
     * Creates the label on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pHeight The height of the label
     * @param pText The text of the label
     */
    public Label(double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pText); //default/first window 
    }

    /**
     * Creates the label
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pHeight The height of the label
     * @param pWindow The window, the label should be placed
     * @param pText The label of the label
     */
    public Label(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this.fxLabel = new javafx.scene.control.Label();
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxLabel);
        this.setText(pText);
 
    }
}
