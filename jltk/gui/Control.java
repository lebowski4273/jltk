package jltk.gui;
import jltk.Window; 
import jltk.Component;

/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

/**
 * abstract class Control - The base class for all gui elements
 * Each element has an absolute position, which is defined by the upper left corner. 
 * <p>To use a gui component, you have to create a subclass of the class App 
 * (<i>public class myClass extends App {...} </i>). Then create an object of the gui component
 * you like and pass the position and size to the constructor:</p>
 * <center><code>Button myButton = new Button(50,75,100,25,"Hello World")</code></center>
 * <p>The button will be occure on the window that belongs to the App instance. In this case the upper
 * left corner of the button has the coordinates (50,75) and the size 100x25. The button is labled 
 * with "Hello World".</br>
 * For each component different eventhandler can be set. For example:
 * <center><code>myButton.onButtonClicked("myMethod")</code><center>
 * Every time the button is clicked, the method <i>myMethod</i> is invoked. <i>myMethod</i> must be 
 * implement in your subclass of App.</p>
 * <p>Please see the documentation of the different classes in package jltk.gui for detailed information.
 * </p>
 * 
 * 
 * @author M. Schulte
 * @version 25.3.2022
 */
public abstract class Control extends Component
{
    protected javafx.scene.control.Control fxControl; // The corresponing node/component from Javafx
    protected javafx.scene.paint.Color color;
    private double width, height;
    /**
     * Basic initialisation, like setting postion and size, add the Element to the window. 
     * For some reason this cannot be solved using a constructor, because in subclasses the javaFX
     * component must be initialised first. After this, init must be called.
     *
     * @param pX The horizontal coordinate
     * @param pY The vertical coordinate
     * @param pWidth The width of the component
     * @param pHeight The height of the component
     * @param pWindow The window on which the component should be placed
     * @param pComponent The corresponding JavaFX-Component
     */
    protected void init(double pX, double pY, double pWidth, double pHeight, Window pWindow, javafx.scene.control.Control pComponent)
    {
        this.fxControl = pComponent;
        this.myWindow = pWindow;
        super.init(fxControl); // init of Node (focusGained Eventhandler ...)
        this.setPosition(pX, pY);
        this.setSize(pWidth, pHeight);
        this.fxControl.setOpacity(100);
        
        this.myWindow.addComponent(this.fxControl);
    }

    /**
     * Set the new width of this component to the specified value.
     *
     * @param pWidth The new width of this component
     */
    public void setWidth(double pWidth){
        this.fxControl.setPrefWidth(pWidth);
        this.width= pWidth;
    }
    
      /**
     * Set the new height of this component to the specified value.
     *
     * @param pHeight The new height of this component
     */
    public void setHeight(double pHeight){
        this.fxControl.setPrefHeight(pHeight);
        this.height = pHeight;
    }
    
    /**
    Sets the new size of the component.
    @param pWidth the new width
    @param pHeight the new height
     */
    public void setSize(double pWidth, double pHeight)
    {
        this.fxControl.setPrefSize(pWidth, pHeight);
        this.width = pWidth;
        this.height = pHeight;
    }

    /**
    The backgroundcolor of the component will be changed.
    @param pColor The new color
     */
    public void setColor(javafx.scene.paint.Color pColor)
    {
        fxControl.setBackground(
            new javafx.scene.layout.Background(
                new javafx.scene.layout.BackgroundFill(
                    pColor, javafx.scene.layout.CornerRadii.EMPTY, javafx.geometry.Insets.EMPTY
                )
            )
        );
        this.color = pColor;
    }

    /**
     * The background color of the component will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setColor(double pRed, double pGreen, double pBlue)
    {
        javafx.scene.paint.Color lColor = javafx.scene.paint.Color.color(pRed, pGreen, pBlue);
        this.setColor(lColor);
    }


    /**
    The background color of the component will be changed. As Parameter you can use the color name 
    from the javafx.scene.paint.Color field https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html#field.summary
    For example setColor("ALICEBLUE") or setColor("blue").

    @param pColor the new color
     */
    public void setColor(String pColor){

        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            javafx.scene.paint.Color lColor = (javafx.scene.paint.Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.setColor(lColor);

            }
        }catch(Exception e){
            System.out.println("setColor: "+pColor+" is not a valid color.");
        }

    }
    /**
     * Return the color of the component.
     *
     * @return The color of the component
     * @see #setColor(String)
     * @see #setColor(Color)
     * @see #setColor(double,double,double)
     */
    public javafx.scene.paint.Color getColor(){
        return this.color;
    }

    /**
    Return the height of the component
    @return The height of the component
     */
    public double getHeight()
    {
        //return this.fxControl.getHeight();
        return this.height;
    }

    /**
    Return the width of the component
    @return The width of the component
     */
    public double getWidth()
    {
       // return this.fxControl.getWidth();
       return this.width;
    }
  

    /**
    This component get an tooltip. Tooltips typically shown, if the mouse moves over the component.
    @param pText the text of the tooltip
     */
    public void setTooltip(String pText)
    {
        javafx.scene.control.Tooltip t = new javafx.scene.control.Tooltip(pText);
        javafx.scene.control.Tooltip.install(this.fxControl, t);
    }
}
