package jltk.gui;
import jltk.*;

/**
 * A object of this class is used to show an image.
 * 
 * @author M. Schulte
 * @version 18.8.2022
 */
public class Image extends Control
{
    protected javafx.scene.image.ImageView fxImageView;
    protected javafx.scene.image.Image fxImage;
     protected javafx.scene.control.Label fxLabel;
      private String onImageClicked ="";
    
     
      /**
     * Loads the image from the specified file. The image is scaled to the specified width and height.
     * 
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pHeight The height of the image
     * @param pImage The filename of the image. 
     */
    public Image(double pLeft, double pTop, double pWidth, double pHeight, String pFile)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pFile); //default/first window 
 
    }
    
      /**
     * Loads the image from the specified file. The image is scaled to the specified width. The height is scaled to 
     * preserve ratio.
     * 
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pImage The filename of the image. 
     */
    public Image(double pLeft, double pTop, double pWidth, String pFile)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, -1, pFile); //default/first window 
 
    }
    
      /**
     * Loads the image from the specified file. The image is scaled to the specified width. The height is scaled to 
     * preserve ratio. The image is placed on the specified window.
     * 
     * @param pWindow The window, the image should be place
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pImage The filename of the image. 
     */
    public Image(Window pWindow, double pLeft, double pTop, double pWidth, String pFile)
    {
        this(pWindow, pLeft, pTop, pWidth, -1, pFile); 
 
    }
    
    
    /**
     * Loads the image from a file. The image is scaled to the specified width and height and placed on the 
     * specified window
     * 
     * @param pWindow The window, the image should be placed
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the label
     * @param pHeight The height of the image
     * @param pImage The filename of the image. 
     */
    public Image(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pFile)
    {
        try{
            java.net.URI resource = new java.net.URI(pFile);
            // is it a file
            if(!"http".equalsIgnoreCase(resource.getScheme()) && !"https".equalsIgnoreCase(resource.getScheme())){
                pFile = new java.io.File(pFile).toURI().toString();
            }
        }catch(Exception e){
            System.out.println("Image "+pFile+" not found.");
            e.printStackTrace();
        }
        this.fxImage = new javafx.scene.image.Image(pFile);
        
        this.fxImageView = new javafx.scene.image.ImageView(this.fxImage);
        this.fxImageView.setCache(true);
        this.fxImageView.setFitWidth(pWidth);
        if(pHeight == -1){// preserve ratio and scale
            this.fxImageView.setPreserveRatio(true);
            pHeight = this.fxImageView.getBoundsInParent().getHeight();
        }else{
             this.fxImageView.setFitHeight(pHeight);   
        }
        this.fxLabel = new javafx.scene.control.Label();
        this.fxLabel.setGraphic(this.fxImageView);
        this.fxLabel.setOnMouseClicked((event) -> {
                this.runMethodByName(this.onImageClicked);
            });
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxLabel);
 
    }
    
    
    /**
     * Set a new image and scale it to the width an height of the old image
     *
     * @param pFile The filename of the new image.
     */
    public void setImage(String pFile){
        this.fxImage = new javafx.scene.image.Image(pFile);
        this.fxImageView.setImage(this.fxImage);
        this.fxImageView.setFitWidth(this.getHeight());
        this.fxImageView.setFitWidth(this.getWidth());
    }
    
     /**
     * Set the name of the method, which will be invoked,
     * if the image is clicked.
     */
    public void setOnImageClicked(String pMethodName){
        this.onImageClicked = pMethodName;
    }
}
