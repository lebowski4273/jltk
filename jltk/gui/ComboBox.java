package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
import jltk.*;

/**
 * A Combobox holds different entries. One entry can be select.
 * 
 * events: itemSelected
 * 
 * @author Martin Schulte
 * @version 10.4.2022
 */
public class ComboBox extends Control
{
    protected javafx.scene.control.ComboBox fxComboBox;
    private String onItemSelected ="";
    /**
     * Create the ComboBox. Put it on the window and set the actionlistener.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the combo box
     * @param pHeight The height of the combo box
     */
    public ComboBox(double pLeft, double pTop, double pWidth, double pHeight)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight); //default/first window 
    }

    /**
     * Create the combo box
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the combo box
     * @param pHeight The height of the combo box
     * @param pWindow The window, the combo box should be placed
     * @param pText The label of the combo box
     */
    public ComboBox(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight)
    {
        this.fxComboBox = new javafx.scene.control.ComboBox();
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxComboBox);
        this.fxComboBox.setOnAction((event) -> {
                this.runMethodByName(this.onItemSelected);
            });
    }

    /**
     * Append pText to the Combobox
     *
     * @param pText The object to be append.
     */
    public void append(String pText){
        this.fxComboBox.getItems().add(pText);

    }

    /**
     * Append pObject to the Combobox
     *
     * @param pObject The object to be append.
     */
    public void append(Object pObject){
        this.fxComboBox.getItems().add(pObject);

    }

    /**
     * Insert pText at the specified position in this combobox. Shifts the item currently at that position (if any) and any subsequent item to the bottom (adds one to their indices, first item has index 0).
     *
     * @param pText The text to be add.
     */
    public void add(int pIndex, String pText){
        this.fxComboBox.getItems().add(pIndex, pText);

    }

    /**
     * Insert pObject at the specified position in this combobox. Shifts the item currently at that position (if any) and any subsequent item to the bottom (adds one to their indices, first item has index 0).
     *
     * @param pObject The object to add.
     */
    public void add(int pIndex, Object pObject){
        this.fxComboBox.getItems().add(pIndex, pObject);

    }

    /**
     * Replace the item at the specified position in this combobox with pText (first item has index 0).
     *
     * @param pText The text to be set.
     */
    public void set(int pIndex, String pText){
        this.fxComboBox.getItems().set(pIndex, pText);

    }

    /**
     * Replace the item at the specified position in this combobox with pObject (first item has index 0).
     *
     * @param pText The object to be set.
     */
    public void set(int pIndex, Object pObject){
        this.fxComboBox.getItems().set(pIndex, pObject);

    }

    /**
     * Remove the item at the specified index (first item has index 0).
     *
     * @param pIndex the Index of the item, should be removed.
     */
    public void remove(int pIndex){
        this.fxComboBox.getItems().remove(pIndex);
    }

    /**
     * Remove all entries from the combobox
     *
     */
    public void clear(){
        this.fxComboBox.getItems().clear();

    }

    /**
     * Selects (highlights) the item specified by pIndex
     *
     * @param pIndex 
     */
    public void select(int pIndex){
        this.fxComboBox.getSelectionModel().select(pIndex);
    }
    
    /**
     * Deselects the item specified by pIndex
     *
     * @param pIndex 
     */
    public void deselect(int pIndex){
        this.fxComboBox.getSelectionModel().clearSelection(pIndex);
    }

    /**
     * Clears the selection of all selected indices. 
     */
    public void clearSelection(){
        this.fxComboBox.getSelectionModel().clearSelection();
    }
    
     /**
     * Returns true, if the given index is currently selected, otherwise false.
     *
     * @param pIndex The index to check as to whether it is currently selected or not
     * @return True if the given index is selected, false otherwise
     */
    public boolean isSelected(int pIndex){
        return this.fxComboBox.getSelectionModel().isSelected(pIndex);
    }
    
    /**
     * Returns the index of the selected item (first item has index 0).
     *
     * @return The index of the selected item
     */
    public int getSelectedIndex(){
        return this.fxComboBox.getSelectionModel().getSelectedIndex();
    }

    /**
     * Returns the selected item as object.
     *
     * @return The selected item
     */
    public Object getSelectedItem(){
        return this.fxComboBox.getSelectionModel().getSelectedItem();
    }
    
    /**
     * Returns the number of items in this combobox.
     *
     * @return The number of items.
     */
    public int getSize(){
        return this.fxComboBox.getItems().size();
    }

    /**
     * Returns the selected item as string.
     *
     * @return The string representation of the item.
     */
    public String getSelectedItemAsText(){
        return this.fxComboBox.getSelectionModel().getSelectedItem().toString();
    }
    
    
    /**
     * Sets the name of the method, which should be invoke,
     * if an item of the combobox is selected.
     *
     */
    public void setOnItemSelected(String pMethodName){
        this.onItemSelected = pMethodName;
    }



}


