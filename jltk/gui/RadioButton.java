package jltk.gui;
import jltk.*;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

/**
 * A radio button can be checked or unchecked. If you create a serie of radio buttons and place them in
 * a radio button group, only one radio button ca be selected at a time.
 * 
 * events: clicked, selcted, deselected
 * 
 * @author M.Schulte    
 * @version 18.05.2022
 */
public class RadioButton extends Labeled
{
    protected javafx.scene.control.RadioButton fxRadioButton;
    private String onSelected ="";
    private String onDeselected ="";
    private String onRadiobuttonClicked ="";
    
    /**
     * Creates the radio button
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the radio button
     * @param pHeight The height of the radio button
     * @param pText The label of the check radio butotn
     */
    public RadioButton(double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pText); //default/first window 
    }

     /**
     * Creates the check box
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the radio button
     * @param pHeight The height of the radio button
     * @param pWindow The window, the radio button should be placed
     * @param pText The label of the radio button
     */
    public RadioButton(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this.fxRadioButton = new javafx.scene.control.RadioButton();
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxRadioButton);
        this.setText(pText);
         this.fxRadioButton.setOnAction((event) -> {
            if (this.fxRadioButton.isSelected()) {
                this.runMethodByName(this.onSelected);
            } else {
                this.runMethodByName(this.onDeselected);
            }
            this.runMethodByName(this.onRadiobuttonClicked);
        });
    }
    
       
    /**
     * Sets the name of the method, which will be invoked,
     * if the radio button is selected.      *
     */
    public void setOnSelected(String pMethodName){
        this.onSelected = pMethodName;
    }
    
    /**
     * Sets the name of the method, which will be invoked,
     * if the radio button is selected.      *
     */
    public void setOnDeselected(String pMethodName){
        this.onDeselected = pMethodName;
    }
    
    /**
     * Sets the name of the method, which will be invoked
     * if the radio button is clicked.
     */
    public void setOnRadiobuttonClicked(String pMethodName){
        this.onRadiobuttonClicked = pMethodName;
    }

    
    /**
     * Returns true, if the radio button is selected.
     *
     * @return 
     */
    public boolean isSelected(){
        return this.fxRadioButton.isSelected();
    }
    
    /**
     * Selects this radio button.
     *
     */
    public void select(){
        this.fxRadioButton.setSelected(true);
    }
    
    /**
     * Deselects this radio button.
     *
     */
    public void deselect(){
        this.fxRadioButton.setSelected(false);
    }
    
    /**
     * Toggles the radio button (change the state)
     *
     */
    public void toggle(){
        this.fxRadioButton.fire();
    }
    
}
