package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */ 
import jltk.*;

/**
 * A table 
 * 
 * events: cellselected, cellEdited, contentChanged
 * 
 * @author M. Schulte
 * @version 27.05.2022
 */
public class Table extends Control
{
    protected javafx.scene.control.TableView<java.util.ArrayList<String>> fxTableView;
    private String onCellSelected ="";
    private String onCellEdited ="";
    private String onContentChanged = "";
    private javafx.scene.control.TableView.TableViewSelectionModel defaultSelectionModel;

    java.util.ArrayList<String> line1;
    java.util.ArrayList<String> line2;
    private javafx.collections.ObservableList<java.util.ArrayList<String>> items;
    private java.util.ArrayList<String> columnNames;

    /**
     * Create the Table on the main View
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the combo box
     * @param pHeight The height of the combo box
     */
    public Table(double pLeft, double pTop, double pWidth, double pHeight)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight); //default/first window 
    }

    /**
     * Creates the Table
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the table
     * @param pHeight The height of the table
     * @param pWindow The window, the table should be placed
     */
    public Table(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight)
    {
        this.fxTableView = new javafx.scene.control.TableView<>();
        this.fxTableView.setColumnResizePolicy(javafx.scene.control.TableView.UNCONSTRAINED_RESIZE_POLICY);
        /**this.fxTableView.setOnMouseClicked((event) -> {
        this.runMethodByName(this.eventhandlerSelected);
        });**/
        // Eventhandler selection changed
        this.fxTableView.getSelectionModel().getSelectedCells().addListener((javafx.collections.ListChangeListener)(c  -> {
                    this.runMethodByName(this.onCellSelected);
                }));
        this.defaultSelectionModel = this.fxTableView.getSelectionModel();
        this.columnNames = new java.util.ArrayList<String>();
        this.items = javafx.collections.FXCollections.observableArrayList();
        this.fxTableView.setItems(items);
        /*columnNames.add("A");
        columnNames.add("B");
        columnNames.add("C");
        for (int i = 0; i < columnNames.size(); i++) {
        final int index = i;
        javafx.scene.control.TableColumn< java.util.ArrayList<String>, String> column = new javafx.scene.control.TableColumn<>(columnNames.get(i));
        column.setCellValueFactory(cd -> new javafx.beans.property.SimpleStringProperty(cd.getValue().get(index)));
        this.fxTableView.getColumns().add(column);
        }
        line1 = new java.util.ArrayList<String>();
        line1.add("1");
        line1.add("2");
        line1.add("3");

        line2 = new java.util.ArrayList<String>();
        line2.add("a");
        line2.add("b");
        line2.add("c");
        items = javafx.collections.FXCollections.observableArrayList(line1, line2);

        this.items.addListener(new javafx.collections.ListChangeListener() {
        @Override
        public void onChanged(javafx.collections.ListChangeListener.Change change) {
        System.out.println("change!");
        }

        });*/
        //this.fxTableView.setItems(items);
        //this.items.addListener((javafx.collections.ListChangeListener<java.util.ArrayList<String>>)(c  -> {
        //            this.runMethodByName(this.onContentChanged);
        //        }));
        /*String[] columnNames = {"A","B","C"};
        // https://stackoverflow.com/questions/53807650/adding-items-into-tableview-without-using-an-instance-of-any-class
        for (int i = 0; i < columnNames.length; i++) {
        final int index = i;
        javafx.scene.control.TableColumn<String[], String> column = new javafx.scene.control.TableColumn<>(columnNames[i]);
        column.setCellValueFactory(cd -> new javafx.beans.property.SimpleStringProperty(cd.getValue()[index]));
        this.fxTableView.getColumns().add(column);
        }
        javafx.collections.ObservableList<String[]> items = javafx.collections.FXCollections.observableArrayList({"ARD", "ZDF", "N3"});;
        // javafx.collections.ObservableList<String> items = javafx.collections.FXCollections.observableArrayList("Itachi");
        this.fxTableView.setItems(items);*/
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxTableView);
    }

    public void test(String neu){
        //line1.set(1,neu);
        java.util.ArrayList<String> line3 = new java.util.ArrayList<String>();
        line3.add("i");
        line3.add("ii");
        line3.add("iii");
        line3.add("iv");
        this.items.add(line3);
        java.util.ArrayList<String> line4 = new java.util.ArrayList<String>();
        line4.add("y");
        line4.add("x");
        line4.add("z");
        this.items.add(line4);
        //this.appendColumn("NNeu");
        String[] row ={"N1","N2"};
        //this.appendColumn("gNeu",row);
        this.insertColumn(1, "sNeu");
        this.removeColumn(2);
        this.setColumnWidth(50);
        //this.disableSelection();
        this.enableMultiSelection();
        //this.enableCellSelection();
        this.select(2,1);
        this.select(3);
    }

    private void addTableColumn(javafx.scene.control.TableColumn< java.util.ArrayList<String>, String> pColumn, int pIndex, int pCallbackIndex){
        //Add new column an wait until ready. This is 
        final java.util.concurrent.CountDownLatch waitForJavaFx = new java.util.concurrent.CountDownLatch(1);
        javafx.application.Platform.runLater(() -> {
                pColumn.setCellValueFactory(cd -> new javafx.beans.property.SimpleStringProperty(cd.getValue().get(pCallbackIndex)));
                if(pIndex < 0){
                    this.fxTableView.getColumns().add(pColumn);
                }else{
                    this.fxTableView.getColumns().add(pIndex,pColumn);
                }
                waitForJavaFx.countDown();
            });
        try
        {
            waitForJavaFx.await();
        }
        catch (InterruptedException ie)
        {
            ie.printStackTrace();
        }
    }

    /**
     * Appends a column to the end of this table. If the underlying data still hold data for this column, this data will 
     * be shown. If not, the column will be empty
     *
     * @param pTitle The title of the new column
     */
    public void appendColumn(String pTitle){
        javafx.scene.control.TableColumn< java.util.ArrayList<String>, String> column = new javafx.scene.control.TableColumn<>(pTitle);
        int columnCount = this.getColumnCount()+1;
        // Fix size for each row
        for(java.util.ArrayList<String> item : items){
            if(item.size() < columnCount){
                // Fix missing before, maybe superfluous if remove works as expected
                for(int i=item.size(); i<columnCount;i++){
                    item.add(i,"");
                }
            }
        }
        //System.out.println("ColumnCount: "+columnCount);
        this.addTableColumn(column, -1, columnCount-1);
        //javafx.application.Platform.runLater(() ->this.fxTableView.getColumns().add(column));
    }

    /**
     * Appends a column with the specified content to the end of this table. pContent must be an array of any kind
     * of objects. The string representation (.toString()) of each object will be used. 
     * If the underlying data still hold data for this column, they will be overwritten by the new data.
     *
     * @param pTitle The title of the new column
     * @param pContent The content of the new column
     */
    public void appendColumn(String pTitle, Object[] pContent){
        javafx.scene.control.TableColumn< java.util.ArrayList<String>, String> column = new javafx.scene.control.TableColumn<>(pTitle);
        int columnCount = this.getColumnCount()+1;
        int row = 0;
        // Fix size for each row
        for(java.util.ArrayList<String> item : items){
            if(item.size() < columnCount){
                // Fix missing before, maybe superfluous if remove works as expected
                for(int i=item.size(); i<columnCount;i++){
                    item.add(i,"");
                }
            }
            try{
                item.set(columnCount-1, String.valueOf(pContent[row]));
            }catch(ArrayIndexOutOfBoundsException e) {
                item.set(columnCount-1, "");
                System.out.println("appendColumn(): Not enough data for this column. Fill up with empty strings.");
            }catch(Exception e){
                e.printStackTrace();
            }
            row++;
        }
        this.addTableColumn(column, -1, columnCount-1);
    }

    /**
     * Inserts a empty column at the specified position of this table. Shifts the column currently at this position
     * and any subsequent columns to the right. 
     * The index of the first column is 0.
     *
     * @param pIndex The position of the new column (first column has index 0)
     * @param pTitle The title of the new column
     */
    public void insertColumn(int pIndex, String pTitle){
        String[] emptyContent = new String[this.getRowCount()];
        for(int i = 0; i< emptyContent.length; i++){
            emptyContent[i]="";
        }
        this.insertColumn(pIndex, pTitle, emptyContent);
    }

    /**
     * Inserts a column with specified content at the specified position of this table. Shifts the column currently at 
     * this position and any subsequent columns to the right.pContent must be an array of any kind
     * of objects. The string representation (.toString()) of each object will be used.
     * The index of the first column is 0.
     *
     * @param pIndex The position of the new column (first column has index 0)
     * @param pTitle The title of the new column
     * @param pContent The content of the new column
     */
    public void insertColumn(int pIndex, String pTitle, Object[] pContent){
        if(pIndex <= this.getColumnCount() && pIndex>=0){
            javafx.scene.control.TableColumn<java.util.ArrayList<String>, String> column = new javafx.scene.control.TableColumn<>(pTitle);
            int columnCount = this.fxTableView.getColumns().size()+1;
            int row = 0;
            for(java.util.ArrayList<String> item : items){
                try{
                    item.add(pIndex,String.valueOf(pContent[row]));
                }catch(ArrayIndexOutOfBoundsException e) {
                    item.add(pIndex, "");
                    System.out.println("insertColumn(): Not enough data for this column. Fill up with empty strings.");
                }catch(Exception e){
                    e.printStackTrace();
                }
                row++;
            }
            //column.setCellValueFactory(cd -> new javafx.beans.property.SimpleStringProperty(cd.getValue().get(pIndex)));
            // Fix the callback for all following columns.
            this.addTableColumn(column, pIndex, pIndex);
            for(int i = pIndex+1; i<this.fxTableView.getColumns().size();i++){
                final int index = i; // For lambda below
                javafx.scene.control.TableColumn<java.util.ArrayList<String>, String> nextColumn =(javafx.scene.control.TableColumn<java.util.ArrayList<String>, String>) this.fxTableView.getColumns().get(i);
                nextColumn.setCellValueFactory(cd -> new javafx.beans.property.SimpleStringProperty(cd.getValue().get(index)));
            }
            //javafx.application.Platform.runLater(() ->this.fxTableView.getColumns().add(pIndex,column));

        }
    }

    /**
     * Removes the column at the specified position of this table. Any subsequent columns shift to the left. 
     * The index of the first column is 0
     *
     * @param pIndex The position of the new column (first column has  index 0)
     */
    public void removeColumn(int pIndex){
        if(pIndex < this.getColumnCount() && pIndex>=0){
            // Remove data
            for(java.util.ArrayList<String> item : items){
                item.remove(pIndex)   ;
            }
            // Fix the callback for all following columns.
            for(int i = pIndex+1; i<  this.fxTableView.getColumns().size();i++){
                final int index = i; // For lambda below
                javafx.scene.control.TableColumn<java.util.ArrayList<String>, String> nextColumn =(javafx.scene.control.TableColumn<java.util.ArrayList<String>, String>) this.fxTableView.getColumns().get(i);
                nextColumn.setCellValueFactory(cd -> new javafx.beans.property.SimpleStringProperty(cd.getValue().get(index-1)));
            }
            javafx.application.Platform.runLater(() ->this.fxTableView.getColumns().remove(pIndex));
        }
    }

    /**
     * Appends a empty row to the end of this table. 
     *
     */
    public void appendRow(){
        String[] newRow = new String[0];
        this.appendRow(newRow);
    }

    /**
     * Inserts a row with the specified content to the end of this table.
     *
     * @param pContent The content for the new row
     */
    public void appendRow(Object[] pContent){
        java.util.ArrayList<String> newRow = new java.util.ArrayList<String>();
        for(Object item : pContent){
            newRow.add(String.valueOf(item));
        }
        // pContent to short?
        if(newRow.size()<this.getColumnCount()){
            // Then fill up with empty strings.
            for(int i=newRow.size();i<this.getColumnCount();i++){
                newRow.add(i,"");
            }
        }
        this.items.add(newRow);
    }

    /**
     * Inserts a row with the specified content at the specified position in this table. Shifts the row currently at this position
     * and any subsequent rows to the right. 
     * The index of the first row is 0.
     *
     * @param pIndex The position of the new row (first column has index 0)
     */
    public void insertRow(int pIndex, Object[] pContent){
        if(pIndex <= this.getRowCount() && pIndex>=0){
            java.util.ArrayList<String> newRow = new java.util.ArrayList<String>();
            for(Object item : pContent){
                newRow.add(String.valueOf(item));
            }
            // pContent to short?
            if(newRow.size()<this.getColumnCount()){
                // Then fill up with empty strings.
                for(int i=newRow.size();i<this.getColumnCount();i++){
                    newRow.add(i,"");
                }
            }
            this.items.add(pIndex,newRow);
        }
    }

    /**
     * Inserts a row with the specified content at the specified position in this table. Shifts the row currently at this position
     * and any subsequent rows to the right. 
     * The index of the first row is 0.
     *
     * @param pIndex The position of the new row (first column has index 0)
     */
    public void insertRow(int pIndex){
        String[] newRow = new String[0];
        this.insertRow(pIndex,newRow);
    }

    /**
     * Removes the row at the specified position of this table. Any subsequent row shift to the left. 
     * The index of the first row is 0
     *
     * @param pIndex The position of the new row (first row has  index 0)
     */
    public void removeRow(int pIndex){
        if(pIndex < this.getColumnCount() && pIndex>=0){
            this.items.remove(pIndex); // Remove data
        }
    }

    /**
     * Removes all columns and data and creates new columns with the specified title.
     *
     * @param pTitles The titles of the new columns
     */
    public void createColumns(String[] pTitles){
        this.clearAll();
        for(String title : pTitles){
            this.appendColumn(title);
        }
    }

    /**
     * Sets the title of the specified column.
     *
     * @param pIndex The index of the column (First column has index 0)
     * @param pTitle The new title.
     */
    public void setColumnTitle(int pIndex, String pTitle){
        javafx.application.Platform.runLater(() -> 
                this.fxTableView.getColumns().get(pIndex).setText(pTitle)
        );

    }

    /**
     * Removes all columns and data and creates the specified number of columns. The column 
     * title will be set to A,B,C,...
     *
     * @param pQuantity The nummber of new columns.
     */
    public void createColumns(int pNumber){
        this.clearAll();
        for(int i=0; i<pNumber;i++){
            this.appendColumn((char) (i+65)+"");
        }
    }

    /**
     * Sets the specified content as new content for the tables. pTitles specifies the column title.
     * If the length of pTitles is smaller than the number of data columns, empty columns will be append,
     * If the length of pTitles is greater than the number of data columns, the colums will be titled 
     * with chars.
     *
     * @param pContent The new content of the table
     * @param pContent The new title of the table columns
     */
    public void setItems(Object[][] pContent, String[] pTitles){
        this.clearAll();
        int columnLength = pTitles.length;
        if(pContent.length >0 && pContent[0].length > pTitles.length){// More columns than titles
            columnLength = pContent[0].length;
        }

        for(int i =0;i<columnLength;i++){
            String title = "";
            try{
                title = pTitles[i];
            }catch(Exception e){
                title = (char) (i+65)+"";  // char for title
            }
            this.appendColumn(title);
        }

        for(Object[] obj : pContent){
            this.appendRow(obj);
        }
        //this.fixCallback();
        //this.refresh();
    }

    /**
     * Sets the specified content as new content for the tables. The column
     * title will be automatically set to A,B,C,...
     *
     * @param pContent the new content of the table.
     */
    public void setItems(Object[][] pContent){
        this.setItems(pContent, new String[0]);
    }

    /**
     * For internal use. Connects table cells with the right position in items.
     *
     */
    private void fixCallback(){
        for(int i = 0; i<this.fxTableView.getColumns().size();i++){
            final int index = i; // For lambda below
            javafx.scene.control.TableColumn<java.util.ArrayList<String>, String> nextColumn =(javafx.scene.control.TableColumn<java.util.ArrayList<String>, String>) this.fxTableView.getColumns().get(i);
            nextColumn.setCellValueFactory(cd -> new javafx.beans.property.SimpleStringProperty(cd.getValue().get(index)));
        }
    }

    /**
     * Refresh/Rebuild the table.
     *
     */
    public void refresh(){
        this.fixCallback();
        this.fxTableView.refresh();
    }

    /**
     * Clears the table. Deletes all data and all headers.
     *
     */
    public void clearAll(){
        this.clearData();
        final java.util.concurrent.CountDownLatch waitForJavaFx = new java.util.concurrent.CountDownLatch(1);
        javafx.application.Platform.runLater(() -> {
                this.fxTableView.getColumns().clear();
                waitForJavaFx.countDown();
            });
        try
        {
            waitForJavaFx.await();
        }
        catch (InterruptedException ie)
        {
            ie.printStackTrace();
        }
    }

    /**
     * Removes all data from the table. Do not clear the headers.
     *
     */
    public void clearData(){
        this.items.clear();
    }

    /**
     * Replaces the item at the specified postion in this table. 
     *
     * @param pRow The index of the row (starting with 0).
     * @param pColumn The index of the column (starting with 0).
     * @param pText The string to be inserted
     */
    public void setItem(int pRow, int pColumn, String pText){
        this.items.get(pRow).set(pColumn, pText);
        this.fxTableView.refresh();
    }

    /**
     * Returns the number of rows of this table.
     *
     * @return The number of rows.
     */
    public int getRowCount(){
        return this.items.size();
    }

    /**
     * Returns the number of columns of this table.
     *
     * @return The number of columns.
     */
    public int getColumnCount(){
        return this.fxTableView.getColumns().size();
    }

    /**
     * Returns the element at the specified position in this table. If the cell does not exists, null will be returned.
     *
     * @param pRow The index of the row (starting with 0).
     * @param pColumn The index of the column (starting with 0).
     * 
     * @return The element at the specified position in this table. null, if not exists.
     */
    public String getItem(int pRow, int pColumn){
        if(pRow < this.getRowCount() && pColumn < this.getColumnCount() && pRow >=0 && pColumn >= 0){
            return this.items.get(pRow).get(pColumn);
        }else{
            return null;
        }
    }

    /**
     * Returns all items of the table as 2d string array.
     *
     * @return all items of the table.
     */
    public String[][] getItems(){
        String[][] itemsArray = new String[this.getRowCount()][this.getColumnCount()];
        for(int i=0;i<items.size();i++){
            itemsArray[i] = items.get(i).toArray(new String[this.getColumnCount()]);
        }
        return itemsArray;
    }

    /**
     * Returns the specified row as string array. If the row does not exists, null will be returned
     * The first row has the index 0.
     *
     * @param pRow The index of the row (starting with 0).
     * 
     * @return The specified row as string array. null, if not exists.
     */
    public String[] getRow(int pIndex){
        if(pIndex < this.getRowCount()){
            return this.items.get(pIndex).toArray(new String[0]);
        }else{
            return null;
        }
    }

    /**
     * Returns the specified column as string array. If the column does not exists, null will be returned
     * The first column has the index 0.
     *
     * @param pColumn The index of the column (starting with 0).
     * 
     * @return The specified row as string array. null, if not exists.
     */
    public String[] getColumn(int pIndex){
        if(pIndex<this.getColumnCount()){
            String[] column = new String[this.getRowCount()];
            int row = 0;
            for(java.util.ArrayList<String> item : items){
                try{
                    column[row] = item.get(pIndex);
                }catch(Exception e){
                    e.printStackTrace();
                }
                row++;
            }
            return column;
        }else{
            return null;
        }
    }

    /**
     * Returns the index of the selected row and -1, if there is no selection. If multi-selection is enabled, the lowest
     * index is returned. You can use getSelectedRowIndices() to get all selected indices if you enabled multi-selection.
     *
     * @return The index of the selected row.
     */
    public int getSelectedRowIndex(){
        if(this.fxTableView.getSelectionModel() != null){
            return this.fxTableView.getSelectionModel().getSelectedIndex();
        }
        return -1;
    }

    /**
     * Returns the index of the selected column and -1, if there is no selection. If multiple columns are 
     * selected, the lowest index of that column, which is selected in the row with the lowest index is returned.
     * Especially this means, you always get 0, if row selection is enabled. If you enabled multi-selection, you can use getSelectedCellIndices() 
     * to get all indices of the selected cells.
     *
     * @return The index of the selected column.
     */
    public int getSelectedColumnIndex(){
        if(this.fxTableView.getSelectionModel() != null && !this.fxTableView.getSelectionModel().getSelectedCells().isEmpty()){
            return this.fxTableView.getSelectionModel().getSelectedCells().get(0).getColumn();
        }
        return -1;
    }

    public int[] getSelectedColumnsIndices(){
        int[][] cellIndices = this.getSelectedCellsIndices();
        int[] indices = new int[cellIndices.length];

        for(int i = 0; i<cellIndices.length; i++){
            indices[i] = cellIndices[i][1];
        }
        return indices;
    }

    /**
     * Returns an int array containing the indices of all selected rows.
     *
     * @return The indices of the selected rows.
     */
    public int[] getSelectedRowsIndices(){
        if(this.fxTableView.getSelectionModel() != null){
            javafx.collections.ObservableList<Integer> selectedIndices = this.fxTableView.getSelectionModel().getSelectedIndices();
            Integer[] indices = selectedIndices.toArray(new Integer[selectedIndices.size()]);
            int[] intIndices = java.util.Arrays.stream(indices).mapToInt(Integer::intValue).toArray();
            return intIndices;
        }
        return new int[0];
    }

    /**
     * Returns the indices of all selected cells. It is an nx2 int-array. Each line of the array holds the row index and the 
     * column index of a cell. E.g. if arr is the array, arr[3][0] describes the row index of the fourth selected cell and 
     * arr[10][1] describes the column index of the eleventh selected cell.
     *
     * @return The indices of all selected cell.
     */
    public int[][] getSelectedCellsIndices(){
        if(this.fxTableView.getSelectionModel() != null){
            javafx.collections.ObservableList<javafx.scene.control.TablePosition> cells = this.fxTableView.getSelectionModel().getSelectedCells();
            int[][] indices = new int[cells.size()][2];
            int i = 0;
            for(javafx.scene.control.TablePosition cell : cells){
                indices[i][0]= cell.getRow();
                indices[i][1]= cell.getColumn();
                i++;
            }
            return indices;
        }
        return new int[0][2];
    }

    /**
     * Returns the currently selected row as string array. If ther is no selection, an empty array (length is 0)
     * will be returned. If multi-selection is enabled, the the row with the lowest index is returned. You can 
     * use getSelectedRows() to get all selected rows, if you enabled multi-selection.
     * 
     * @return The selected row as string array.
     */
    public String[] getSelectedRow(){
        if(this.getSelectedRowIndex() != -1){
            return this.getRow(this.getSelectedRowIndex());
        }else{
            return new String[0];
        }
    }

    /**
     * Returns returns the currently selected rows as 2-d string array. The first element (array[0]) represents 
     * the first selected row, the second element (array[1]), represents the second selected row, and so on.
     * The array is empty (length = 0) if there is no selection.
     *
     * @return The selected rows as 2-d string array.
     */
    public String[][] getSelectedRows(){
        int[] rowIndices = this.getSelectedRowsIndices();
        String[][] rows = new String[rowIndices.length][this.getColumnCount()];
        for(int i=0; i<rowIndices.length;i++){
            rows[i] = this.getRow(rowIndices[i]);
        }
        return rows;
    }

    public String[] getSelectedColumn(){
        if(this.getSelectedColumnIndex() != -1){
            return this.getColumn(this.getSelectedColumnIndex());
        }else{
            return new String[0];
        }
    }

    public String[][] getSelectedColumns(){
        int[] columnIndices = this.getSelectedColumnsIndices();
        String[][] columns = new String[columnIndices.length][this.getRowCount()];
        for(int i=0; i<columnIndices.length;i++){
            columns[i] = this.getColumn(columnIndices[i]);
        }
        return columns;
    }

    public String[] getSelectedItems(){
        int[][] itemsIndices = this.getSelectedCellsIndices();
        String[] items = new String[itemsIndices.length];
        for(int i = 0; i< itemsIndices.length; i++){
            items[i] = this.getItem(itemsIndices[i][0], itemsIndices[i][1]);
        }
        return items;
    }

    public String getSelectedItem(){
        String item = this.getItem(this.getSelectedRowIndex(), this.getSelectedColumnIndex());
        if(item == null){
            return "";
        }else{
            return item;
        }

    }

    /**
     * Returns true, if the specified cell is selected or not. If row selection is enabled, it will return true,
     * if the specified row is selected, no matter what pColumn is.
     *
     * @param pRow Ein Parameter
     * @param pColumn Ein Parameter
     * @return Der Rückgabewert
     */
    public boolean isSelected(int pRow, int pColumn){
        try{
            return this.fxTableView.getSelectionModel().isSelected(pRow, this.fxTableView.getColumns().get(pColumn));
        }catch(Exception e){
            return false;
        }
    }

    /**
     * Returns true, if the specified row currently is selected.
     *
     * @param pRow The index of the row (first row has index 0)
     * @return true, if the row is selected.
     */
    public boolean isSelected(int pRow){
        if(this.fxTableView.getSelectionModel() != null){
            return this.fxTableView.getSelectionModel().isSelected(pRow);
        }
        return false;
    }

    /**
     * Set the width of the specified column to the specified width in pixel.
     *
     * @param pIndex The index of the row (starting with 0).
     * @param pWidth The new width of the row in pixel.
     */
    public void setColumnWidth(int pIndex, int pWidth){
        try{
            this.fxTableView.getColumns().get(pIndex).setPrefWidth(pWidth);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Set the width of every column to the specified width in pixel.
     *
     * @param pWidth The new width for every row in pixel.
     */
    public void setColumnWidth(int pWidth){
        for(int i = 0; i<this.getColumnCount();i++){
            try{
                this.fxTableView.getColumns().get(i).setPrefWidth(pWidth);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Increases or decreases the the width of each cell to fit all cells into the visible part of the table.
     *
     */
    public void autosize(){
        double overallWidth = 0;
        int columnCount = this.getColumnCount();
        for(int i=0; i<columnCount; i++){
            overallWidth += this.fxTableView.getColumns().get(i).getWidth();
        }
        double extraSpace = (this.getWidth()-overallWidth)/columnCount;
        for(int i=0; i<columnCount; i++){
            this.setColumnWidth(i,(int)(this.fxTableView.getColumns().get(i).getWidth()+extraSpace));
        }
    }

    /**
     * Selects the specified row
     *
     * @param pRow The index of the row (first row has index 0).
     */
    public void select(int pRow){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().select(pRow);
        }
    }

    /**
     * Selects the specified cell.
     *
     * @param pRow The index of the row (first row has index 0).
     * @param pColumn The index of the column (first column has index 0).
     */
    public void select(int pRow, int pColumn){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().select(pRow, this.fxTableView.getColumns().get(pColumn));
        }
    }

    /**
     * Selects all cells. This only works, if multi-selection is enabled (.enableMultiSelection())
     */
    public void selectAll(){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().selectAll();
        }
    }

    /**
     * Deselects the specified row.
     *
     * @param pRow The index of the row (first row has index 0)
     */
    public void deselect(int pRow){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().clearSelection(pRow);
        }
    }

    /**
     * Deselects the specified cell.
     *
     * @param pRow The index of the row (first row has index 0).
     * @param pColumn The index of the column (first column has index 0).
     */
    public void deselect(int pRow, int pColumn){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().clearSelection(pRow, this.fxTableView.getColumns().get(pColumn));
        }
    }

    /**
     * Removes every selection.
     */
    public void deselectAll(){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().clearSelection();
        }
    }

    /**
     * Disables selection of cells an rows. To enable selection use enableCellSelection() or enableRowSelection() to
     * enable selction.
     */
    public void disableSelection(){
        this.fxTableView.setSelectionModel(null);
    }

    /**
     * Enables selection of single cells. You can use enabelMultiSelect() to enable selecting 
     * of multiple cell.
     */
    public void enableCellSelection(){
        if(this.fxTableView.getSelectionModel() == null){
            this.fxTableView.setSelectionModel(this.defaultSelectionModel);
        }
        this.fxTableView.getSelectionModel().setCellSelectionEnabled(true);
    }

    /**
     * Enables selection of single rows. You can use enabelMultiSelect() to enable selecting 
     * of multiple cell.
     */
    public void enableRowSelection(){
        if(this.fxTableView.getSelectionModel() == null){
            this.fxTableView.setSelectionModel(this.defaultSelectionModel);
        }
        this.fxTableView.getSelectionModel().setCellSelectionEnabled(false);
    }

    /**
     * Enables multi selection of rows or cells. Depending if row or cell-selection is enabled (see enableCellSelection() 
     * and enableRowSelection())
     */
    public void enableMultiSelection(){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
        }
    }

    /**
     * Enables single selection of rows or cells (only one selection per at the same time).  Depending if row or cell-selection is enabled (see enableCellSelection() 
     * and enableRowSelection())
     */
    public void enableSingleSelection(){
        if(this.fxTableView.getSelectionModel() != null){
            this.fxTableView.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.SINGLE);
        }
    }

    /**
     * Makes the specified row of this table editable.
     *
     */
    public void enableEditable(int pRow){
        if(pRow < this.getColumnCount()){
            this.fxTableView.setEditable(true);
            javafx.scene.control.TableColumn<java.util.ArrayList<String>, String> column =(javafx.scene.control.TableColumn<java.util.ArrayList<String>, String>) this.fxTableView.getColumns().get(pRow);
            column.setCellFactory(javafx.scene.control.cell.TextFieldTableCell.forTableColumn());
            column.setOnEditCommit((event) -> {
                    this.setItem(this.getSelectedRowIndex(), this.getSelectedColumnIndex(), event.getNewValue()); // Put the new Value into the underlaying datas.
                    this.runMethodByName(this.onCellEdited);
                });
        }
    }

    public void enableEditable(){
        for(int i=0; i<this.getColumnCount();i++){
            this.enableEditable(i);
        }
    }

    /**
     * Makes the table read-only
     *
     */
    public void disableEditable(){
        this.fxTableView.setEditable(false);
    }

    /**
     * Sets the name of the method, which should be invoke,
     * if a selection was made.
     */
    public void setOnCellSelected(String pMethodName){
        this.onCellSelected = pMethodName;
    }

    /**
     * Sets the name of the method, which should be invoke,
     * if a cell was edited.
     */
    public void setOnCellEdited(String pMethodName){
        this.onCellEdited = pMethodName;
    }

    /**
     * Sets the name of the method, which should be invoke,
     * if the table data where changed.
     */
    public void setOnContentChanged(String pMethodName){
        this.onContentChanged = pMethodName;
    }

}
;