package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
import jltk.*;

/**
 * A slider ist used to select a numeric value  from a continuous or discrete range.
 * 
 * events: changed
 * 
 * @author M. Schulte
 * @version 19.05.2022
 */
public class Slider extends Control
{
    protected javafx.scene.control.Slider fxSlider;
    private String onChanged ="";

    /**
     * Creates the slider on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the slider
     * @param pHeight The height of the slider
     * @param pMin The minimal value of the slider
     * @param pMax The maximal value of the slider
     * @param pStep The amount to step by
     */
    public Slider(double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pInitialValue)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax, pInitialValue);
    }

    /**
     * Creates the spinner on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Slider(double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pInitialValue, double pStep)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax, pInitialValue, pStep);
    }

    /**
     * Creates the spinner at the specified position
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pWindow The window, the spinner should be placed
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Slider(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pInitialValue)
    {
        this.fxSlider = new javafx.scene.control.Slider(pMin, pMax, pInitialValue);
        // Vertical or horizontal
        if(pWidth < pHeight){
            this.fxSlider.setOrientation(javafx.geometry.Orientation.VERTICAL);   
        }
        this.fxSlider.valueProperty().addListener((observable, oldValue, newValue) ->
                this.runMethodByName(this.onChanged));
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxSlider);
    }

    /**
     * Creates the spinner at the specified position
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pWindow The window, the spinner should be placed
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Slider(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pInitialValue, double pStep)
    {
        this(pWindow, pLeft, pTop, pWidth, pHeight, pMin, pMax, pInitialValue);
        this.fxSlider.setBlockIncrement(pStep);

        
        
    }

    /**
     * Sets the name of the method, which will be invoked,
     * if the slider is changed
     */
    public void setOnChanged(String pMethodName){
        this.onChanged = pMethodName;
    }

    /**
     * Attempts to increment the slider by one step.
     *
     */
    public void increment(){
        this.fxSlider.increment();
    }

    /**
     * Attempts to increment the slider by one step.
     *
     */
    public void decrement(){
        this.fxSlider.decrement();
    }

    /**
     * Returns the current value as integer
     *
     * @return the current value of the slider
     */
    public int getValueAsInteger(){
        return (int) this.fxSlider.getValue();
    }

    /**
     * Returns the current value as double
     *
     * @return the current value of the slider
     */
    public double getValueAsDouble(){
        return  this.fxSlider.getValue();
    }

    /**
     * Sets the value of the slider to the specified value.
     *
     * @param pValue New value of the slider
     */
    public void setValue(double pValue){
        this.fxSlider.adjustValue(pValue);
    }

    /**
     * Show tick marks. 
     *
     */
    public void showTickMarks(){
        this.fxSlider.setShowTickMarks(true);
    }

    /**
     * Hide tick marks. 
     *
     */
    public void hideTickMarks(){
        this.fxSlider.setShowTickMarks(false);
    }

    /**
     * Show tick labels. 
     *
     */
    public void showTickLabels(){
        this.fxSlider.setShowTickLabels(true);
    }

    /**
     * Hide tick labels. 
     *
     */
    public void hideTickLabels(){
        this.fxSlider.setShowTickLabels(true);
    }

    /**
     * Sets the ditance between two labled ticks.
     *
     * @param pDistance The distance between two labled ticks.
     */
    public void setLabledTickDistance(double pDistance){
        this.fxSlider.setMajorTickUnit(pDistance);
    }

    /**
     * Set the amount of unlabled ticks between two labled
     * ticks.
     *
     * @param pCount The number of unlabled ticks.
     */
    public void setUnlabledTickCount(int pCount){
        this.fxSlider.setMinorTickCount(pCount);
    }

    /**
     * Enables "snap to tick. The value of the slider will
     * be align to a tick.
     */
    public void enableSnaping(){
        this.fxSlider.setSnapToTicks(true);
    }

    /**
     * Disables "snap to tick". The slider can be move
     * continously.
     */
    public void disableSnaping(){
        this.fxSlider.setSnapToTicks(false);
    }

    /**
     * Sets the minimum value represented by this slider
     *
     * @param pMin Slider minium value
     */
    public void setMinValue(double pMin){
        this.fxSlider.setMin(pMin);
    }

    /**
     * Sets the maximal value represented by this slider
     *
     * @param pMax Slider maximum value
     */
    public void setMaxValue(double pMax){
        this.fxSlider.setMax(pMax);
    }

    /**
     * Gets the minimum value represented by this slider
     *
     * @return Slider minimum value
     */
    public double getMinValue(){
        return this.fxSlider.getMin();
    }

    /**
     * Gets the maximal value represented by this slider
     *
     * @return Slider maximal value
     */
    public double getMaxValue(){
        return this.fxSlider.getMax();
    }

}
