package jltk.gui;
import jltk.*;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

/**
 * A checkbox is a small box which, can be checked or unchecked.
 * 
 * events: selcted, deselected
 *
 * 
 * @author M.Schulte    
 * @version 18.05.2022
 */
public class CheckBox extends Labeled
{
    protected javafx.scene.control.CheckBox fxCheckBox;
    private String onSelected ="";
    private String onDeselected ="";
    
    /**
     * Creates the check box
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the check box
     * @param pHeight The height of the check box
     * @param pText The label of the check box
     */
    public CheckBox(double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pText); //default/first window 
    }

     /**
     * Creates the check box
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the check box
     * @param pHeight The height of the check box
     * @param pWindow The window, the check box should be placed
     * @param pText The label of the check box
     */
    public CheckBox(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        this.fxCheckBox = new javafx.scene.control.CheckBox();
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxCheckBox);
        this.setText(pText);
         this.fxCheckBox.setOnAction((event) -> {
            if (this.fxCheckBox.isSelected()) {
                this.runMethodByName(this.onSelected);
            } else {
                this.runMethodByName(this.onDeselected);
            }
        });
    }
    
       
    /**
     * Sets the name of the method, which will be invoked,
     * if the checkbox is selected.      *
     */
    public void setOnSelected(String pMethodName){
        this.onSelected = pMethodName;
    }
    
    /**
     * Sets the name of the method, which will be invoked,
     * if the checkbox is selected.      *
     */
    public void setOnDeselected(String pMethodName){
        this.onDeselected = pMethodName;
    }

    
    /**
     * Returns true, if the check box is selected.
     *
     * @return 
     */
    public boolean isSelected(){
        return this.fxCheckBox.isSelected();
    }
    
    /**
     * Selects this check box.
     *
     */
    public void select(){
        this.fxCheckBox.setSelected(true);
    }
    
    /**
     * Deselects this check box.
     *
     */
    public void deselect(){
        this.fxCheckBox.setSelected(false);
    }
    
    /**
     * Toggles the check box (change the state)
     *
     */
    public void toggle(){
        this.fxCheckBox.fire();
    }
    
}
