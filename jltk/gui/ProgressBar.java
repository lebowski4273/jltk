package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
import jltk.*;

/**
 * A linear control which is used for indicating progress.  
 * Often used to indicate the progress of a task.
 * 
 * @author M. Schulte
 * @version 24.05.2022
 */
public class ProgressBar extends Control
{
    protected javafx.scene.control.ProgressBar fxProgressBar;
    private double min, max, value;

    /**
     * Creates the progress bar on the specified postion. The minimal value is 0 and the maximal value is 1.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the progress bar
     * @param pHeight The height of the progress bar
     */
    public ProgressBar(double pLeft, double pTop, double pWidth, double pHeight)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, 0, 1);
    }

    /**
     * Creates the progress bar on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the progress bar
     * @param pHeight The height of the progress bar
     * @param pMin The minimal value of the progress bar
     * @param pMax The maximal value of the progress bar
     */
    public ProgressBar(double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax);
    }

    /**
     * Creates the progress bar at the specified position
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the progress bar
     * @param pHeight The height of the progress bar
     * @param pWindow The window, the progress bar should be placed
     * @param pMin The minimal value of the progress bar
     * @param pMax The maximal value of the progress bar

     */
    public ProgressBar(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax)
    {
        this.fxProgressBar = new javafx.scene.control.ProgressBar(0);
        this.min = pMin;
        this.max = pMax;
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxProgressBar);
    }

    /**
     * Attempts to increment the progress bar by the given value
     *
     */
    public void increment(double pValue){
        this.setValue(this.getValue()+pValue);
    }

    /**
     * Attempts to decrement the progress bar by the given value.
     *
     */
    public void decrement(double pValue){
        this.setValue(this.getValue()-pValue);
    }

    /**
     * Returns the current value of the progress bar
     *
     * @return the current value of the progress bar
     */
    public double getValue(){
        return value;
    }

    /**
     * Sets the value of the progress bar to the specified value. 
     * The value mus between the minimal and maximal value of the progress bar.
     * A value smaller than the minmal value is interpreted as minimal value. 
     * A value greater than the maximum value is interpreted as maximum value.
     *
     * @param pValue New value of the progress bar.
     */
    public void setValue(double pValue){
        this.value = pValue;
        javafx.application.Platform.runLater(() ->this.fxProgressBar.setProgress(this.mapValue(pValue)));
        //System.out.println("Value: "+pValue +" Map: "+this.mapValue(pValue)+" Progress: "+this.fxProgressBar.getProgress()+"!!!");
        //javafx.application.Platform.runLater(() ->System.out.println("$$$"+this.fxProgressBar.getProgress()));
    }

    /**
     * Sets the minimum value represented by this progress bar
     *
     * @param pMin ProgressBar minium value
     */
    public void setMinValue(double pMin){
        this.min = pMin;
    }

    /**
     * Sets the maximal value represented by this progress bar
     *
     * @param pMax ProgressBar maximum value
     */
    public void setMaxValue(double pMax){
        this.max = pMax;
    }

    /**
     * Gets the minimum value represented by this progress bar
     *
     * @return ProgressBar minimum value
     */
    public double getMinValue(){
        return this.min;
    }

    /**
     * Gets the maximal value represented by this progress bar
     *
     * @return ProgressBar maximal value
     */
    public double getMaxValue(){
        return this.max;
    }

    /**
     * Maps a value between min and max value of this progress bar
     * to the corresponding value between 0 and 1
     */
    private double mapValue(double pValue){
        if(pValue < this.min){
            return 0;
        }
        if(pValue > this.max){
            return 1;
        }
        return 1.0*(pValue-this.min)/(this.max-this.min);
    }

}
