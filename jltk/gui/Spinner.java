package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
import jltk.*;

/**
 * A single line with an up and down arrow, that let you select a number or an object. 
 * A Spinner ca be editable.
 * 
 * events: changed
 * 
 * @author M. Schulte
 * @version 19.05.2022
 */
public class Spinner extends Control
{
    protected javafx.scene.control.Spinner fxSpinner;
    private javafx.scene.control.Spinner<Double> fxSpinnerDouble;
    private javafx.scene.control.Spinner<Integer> fxSpinnerInteger;
    private String onChanged ="";

    /**
     * Creates the spinner on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pStep The amount to step by
     */
    public Spinner(double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pStep)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax, pMin, pStep);
    }

    /**
     * Creates the spinner on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Spinner(double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pInitialValue, double pStep)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax, pInitialValue, pStep);
    }

    /**
     * Creates the spinner at the specified position
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pWindow The window, the spinner should be placed
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Spinner(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pStep)
    {
        this(pWindow, pLeft, pTop, pWidth, pHeight, pMin, pMax, pMin, pStep);
    }

    /**
     * Creates the spinner at the specified position
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pWindow The window, the spinner should be placed
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Spinner(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, double pMin, double pMax, double pInitialValue, double pStep)
    {
        this.fxSpinnerDouble = new javafx.scene.control.Spinner<Double>(pMin, pMax, pInitialValue, pStep);
        this.fxSpinner = this.fxSpinnerDouble;
        this.fxSpinner.valueProperty().addListener((observable, oldValue, newValue) ->
                this.runMethodByName(this.onChanged));
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxSpinner);
    }

    // The same for int values

    /**
     * Creates the spinner on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pStep The amount to step by
     */
    public Spinner(double pLeft, double pTop, double pWidth, double pHeight, int pMin, int pMax, int pStep)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax, pMin, pStep);
    }

    /**
     * Creates the spinner on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Spinner(double pLeft, double pTop, double pWidth, double pHeight, int pMin, int pMax, int pInitialValue, int pStep)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax, pInitialValue, pStep);
    }

    /**
     * Creates the spinner at the specified position
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pWindow The window, the spinner should be placed
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Spinner(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, int pMin, int pMax, int pStep)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, pMin, pMax, pMin, pStep);
    }

    /**
     * Creates the spinner at the specified position
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the spinner
     * @param pHeight The height of the spinner
     * @param pWindow The window, the spinner should be placed
     * @param pMin The minimal value of the spinner
     * @param pMax The maximal value of the spinner
     * @param pInitialValue  Value shown on start.
     * @param pStep The amount to step by
     */
    public Spinner(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, int pMin, int pMax, int pInitialValue, int pStep)
    {
        this.fxSpinnerInteger = new javafx.scene.control.Spinner<Integer>(pMin, pMax, pInitialValue, pStep);
        this.fxSpinner = this.fxSpinnerInteger;
        this.fxSpinner.valueProperty().addListener((observable, oldValue, newValue) ->
                this.runMethodByName(this.onChanged));
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxSpinner);
    }

    /**
     * Sets the name of the method, which will be invoked,
     * if the spinner is changed
     */
    public void setOnChanged(String pMethodName){
        this.onChanged = pMethodName;
    }
    
    
    /**
     * Attempts to increment the spinner by one step.
     *
     */
    public void increment(){
        this.fxSpinner.increment();
    }

    /**
     * Attempts to increment the spinner by the given number steps.
     */
    public void increment(int pSteps){
        this.fxSpinner.increment(pSteps);
    }

    /**
     * Attempts to increment the spinner by one step.
     *
     */
    public void decrement(){
        this.fxSpinner.decrement();
    }

    /**
     * Attempts to increment the spinner by the given number steps.
     */
    public void decrement(int pSteps){
        this.fxSpinner.decrement(pSteps);
    }

    /**
     * Returns the current value as integer
     *
     * @return the current value of the spinner
     */
    public int getValueAsInteger(){
        if(this.fxSpinnerInteger != null)
            return this.fxSpinnerInteger.getValue();
        else
            return this.fxSpinnerDouble.getValue().intValue();    
    }

    /**
     * Returns the current value as double
     *
     * @return the current value of the spinner
     */
    public double getValueAsDouble(){
       if(this.fxSpinnerInteger != null)
            return this.fxSpinnerInteger.getValue()*1.0;
        else
            return this.fxSpinnerDouble.getValue();   
    }
    
    /**
     * Methode setEditable
     *
     * @param pEditable Ein Parameter
     */
    /**
     * public void setEditable(boolean pEditable){
        this.fxSpinner.setEditable(pEditable);
    }**/

}