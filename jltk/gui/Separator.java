package jltk.gui;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
import jltk.*;
/**
 * A horizontal or vertical separator line.
 * 
 * events: <i>none</i>
 * 
 * @author M. Schulte
 * @version 24.08.2022
 */
public class Separator extends Control
{
    protected javafx.scene.control.Separator fxSeparator;

    
    /**
     * Create a new separator line.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the text field
     * @param pHeight The height of the text field
     */
    public Separator(double pLeft, double pTop, double pWidth, double pHeight)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight, ""); //default/first window 
    }


    /**
     * Creates an text field on the specified postion.
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the text field
     * @param pHeight The height of the text field
     * @param pWindow The window, the text field should be placed
     * @param pText The label of the text field
     */
    public Separator(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight, String pText)
    {
        if(pWidth > pHeight){
            this.fxSeparator = new javafx.scene.control.Separator(javafx.geometry.Orientation.HORIZONTAL);
        }else{
            this.fxSeparator = new javafx.scene.control.Separator(javafx.geometry.Orientation.VERTICAL);
        }
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxSeparator);
    }
    

    


}

