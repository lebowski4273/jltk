package jltk.gui;
import jltk.*;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

/**
 * A ListView contains objects line by line
 * 
 * events: itemSelected, itemEdited
 * 
 * @author Martin Schulte
 * @version 24.05.2022
 */
public class ListView extends Control
{
    protected javafx.scene.control.ListView<String> fxListView;
    private String onItemSelected ="";
    private String onItemEdited ="";
    /**
     * Create the ListView on the main View
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the combo box
     * @param pHeight The height of the combo box
     */
    public ListView(double pLeft, double pTop, double pWidth, double pHeight)
    {
        this(jltk.App.theApp.window, pLeft, pTop, pWidth, pHeight); //default/first window 
    }

    /**
     * Creates the ListView
     *
     * @param pLeft The horizontal coordinate
     * @param pTop The vertical coordinate
     * @param pWidth The width of the list
     * @param pHeight The height of the list
     * @param pWindow The window, the list should be placed
     */
    public ListView(Window pWindow, double pLeft, double pTop, double pWidth, double pHeight)
    {
        this.fxListView = new javafx.scene.control.ListView<String>();
        this.init(pLeft, pTop, pWidth, pHeight, pWindow, this.fxListView);
        this.fxListView.getSelectionModel().selectedItemProperty().addListener((javafx.beans.value.ObservableValue<? extends String> ov, String old_val, String new_val) -> {
                this.runMethodByName(this.onItemSelected);
            });
        this.fxListView.setOnEditCommit((event) -> {
                event.getSource().getItems().set(event.getIndex(), event.getNewValue());  // set the new value into the ListView
                this.runMethodByName(this.onItemEdited);

            });

    }

    /**
     * Appends pText to the ListView
     *
     * @param pText The object to be append.
     */
    public void append(String pText){
        this.fxListView.getItems().add(pText);

    }

    /**
     * Appends pObject to the ListView
     *
     * @param pObject The object to be append.
     */
    public void append(Object pObject){
        this.fxListView.getItems().add(pObject.toString());

    }

    /**
     * Appends the given String array to the ListView
     *
     * @param pArray a one-dimensiol string array
     */
    public void append(String[] pArray){
        for(String item : pArray){
            this.append(item);
        }
    }

    /**
     * Appends the given array of objects to the ListView
     *
     * @param pArray a one-dimensiol array
     */
    public void append(Object[] pArray){
        for(Object item : pArray){
            this.append(item.toString());
        }
    }

    /**
     * Inserts pText at the specified position in this ListView. Shifts the item currently at that position (if any) and any subsequent item to the bottom (adds one to their indices, first item has index 0).
     *
     * @param pText The text to be add.
     */
    public void insert(int pIndex, String pText){
        this.fxListView.getItems().add(pIndex, pText);

    }

    /**
     * Inserts the string representation of pObject (.toString()) at the specified position 
     * in this ListView. Shifts the item currently at that position (if any) and any subsequent 
     * item to the bottom (adds one to their indices, first item has index 0).
     *
     * @param pObject The object to add.
     */
    public void insert(int pIndex, Object pObject){
        this.fxListView.getItems().add(pIndex, pObject.toString());

    }

    /**
     * Replaces the item at the specified position in this ListView with pText (first item has index 0).
     *
     * @param pText The text to be set.
     */
    public void set(int pIndex, String pText){
        this.fxListView.getItems().set(pIndex, pText);

    }

    /**
     * Replaces the item at the specified position in this ListView with the string representation 
     * of pObject (.toString(), first item has index 0).
     *
     * @param pText The object to be set.
     */
    public void set(int pIndex, Object pObject){
        this.fxListView.getItems().set(pIndex, pObject.toString());

    }

    /**
     * Remove the item at the specified index (first item has index 0).
     *
     * @param pIndex the Index of the item, should be removed.
     */
    public void remove(int pIndex){
        this.fxListView.getItems().remove(pIndex);
    }

    /**
     * Removes all entries from the ListView
     *
     */
    public void removeAll(){
        this.fxListView.getItems().clear();

    }

    /**
     * Selects (highlights) the item specified by pIndex
     *
     * @param pIndex 
     */
    public void select(int pIndex){
        this.fxListView.getSelectionModel().select(pIndex);
    }

    /**
     * Deselects the item specified by pIndex
     *
     * @param pIndex 
     */
    public void deselect(int pIndex){
        this.fxListView.getSelectionModel().clearSelection(pIndex);
    }

    /**
     * Clears the selection of all selected indices. 
     */
    public void deselectAll(){
        this.fxListView.getSelectionModel().clearSelection();
    }

    /**
     * Returns true, if the given index is currently selected, otherwise false.
     *
     * @param pIndex The index to check as to whether it is currently selected or not
     * @return True if the given index is selected, false otherwise
     */
    public boolean isSelected(int pIndex){
        return this.fxListView.getSelectionModel().isSelected(pIndex);
    }

    /**
     * Returns the index of the selected item (first item has index 0).
     *
     * @return The index of the selected item
     */
    public int getSelectedIndex(){
        return this.fxListView.getSelectionModel().getSelectedIndex();
    }

    /**
     * Returns an int array containing all indices of the selected items (first item has index 0).
     *
     * @return int array with indices of the selected items.
     */
    public int[] getSelectedIndices(){

        javafx.collections.ObservableList<Integer> lIndices = this.fxListView.getSelectionModel().getSelectedIndices();
        int[] lSelectedIndices = new int[lIndices.size()];
        for(int i = 0; i<lSelectedIndices.length;i++){
            lSelectedIndices[i] = lIndices.get(i).intValue();
        }
        return lSelectedIndices;
    }

    /**
     * Returns the number of items in this ListView.
     *
     * @return The number of items.
     */
    public int getSize(){
        return this.fxListView.getItems().size();
    }

    /**
     * Returns the selected item as string.
     *
     * @return The string representation of the item.
     */
    public String getSelectedItem(){
        try{
            return this.fxListView.getSelectionModel().getSelectedItem().toString();
        }catch(Exception e){
            return "";
        }
    }

    /**
     * Return selected items as String array
     *
     * @return Selected items as String array
     */
    public String[] getSelectedItems(){
        //see https://docs.oracle.com/javase/8/docs/api/java/util/List.html?is-external=true#toArray-T:A-
        return this.fxListView.getSelectionModel().getSelectedItems().toArray(new String[0]);
    }

    /**
     * Return all items as String array
     *
     * @return All items as String array
     */
    public String[] getAllItems(){
        //see https://docs.oracle.com/javase/8/docs/api/java/util/List.html?is-external=true#toArray-T:A-
        return this.fxListView.getItems().toArray(new String[0]);
    }

    /**
     * Set the name of the method, which should be invoke,
     * if an item of the ListView is selected.
     *
     */
    public void setOnItemSelected(String pMethodName){
        this.onItemSelected = pMethodName;
    }

    /**
     * Set the name of the method, which should be invoke,
     * if the edit of an item of the ListView is comitted.
     *
     */
    public void setOnItemEdited(String pMethodName){
        this.onItemEdited = pMethodName;
    }

    //public void select()

    /**
     * Allows one or more items to be selected at a time.
     *
     */
    public void enableMultiselect(){
        this.fxListView.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.MULTIPLE);
    }

    /**
     * Allows one item to be selected at a time.
     *
     */
    public void disableMultiselect(){
        this.fxListView.getSelectionModel().setSelectionMode(javafx.scene.control.SelectionMode.SINGLE);
    }

    /**
     * Makes the ListView editable
     *
     */
    public void enableEditable(){
        this.fxListView.setEditable(true);
        this.fxListView.setCellFactory(javafx.scene.control.cell.TextFieldListCell.forListView());
    }

    /**
     * Makes the LiestView read-only
     *
     */
    public void disableEditable(){
        this.fxListView.setEditable(false);
    }

}
