package jltk.util;

import javafx.scene.text.*;
import java.util.List;

/**
 * Miscellaneous static helper functions
 * 
 * @author M. Schulte
 * @version 02.06.2022
 */
public class Misc
{

    public Misc()
    {
 
    }

    
    /**
     * Print all available fonts on your system.
     *
     */
    public static void printAvailableFontNames(){
        List<String> fonts = Font.getFamilies();
        for(String font : fonts){
            System.out.println(font);
        }
    }

}
