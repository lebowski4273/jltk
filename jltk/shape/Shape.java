package jltk.shape;

import jltk.Window;
import jltk.App;
import jltk.Component;
import javafx.application.Platform;
import java.util.concurrent.CountDownLatch;

public abstract class Shape extends Component
{

    public javafx.scene.shape.Shape fxShape; // The corresponing shape from Javafx
    protected javafx.scene.paint.Color lineColor;
    protected javafx.scene.paint.Color fillColor = javafx.scene.paint.Color.BLACK;

    protected double transparancy = 0;
    protected javafx.scene.transform.Rotate rotate;
    protected javafx.scene.transform.Scale scale;

    protected double xOffset = 0;
    protected double yOffset = 0;
    
    protected double x;
    protected double y;
    

    /**
     * Basic initialisation, like setting postion and size, add the Element to the window. 
     * For some reason this cannot be solved using a constructor, because in subclasses the javaFX
     * component must be initialised first. After this, init must be called.
     *
     * @param pX The horizontal coordinate
     * @param pY The vertical coordinate
     * @param pWindow The window on which the component should be placed
     * @param pShape The corresponding JavaFX-Component
     */
    protected void init(double pX, double pY, Window pWindow, javafx.scene.shape.Shape pShape)
    {
        this.fxShape = pShape;
        this.myWindow = pWindow;
        this.setPosition(pX,pY);
        this.setTransparancy(this.transparancy);
        this.setFillColor(this.fillColor);
        this.setLineWidth(1); // Width 1 but no color
        this.rotate = new javafx.scene.transform.Rotate(); // for turn and setRotation
        this.scale = new javafx.scene.transform.Scale(); // for scaling an flipping
        this.fxShape.getTransforms().addAll(this.rotate,this.scale);
        super.init(this.fxShape); // init of Component (focus eventhandler ...)
        this.myWindow.addComponent(this.fxShape);
    }
    /*
    /**
     * Return the x coordinate of the center of this shape. This comes up to the
     * x-coordinate of the midpoint of the boundingbox.
     *
     * @return The central x coordinate of this shape.
     *
    public double getCenterX(){
        return this.fxShape.getBoundsInParent().getCenterX();
    }

    /**
     * Returns the y coordinate of the center of this shape. This comes up to the
     * y-coordinate of the midpoint of the boundingbox.
     *
     * @return The central y coordinate of this shape.
     *
    public double getCenterY(){
        return this.fxShape.getBoundsInParent().getCenterY();
    }
    */

    /**
     * Return the x coordinate of this shape. It depends on the shape, what this coordinates discribe:
     * <ul>
     * <li>Circle: The center of the circle</li>
     * <li>Rectangle: The upper left corner. (Rotated rechtangle: upper left corner of the bounding box)</li>
     * </ul>
     * @return The x coordinate of upper left corner
     */
    public double getX(){
        //return this.fxShape.getBoundsInParent().getMinX();
        return this.x;
    }

    /**
     * Return the y coordinate of upper left corner of the bounding box
     * of this shape
     *
     * @return The y coordinate of upper left corner
     */
    public double getY(){
        //return this.fxShape.getBoundsInParent().getMinY();
        return this.y;
    }

    /**
     * Return the width of this shape. This comes up
     * to the width of the minimal rectangle surrounding this shape.
     *
     * @return The width of the shape.
     */
    public double getWidth(){
        return this.fxShape.getBoundsInParent().getWidth();
    }

    /**
     * Return the height of this shape. This comes up
     * to the height of the minimal rectangle surrounding this shape.
     *
     * @return The height of the shape.
     */
    public double getHeight(){
        return this.fxShape.getBoundsInParent().getHeight();
    }

    /**
     * Set the upper left corner of the shape to the specified position. 
     * If you might set the positon of the center of this shape, use #setCenterPosition(double, double)
     *
     * @param pX The x coordinate of the new position
     * @param pY The y coordinate of the new position
     * @see #setCenterPosition(double,double)
     * @see #getX()
     * @see #getY()
     */
    public void setPosition(double pX, double pY){
        //relocate(...) is related to the upp left corner of the non-rotated and non-scaled 
        // shape. E.G.: if you rotate an shape by 20 degrees and the run relocate(100,100), 
        // the upper left corner of the unrotated shape will be placed in (100/100). The visible rotated 
        // shape will be placed arround (87.53/95,57). Because of this, we have to clculate the
        // x- and y-offset everytime the shape is turned or scaled (see turn(double,double,double) and
        // scale(double,double))
        //System.out.println("px: "+pX+" pY: "+pY);
        //System.out.println("pxM: "+(pX-this.xOffset)+" pY: "+(pY-this.yOffset));
        final CountDownLatch wait = new CountDownLatch(1);
        Platform.runLater(
            () -> {
                //this.fxShape.relocate(pX+this.xOffset,pY+this.yOffset);
                this.fxShape.setLayoutX(pX+this.xOffset);
                this.fxShape.setLayoutY(pY+this.yOffset);
                this.x = pX+this.xOffset;
                this.y = pY+this.yOffset;
                wait.countDown();
                this.delay(1);
            }
        );
        try
        {
            wait.await();
        }
        catch (InterruptedException ie)
        {
            ie.printStackTrace();
        }
    }

    /**
     * Set the center of the shape to the specified position. 
     * If you might set the positon of the upper left corner, use #setPosition(double, double)
     *
     * @param pX The x coordinate of the new position
     * @param pY The y coordinate of the new position
     * @see #setPosition(double,double)
     */
    public void setCenterPosition(double pX, double pY){
        this.setPosition(pX-this.getWidth()/2,pY-this.getHeight()/2);
    }

    /**
     * The fill color of the shape will be changed to the specified color
     *
     * @param pColor the new fill color
     */
    public void setFillColor(javafx.scene.paint.Color pColor){
        this.fillColor = pColor;
        this.fxShape.setFill(this.fillColor);
    }

    /**
     * The fill color of the shape will be changed to the specified color. As parameter you can use the color 
     * name from the javafx.scene.paint.Color field https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html#field.summary
     * For example setFillColor("ALICEBLUE") or setFillColor("blue").
     * 
     * @param pColor the new fill color
     */
    public void setFillColor(String pColor){
        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            javafx.scene.paint.Color lColor = (javafx.scene.paint.Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.setFillColor(lColor);
            }
        }catch(Exception e){
            System.out.println("setFillColor: "+pColor+" is not a valid color.");
        }
    }

    /**
     * The fillcolor of the shape will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setFillColor(double pRed, double pGreen, double pBlue){
        this.setFillColor(javafx.scene.paint.Color.color(pRed, pGreen, pBlue));
    }

    /**
     * Set the transparancy oh this shape to the specified value. pValue mus between 0.0 (not transparent)
     * and 1.0 (transparent)
     *
     * @param pValue The new transparancy (between 0.0 and 1.0)
     * @see #getTransparancy()
     */
    public void setTransparancy(double pValue){
        try{
            this.fillColor = javafx.scene.paint.Color.color(
                ((javafx.scene.paint.Color)(this.fillColor)).getRed(),
                ((javafx.scene.paint.Color)(this.fillColor)).getGreen(),
                ((javafx.scene.paint.Color)(this.fillColor)).getBlue(),
                1-pValue);
            this.setFillColor(this.fillColor);
            this.transparancy = pValue;
        }catch(Exception e){
            // Skip Exception
            e.printStackTrace();
        }
    }

    /**
     * Return the transparancy of this shape. 0.0 means not transparent  
     * and 1.0 means full transparent
     *
     * @return The transparency of this shape
     */
    public double getTransparancy(){
        return this.transparancy;
    }

    public void flipHorizontal(){
        // double rotationRad = this.getRotation() * Math.PI/180;
        // this.scale.setY(-1*Math.sin(rotationRad)); 
        // this.scale.setX(1*Math.cos(rotationRad)); 
        // scale.setPivotX(this.getCenterX());
        // scale.setPivotY(this.getCenterY());
    }

    public void flipVertical(){

    }

    /**
     * Moves this shape by the specified distance into the current direction,
     * see {@link setRotation(double)}.
     *
     * @param pDistance The distance in pixel.
     */
    public void move(double pDistance){
        double rotationRad = this.getRotation() * Math.PI/180;
        double xNew = pDistance*Math.cos(rotationRad);
        double yNew = pDistance*Math.sin(rotationRad);
        //System.out.println("rad: "+rotationRad+" xNew: "+xNew+" yNew: "+yNew);
        this.move(xNew, yNew);
    }

    /**
     * Move this shape by the specified x- and y-sections.
     *
     * @param pX The x distance in pixel.
     * @param pY The y distance in pixel.
     */
    public void move(double pX, double pY){
        // Make coordinates absolute
        double xNew = this.getX()+pX;
        double yNew = this.getY()+pY;
        this.setPosition(xNew, yNew);
    }

    /**
     * Scale the shape with the specified factors. The scale centrum is
     * the central position of this shape, {@link #getCenterX()} and {@link #getCenterY()}
     *
     * @param pX The scale factor for the x direction
     * @param pY The scale factor for the y direction
     */
    public void scale(double pScaleX, double pScaleY){
        // Prepare the scale object
        this.scale.setPivotX(this.fxShape.getBoundsInLocal().getCenterX());
        this.scale.setPivotY(this.fxShape.getBoundsInLocal().getCenterX());
        double xBefore = this.getX();
        double yBefore = this.getY();
        this.scale.setX(pScaleX*this.scale.getX()); 
        this.scale.setY(pScaleY*this.scale.getY()); 
        // We've to calculate the offset (see setPosition(double,double))
        this.xOffset = xBefore-this.getX();
        this.yOffset = yBefore-this.getY();
    }
    

    /**
     * Scale the shape to the specified width and height.
     *
     * @param pWidth The new width of the shape.
     * @param pHeight The ne height of the shape.
     */
    public void scaleTo(double pWidth, double pHeight){
        double factorX = pWidth/this.getWidth();
        double factorY = pHeight/this.getHeight();
        this.scale(factorX,factorY);
    }

    /**
     * Set the rotation of this shape to the specified angle. Rotation is expressed as a degree value. 
     * Zero degrees is to the east (right-hand side of the window), and the angle increases clockwise.
     *
     * @param pAngle The angle in degrees
     */
    public void setRotation(double pAngle){
        this.turn(this.fxShape.getBoundsInLocal().getCenterX(),this.fxShape.getBoundsInLocal().getCenterY(),pAngle-this.getRotation());
        //this.fxShape.setRotate(pAngle);
    }

    /**
     * Return the current rotation of this shape. Rotation is expressed as a degree value. 
     * Zero degrees is to the east (right-hand side of the window), and the angle increases clockwise.
     *
     * @return The current rotation
     */
    public double getRotation(){
        return this.rotate.getAngle();
    }

    /**
     * Turn this shape by the specified angle (in degrees). 
     * Zero degrees is to the east (right-hand side of the window), and the angle increases clockwise.
     *
     * @param pAngle The angle in degrees; positive values turn clockwise
     */
    public void turn(double pAngle){
        // // Turning arround the center
        this.turn(this.fxShape.getBoundsInLocal().getCenterX(),this.fxShape.getBoundsInLocal().getCenterY(),pAngle);
    }

    /**
     * Turn this schape by the specified angle (in degrees) arround the specified turning center
     * Zero degrees is to the east (right-hand side of the window), and the angle increases clockwise.
     *
     * @param pX The x-coordinate of the turning point.
     * @param pY The y-coordinate of the turning point.
     * @param pAngle The angle in degrees; positive values turn clockwise 
     */
    public void turn(double pX, double pY, double pAngle){
        // Set pivot point to (pX/pY) and angle to pAngle.
        this.rotate.setPivotX(pX);
        this.rotate.setPivotY(pY);
        double xBefore = this.getX();
        double yBefore = this.getY();
        final CountDownLatch wait = new CountDownLatch(1);
        Platform.runLater(
            () -> {
                this.rotate.setAngle(pAngle+this.getRotation());
                wait.countDown();
                // We've to calculate the offset (see setPosition(double,double))
                this.xOffset = xBefore-this.getX();
                this.yOffset = yBefore-this.getY();
                //System.out.println("---"+xOffset+"----"+xOffset);
            }
        );
        try
        {
            wait.await();
        }
        catch (InterruptedException ie)
        {
            ie.printStackTrace();
        }
        this.myWindow.delay(1); 
    }

    /**
     * Return true, if this shape intersects the specified shape.
     *
     * @param pShape Another shape
     * @return true, if intersects, else false.
     */
    public boolean intersects(Shape pShape){
        return Shape.intersects(this,pShape);
    }

    /**
     * Return true, if the specified shapes intersects
     *
     * @param pShape1 The first shape
     * @param pShape2 The second shape
     * @return true, is shape1 and shape2 intersect.
     */
    public static boolean intersects(Shape pShape1, Shape pShape2){
        //Create a new shape from intersection, if width -1, there is no intersecting shape
        javafx.scene.shape.Shape intersectShape = javafx.scene.shape.Shape.intersect(pShape1.fxShape, pShape2.fxShape);
        return intersectShape.getBoundsInLocal().getWidth() != -1;
    }

    /**
     * Return true, if this shape fully includes the specified shape.
     *
     * @param pShape Another shape
     * @return true, if this shape contains the given. else false.
     */
    public boolean contains(Shape pShape){
        return Shape.contains(this, pShape);
    }

    /**
     * Return true, if pOuter fully includes pInner.
     *
     * @param pOuter The outer shape
     * @param pInner The inner shape
     * @return true, if pOuter contains pInner.
     */
    public static boolean contains(Shape pOuter, Shape pInner){
        // Create a new shape from intersection. If pOuter contains pInner, the width an height
        // of this new shape an pInner must be the same
        javafx.scene.shape.Shape intersectShape = javafx.scene.shape.Shape.intersect(pInner.fxShape, pOuter.fxShape);
        return intersectShape.getBoundsInParent().getWidth() == pInner.getWidth() &&
        intersectShape.getBoundsInParent().getHeight() == pInner.getHeight();
    }

    /**
     * Return true if the specified point is inside this shape.
     */
    public boolean contains(double pX, double pY){
        return this.fxShape.contains(pX, pY);
    }

    /**
     * Set the width of the line/border to the specified value.
     *
     * @param pLineWidth The new line width
     */
    public void setLineWidth(double pLineWidth){
        this.fxShape.setStrokeWidth(pLineWidth);
    }

    /**
     * Return the current used linewidth
     */
    public double getLineWidth(){
        return this.fxShape.getStrokeWidth();
    }

    /**
     * The line color of the shape will be changed to the specified color
     *
     * @param pColor The new line color
     */
    public void setLineColor(javafx.scene.paint.Color pColor){
        this.lineColor = pColor;
        this.fxShape.setStroke(pColor);
    }

     
    /**
     * The line color of the shape will be changed to the specified color. As parameter you can use the color 
     * name from the javafx.scene.paint.Color field https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html#field.summary
     * For example setFillColor("ALICEBLUE") or setFillColor("blue").
     * 
     * @param pColor the new line color
     */
    public void setLineColor(String pColor){
        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            javafx.scene.paint.Color lColor = (javafx.scene.paint.Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.setLineColor(lColor);
            }
        }catch(Exception e){
            System.out.println("setLineColor: "+pColor+" is not a valid color.");
        }
    }

    /**
     * The linecolor of the shape will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setLineColor(double pRed, double pGreen, double pBlue){
        this.setLineColor(javafx.scene.paint.Color.color(pRed, pGreen, pBlue));
    }
    
    /**
     * Delay execution for this shape for the specfied number of milliseconds
     *
     * @param pMilliseconds Ein Parameter
     */
    public void delay(long pMilliseconds)
    {
        try
        {
            Thread.sleep(pMilliseconds);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

}
