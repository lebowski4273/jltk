package jltk.shape;

import jltk.Window;

/**
 * Beschreiben Sie hier die Klasse Circle.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Circle extends Shape
{
    protected javafx.scene.shape.Circle fxCircle;
    
    /**
     * Creates a new circle width the specified radius. The circle is centered on the specifed position.
     *
     * @param pX The x-coordinate of the center
     * @param pY The y-coordinate of the center
     * @param pRadius The radius of the circle
     */
    public Circle(double pX, double pY, double pRadius)
    {
        this(pX,pY,pRadius,Window.firstWindow); // if no Window specified, use first window
    }
    
    /**
     * Creates a new circle width the specified radius. It is placed on the specified windows. The circle is 
     * centered on the specifed position.
     *
     * @param pX The x-coordinate of the center
     * @param pY The y-coordinate of the center
     * @param pRadius The radius of the circle
     * @param pWindow The windows, the circle should place
     */
    public Circle(double pX, double pY, double pRadius, Window pWindow)
    {
        this.fxCircle = new javafx.scene.shape.Circle(pRadius);
        this.init(pX-pRadius,pY-pRadius,pWindow, this.fxCircle);
        //this.setPosition(pX,pY);
    }
    
    /**
     * Set the new radius of this circle.
     *
     * @param pRadius The new radius
     */
    public void setRadius(double pRadius){
        this.fxCircle.setRadius(pRadius);
    }
    
    /**
     * Return the radius of this cricle
     *
     * @return The radius of this circle.
     */
    public double getRadius(){
        return this.fxCircle.getRadius();
    }

    
}
