package jltk.shape;

import jltk.Window;

/**
 * Beschreiben Sie hier die Klasse Rectangle.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum) 
 */
public class Rectangle extends Shape
{
    protected javafx.scene.shape.Rectangle fxRect;
    
    /**
     * Creates a new rectangle width the specified width and height. The specified position describes the left 
     * upper corner of this rectangle.
     *
     * @param pX The x-coordinate of the left upper corner
     * @param pY The y-coordinate of the left upper corner
     * @param pWidth The width of this rectangle
     * @param pHeight The height of tis rectangle
     */
    public Rectangle(double pX, double pY, double pWidth, double pHeight)
    {
        this(pX,pY,pWidth,pHeight,Window.firstWindow); // if no Window specified, use first window
    }
    
    /**
     * Creates a new rectangle width the specified width and height. It is placed on the specified window
     * The specified position describes the left upper corner of this rectangle.
     *
     * @param pX The x-coordinate of the left upper corner
     * @param pY The y-coordinate of the left upper corner
     * @param pWidth The width of this rectangle
     * @param pHeight The height of tis rectangle
     * @param pWindow The windows, the rectangle should place
     */
    public Rectangle(double pX, double pY, double pWidth, double pHeight , Window pWindow)
    {
        this.fxRect = new javafx.scene.shape.Rectangle(pWidth,pHeight);
        this.init(pX,pY,pWindow, this.fxRect);
    }
    
    /**
     * Set the new width for this rectangle
     *
     * @param pWidth The new width
     */
    public void setWidth(double pWidth){
        this.fxRect.setWidth(pWidth);
    }
    
     /**
     * Set the new Height for this rectangle
     *
     * @param pWidth The new height
     */
    public void setHeight(double pHeight){
        this.fxRect.setHeight(pHeight);
    }
    
    

    
}
