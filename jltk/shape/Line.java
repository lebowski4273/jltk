package jltk.shape;

import jltk.Window;

/**
 * Beschreiben Sie hier die Klasse Line.
 * 
 * @author M. Schulte
 * @version 1.0
 */
public class Line extends Shape
{
    protected javafx.scene.shape.Line fxLine;
    
    /**
     * Creates a new line from (pStartX|pStartY) to (pEndX|pEndY). The specified position describes the left 
     * upper corner of this rectangle.
     *
     * @param pStartX The x-coordinate of the starting point
     * @param pStartY The y-coordinate of the starting point
     * @param pEndX The x-coordinate of the endpoint
     * @param pEndY The y-coordinate of the endpoint
     */
    public Line(double pStartX, double pStartY, double pEndX, double pEndY)
    {
        this(pStartX,pStartY,pEndX,pEndY,Window.firstWindow); // if no Window specified, use first window
    }
    
    /**
     * Creates a new rectangle width the specified width and height. It is placed on the specified window
     * The specified position describes the left upper corner of this rectangle.
     *
     * @param pX The x-coordinate of the left upper corner
     * @param pY The y-coordinate of the left upper corner
     * @param pWidth The width of this rectangle
     * @param pHeight The height of tis rectangle
     * @param pWindow The windows, the rectangle should place
     */
    public Line(double pStartX, double pStartY, double pEndX, double pEndY, Window pWindow)
    {
        this.fxLine = new javafx.scene.shape.Line(pStartX,pStartY,pEndX,pEndY);
        this.init(pStartX,pStartY,pWindow, this.fxLine);
    }
    
    /**
     * Set the new x coordinate for the start point
     *
     * @param pWidth The x coordinate for the start point
     */
    public void setStartX(double pValue){
        double x = this.fxLine.sceneToLocal(pValue, this.getY()).getX();
        System.out.println("--"+x);
        this.fxLine.setStartX(x);
    }
    
    /**
     * Set the new y coordinate for the start point
     *
     * @param pWidth The y coordinate for the start point
     */
    public void setStartY(double pValue){
        double y = this.fxLine.parentToLocal(0,pValue).getY();
        this.fxLine.setStartY(y);
    }
    
    /**
     * Set the new x coordinate for the end point
     *
     * @param pWidth The x coordinate for the end point
     */
    public void setEndX(double pValue){
        this.fxLine.setEndX(pValue);
    }
    
    /**
     * Set the new y coordinate for the end point
     *
     * @param pWidth The y coordinate for the end point
     */
    public void setEndY(double pValue){
        this.fxLine.setEndY(pValue);
    }
    
    /**
     * Methode setPosition
     *
     * @param pX Ein Parameter
     * @param pY Ein Parameter
     */
    public void setPosition(double pX, double pY){
        // Move start and end point
        double xDiff = this.getWidth();
        double yDiff = this.getHeight();
        this.setStartX(pX);
        this.setStartY(pY);
        this.x=pX;
        this.y=pY;
        this.setEndX(pX+xDiff);
        this.setEndY(pY+yDiff);
        
    }
    
    /**
     * Return the x coordinate of the starting point.
     *
     * @return The x coordinate of the starting point
     */
    public double getStartX(){
        // We have to recognize rotatet lines an transform ther coordinate to scene
        return this.fxLine.localToParent(this.fxLine.getStartX(), this.fxLine.getStartY()).getX();
        //return this.fxLine.getStartX();
    }
    
     /**
     * Return the y coordinate of the starting point.
     *
     * @return The y coordinate of the starting point
     */
    public double getStartY(){
        return this.fxLine.localToParent(this.fxLine.getStartX(), this.fxLine.getStartY()).getY();
    }
    
    /**
     * Return the x coordinate of the end point.
     *
     * @return The x coordinate of the end point
     */
    public double getEndX(){
        return this.fxLine.localToParent(this.fxLine.getEndX(), this.fxLine.getEndY()).getX();
    }
    
    /**
     * Return the y coordinate of the end point.
     *
     * @return The y coordinate of the end point
     */
    public double getEndY(){
        return this.fxLine.localToParent(this.fxLine.getEndX(), this.fxLine.getEndY()).getY();
    }
    
    /**
     * Return the width of this line
     *
     * @return The width of this line.
     */
    public double getWidth(){
        return this.getEndX()-this.getStartX();
    }
    
    /**
     * Return the height of this line
     *
     * @return The height of this line.
     */
    public double getHeight(){
        return this.getEndY()-this.getStartY();
    }
    
    public void turn(double pAngle){
        super.turn(pAngle);
        this.x = this.getStartX();
        this.y = this.getStartY();
    }
    
    

    
}
