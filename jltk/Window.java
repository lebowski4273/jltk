
package jltk;

/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
// Swing (we need a JFrame) to avoid to run this as JavaFx application
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Dimension;

// Everything in the JFrame ist from JavaFX
import javax.swing.JPanel;
import javafx.collections.ObservableList;
import javafx.application.Platform;
import java.util.concurrent.CountDownLatch;
import javafx.embed.swing.JFXPanel;
import javafx.scene.layout.Pane;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.*;
import javafx.scene.Group;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.scene.paint.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.geometry.Insets;
// JavaFX events
import javafx.event.*;
import javafx.beans.value.*;
import javafx.scene.input.*;
import javafx.animation.*;

// Utils
import java.util.ArrayList;
import java.util.Vector;

/**
 * A object of this class represents a window of an graphical user interface (gui). On this window you can draw
 * with a pen or you can place gui elements like button, textfields, et cetera. (See class App for more information
 * how to use gui elements).<br/>
 * For this purpose the window use a coordination system. The origin of the coordinate system is the upper left corner 
 * of the window. The x-axis points to the right, the y-axis points down. 
 * 
 * @author M. Schulte
 * @version 08.04.2022
 */
public class Window
{
    // Attribute
    public static Window firstWindow;
    public static Window topWindow;
    protected App appInstance;
    protected static int windowID;

    private JFrame jFrame ;
    private JPanel jPanel;

    private Scene scene;
    private Canvas canvas;
    private Pane rootPane;

    protected static String defaultFrameTitle = "jltk-window";

    // Mouse, Keyboard
    protected double mouseX, mouseY;
    protected boolean isMouseButtonDown = false;
    protected int buttonNumber;
    protected boolean wasDoubleClick = false;
    protected Vector<String> keyboardBuffer;

    protected boolean hasFocus = false;

    private AnimationTimer timer;

    private Color bgColor = Color.WHITE;

    //Grid
    public int gridXTicks = 25;
    public int gridYTicks = 25;
    ArrayList<Node>grid; // all lines and lables of the grid

    /**
     * Creates a window that fills the entire screen
     *
     */
    public Window()
    {
        this(0, 0, -1, -1, defaultFrameTitle+" " + (windowID + 1));
    }

    /**
     * Creates a window with the specified size and the specified position.
     *
     * @param pLeft The x-coordinate of the upper left corner
     * @param pTop The y-coordinate of the upper left corner
     * @param pWidth The width of the window
     * @param pHeight The height of the window
     */
    public Window(int pLeft, int pTop, int pWidth, int pHeight){
        this(pLeft, pTop, pWidth, pHeight, defaultFrameTitle+" " + (windowID + 1));
    }

    /**
     * Creates a window with the specified size.
     *
     * @param pWidth The width of the window
     * @param pHeight The height of the window
     */
    public Window(int pWidth, int pHeight){
        this(0, 0, pWidth, pHeight, defaultFrameTitle+" " + (windowID + 1));
    }

    /**
     * Creates a window with the specified size, the specified position and the specified title.
     *
     * @param pLeft The x-coordinate of the upper left corner
     * @param pTop The y-coordinate of the upper left corner
     * @param pWidth The width of the window
     * @param pHeight The height of the window
     * @param pTitle The window title
     */
    public  Window(int pLeft, int pTop, int pWidth, int pHeight, String pName){
        Platform.setImplicitExit(false);
        if (this.firstWindow == null){
            this.firstWindow = this; 
        }
        this.keyboardBuffer = new Vector<String>();
        // Init JFrame, then init FX
        this.initSwing(pLeft, pTop, pWidth, pHeight, pName);
        //final CountDownLatch doneLatch = new CountDownLatch(1);
        //SwingUtilities.invokeLater(new Runnable() {
        //        @Override
        //        public void run() {
        //            try{
        //System.out.println("before initSwing");
        //                initSwing(pLeft, pTop, pWidth, pHeight, pName);
        //System.out.println("After initSwing");
        //           }finally{
        //               doneLatch.countDown();
        //           }
        //       }
        //   });
        //try {
        //    doneLatch.await();
        //} catch (InterruptedException e) { 
        // ignore exception
        //}
        windowID++;
        this.topWindow = Window.this;
        grid = new ArrayList();
    }

    // 
    /**
     * Generates a jFrame with the specified size and name. This frame is used to hold the javaFX
     * canvas (and everything inside the canvas). This is necessary to avoid to run this as 
     * javaFX-Application. We want to generate a Window the mor object-oriented way like new Window().
     *
     */
    private void initSwing(int pLeft, int pTop, int pWidth, int pHeight, String pName){
        jFrame = new JFrame(pName);
        Dimension dimension;
        // Fullscreen
        if (pWidth == -1){
            dimension = jFrame.getToolkit().getScreenSize();
            pWidth = dimension.width;
            pHeight = dimension.height;
        }else{
            dimension = new Dimension(pWidth, pHeight);
        }
        this.jPanel = (JPanel) jFrame.getContentPane();
        //Fix Panel to correct width and height

        this.jPanel.setPreferredSize(dimension);

        this.jFrame.pack();
        //this.jFrame.setLayout(null);
        this.jFrame.setLocation(pLeft,pTop);

        this.jFrame.setVisible(true);
        //System.out.println("getWidth: "+this.getWidth()+" panelWidth: "+jPanel.getVisibleRect().width +"pWidth:"+ pWidth);
        //jFrame.setSize(this.getWidth() - panel.getVisibleRect().width + pWidth, this.getHeight() - panel.getVisibleRect().height + pHeight);
        //System.out.println("PanelWidth:"+jPanel.getVisibleRect().width);
        this.jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE );
        // set JFrame events
        this.jFrame.addWindowListener(new java.awt.event.WindowAdapter(){
                @Override
                public void windowClosing(java.awt.event.WindowEvent e)
                {
                    closeWindow();
                }
            });  
        JFXPanel jFXPanel = new JFXPanel();
        jFXPanel.setOpaque(true);
        this.jFrame.add(jFXPanel);
        jFXPanel.addFocusListener(new java.awt.event.FocusAdapter() {
                /**
                 * {@inheritDoc}
                 */
                @Override
                public void focusGained(java.awt.event.FocusEvent e) {
                    topWindow = Window.this;
                    hasFocus = true;
                    if(appInstance != null){
                        appInstance.onFocusGained();
                    }
                }

                public void focusLost(java.awt.event.FocusEvent e)
                {
                    topWindow = Window.this;
                    hasFocus = false;
                    if(appInstance != null){
                        appInstance.onFocusLost();
                    }
                }
            });
        // Start init for fx components and wait until ready (latch)
        final CountDownLatch waitForInitFx = new CountDownLatch(1);
        Platform.runLater(new Runnable(){
                @Override
                public void run() {
                    //System.out.println("Before initFx");
                    initFx(jFXPanel);
                    //System.out.println("After initFx");
                    waitForInitFx.countDown();
                }
            });
        try
        {
            waitForInitFx.await();
        }
        catch (InterruptedException ie)
        {
            ie.printStackTrace();
        }
    }

    /**
     * Generates the fx components.
     */
    private void initFx(JFXPanel pPanel){
        pPanel.setOpaque(false); //non Transparent
        this.rootPane = new Pane();
        this.canvas = new Canvas(jFrame.getWidth(), jFrame.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
        this.scene = new Scene(this.rootPane, jFrame.getWidth(), jFrame.getHeight());
        this.setColor(this.bgColor);
        // set JavaFX events
        // Mouse: Set Mouse properties and invoke methods, if there is an object of a subclass of
        // app class.
        scene.setOnMousePressed(new EventHandler<MouseEvent>(){
                public void handle (MouseEvent mouseEvent) {
                    setMousePosition(mouseEvent);
                    setMouseButton(mouseEvent);
                    isMouseButtonDown = true;
                    if(appInstance != null){
                        appInstance.onMousePressed((int) mouseEvent.getX(), (int) mouseEvent.getY());
                    }
                }
            });
        scene.setOnMouseReleased(new EventHandler<MouseEvent>(){
                public void handle (MouseEvent mouseEvent) {
                    setMousePosition(mouseEvent);   
                    isMouseButtonDown = false;
                    if(appInstance != null){
                        appInstance.onMouseReleased((int) mouseEvent.getX(), (int) mouseEvent.getY());
                    }
                }
            });
        scene.setOnMouseClicked(new EventHandler<MouseEvent>(){
                public void handle (MouseEvent mouseEvent) {
                    if (mouseEvent.getClickCount()%2 == 0) {
                        wasDoubleClick = true;
                        if(appInstance != null){
                            appInstance.onMouseDoubleClicked((int) mouseEvent.getX(), (int) mouseEvent.getY());
                        }
                    }else{
                        wasDoubleClick = false;
                        if(appInstance != null){
                        appInstance.onMouseClicked((int) mouseEvent.getX(), (int) mouseEvent.getY());
                    }
                    }
                    setMousePosition(mouseEvent);
                    setMouseButton(mouseEvent);
                    isMouseButtonDown = false;  
                }
            });
        scene.setOnMouseDragged(new EventHandler<MouseEvent>(){
                public void handle (MouseEvent mouseEvent) {
                    setMousePosition(mouseEvent);
                    setMouseButton(mouseEvent);
                    isMouseButtonDown = true;
                    if(appInstance != null){
                        appInstance.onMouseDragged((int) mouseEvent.getX(), (int) mouseEvent.getY());
                    }
                }
            });
        scene.setOnMouseMoved(new EventHandler<MouseEvent>(){
                public void handle (MouseEvent mouseEvent) {
                    setMousePosition(mouseEvent);
                    isMouseButtonDown = false;
                    if(appInstance != null){
                        appInstance.onMouseMoved((int) mouseEvent.getX(), (int) mouseEvent.getY());
                    }
                }
            });
        // Keyboard
        scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
                public void handle(KeyEvent keyEvent){
                    String key = Keyboard.keyPressed(keyEvent.getCode());
                    if(appInstance != null){
                        appInstance.onKeyPressed(key);
                    }
                }
            });
        scene.setOnKeyReleased(new EventHandler<KeyEvent>(){
                public void handle(KeyEvent keyEvent){
                    String key = Keyboard.keyReleased(keyEvent.getCode());
                    if(appInstance != null){
                        appInstance.onKeyPressed(key);
                    }
                }
            });
        // Create a AnimationTimer. If you use this Window instance through an instance of subclass of App, you
        // can overwrite the act method. This will run all the time, again and again.
        // The purpose of this, is something like "buffered drawing". If you have  heavy/complex drawing,
        // every action inside the act method will put to GraphicContext in the background and will just be 
        // make visible and the end of the act method.
        Window lThis = this; // we need 'this' inside AnimationTimer
        this.timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if(lThis == firstWindow && appInstance != null){
                    appInstance.act();
                }

            }
        };
        if(this.timer != null){  // No instance of app used
            timer.start();
        }
        // Put all together
        ((Pane)scene.getRoot()).getChildren().add(canvas);
        pPanel.setScene(scene);
        pPanel.requestFocusInWindow();
    }

    /**
     * Delays the execution by the specified amount of milliseconds.
     *
     * @param pMilliseconds The duration of delay in milliseconds
     */
    public void delay(long pMilliseconds)
    {
        try
        {
            Thread.sleep(pMilliseconds);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    /**
     * Add a component to the window. This method is mostlx for internal use but you can also
     * add any javafx.scene.Node object to the windows, too.
     *
     * @param pComponent The object, which should place on the window.
     */
    public void addComponent(Node pComponent){
        Platform.runLater(
            () -> {
                this.rootPane.getChildren().add(pComponent); 
                //this.gridToFront();
            }
        );

    }

    //////////////////////////////////
    //// called from Eventhandler ////
    //////////////////////////////////

    // close window
    private void closeWindow()
    {
        jFrame.dispose();
        if (this.equals(firstWindow)){
            if(this.timer != null){  // No instance of app used
                this.timer.stop();
            }
            System.exit(0);
        }
    }

    ///////////////////////////////
    //// set- and get-methods /////
    ///////////////////////////////   

    /**
     * Return the height of this window.
     *
     * @return The height of this window
     */
    public int getHeight(){
        //return jFrame.getHeight();
        return this.jPanel.getHeight();
    }

    /**
     * Return the width of this window.
     *
     * @return The width of this window
     */
    public int getWidth(){
        //return jFrame.getWidth();
        return this.jPanel.getWidth();
    }

    /**
     * Return the current x-coordinate of the mouse.
     *
     * @return The x-coordinate of the mouse
     */
    protected double getMouseX(){
        return this.mouseX;
    }

    /**
     * Return the current y-coordinate of the mouse.
     *
     * @return The y-coordinate of the mouse
     */
    protected double getMouseY(){
        return this.mouseY;
    }

    // Called by a mouse event. Sets the instance varaible to the current mouse position.
    private void setMousePosition(MouseEvent pMouseEvent){
        this.mouseX = pMouseEvent.getX();
        this.mouseY = pMouseEvent.getY();

    }

    // Called by a mouse event. Sets the used mouse button of the specified event.
    private void setMouseButton(MouseEvent pMouseEvent){
        this.buttonNumber = 1;
        if(pMouseEvent.getButton() == MouseButton.MIDDLE) this.buttonNumber = 2;
        if(pMouseEvent.getButton() == MouseButton.SECONDARY) this.buttonNumber = 3;
    }

    /**
     *  The background color of the window will be changed to specified color
     *
     * @param pColor The new background color
     */
    public void setColor(Color pColor){
        ((Pane)scene.getRoot()).setBackground(new Background(new BackgroundFill(pColor, CornerRadii.EMPTY, Insets.EMPTY)));
        this.bgColor = pColor;
    }

    /**
     * The background color of the window will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setColor(double pRed, double pGreen, double pBlue)
    {
        this.setColor(Color.color(pRed, pGreen, pBlue));
    }

    /**
    The background color of the window will be changed. As parameter you can use the color name 
    from the javafx.scene.paint.Color field https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html#field.summary
    For example setColor("ALICEBLUE") or setColor("blue").

    @param pColor the new background color color
     */
    public void setColor(String pColor)
    {
        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            Color lColor = (Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.setColor(lColor);

            }
        }catch(Exception e){
            System.out.println("setColor: "+pColor+" is not a valid color.");
        }
    }

    /**
     * Return the current Backgroundcolor of this window
     *
     * @return The background color
     */
    public Color getColor(){
        return this.bgColor;
    }

    // For internal use
    private Pane getRootPane(){
        return rootPane;
    }

    /**
     * Return true, if this windows currently has the focus.
     *
     * @return Der Rückgabewert
     */
    public boolean hasFocus(){
        return this.hasFocus;
    }

    /**
     * Specify if this window is always on top of every other window.
     *
     * @param true: always on top is activate, false: always on top is deactivate
     */
    public void alwaysOnTop(boolean pOnTop){
        jFrame.setAlwaysOnTop(pOnTop);
    }

    /**
     * Moves this window to the front of all other windows.
     *
     */
    public void toFront(){
        this.jFrame.toFront();
        this.jFrame.repaint();
    }

    /**
     * Removes all drawings made by a pen from this window.
     *
     */
    public void clear(){
        canvas.getGraphicsContext2D().clearRect(0, 0, this.getWidth(), this.getHeight());
    }

    /**
     * The GraphicsOCntext of the canvas. Used by the pen.
     *
     */
    protected GraphicsContext g(){
        return canvas.getGraphicsContext2D();
    }  

    /**
     * Set the attribute appInstance to an instance of a subclass of App, which use this screen.
     *
     */
    protected void setAppInstance(App pInstance){
        this.appInstance = pInstance;
    }

    /**
     * Showing a grid for this windows. You can change the width between two ticks by changing the
     * attribute gridXTicks and gridYTicks.
     *
     */
    public void showGrid(){
        for(int i = 0;i<this.getWidth();i=i+this.gridXTicks){
            Line vLine = new Line(i, 0, i, this.getHeight());
            vLine.setStroke(Color.GRAY);
            Text label = new Text(i+1, 10,i+"");
            label.setFont(Font.font(Font.getDefault().getFamily(),FontWeight.LIGHT,10));
            this.addComponent(vLine);
            this.addComponent(label);
            this.grid.add(vLine);
            this.grid.add(label);
        }
        for(int i = this.gridYTicks;i<this.getHeight();i=i+this.gridYTicks){
            Line hLine = new Line(0, i, this.getWidth(),i);
            hLine.setStroke(Color.GRAY);
            Text label = new Text(0, i+10,i+"");
            label.setFont(Font.font(Font.getDefault().getFamily(),FontWeight.LIGHT,10));
            this.addComponent(hLine);
            this.addComponent(label);
            this.grid.add(hLine);
            this.grid.add(label);
        }
        this.gridToFront();

    }

    /**
     * Hides the grid.
     *
     */
    public void hideGrid(){
        for(Node node : this.grid){
            node.setVisible(false);
        }
    }

    /**
     * Put the grid in the foreground.
     *
     */
    protected void gridToFront(){
        if(!this.grid.isEmpty()){
            for(Node node: this.grid){
                Platform.runLater(
                    () -> {
                        node.toFront();
                    }
                );

            }
        }
    }

    
    /**
     * Set the title of this window to the specified name.
     *
     * @param pTitle The new window title
     */
    public void setTitle(String pTitle){
        this.jFrame.setTitle(pTitle);
    }

}
