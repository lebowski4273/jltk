package jltk;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.*;
import javafx.scene.text.*;

/**
 * Beschreiben Sie hier die Klasse Coloredpen.
 * 
 * @author M. Schulte 
 * @version 08.06.2022
 */
public class ColoredPen extends Pen
{

    protected boolean fillEnabled = false;
    protected Paint fillColor=Color.WHITE;

    /**
     * Creates a new colored pen on the last created. Because of this, you first have to create
     * a window and the a pen.
     */
    public ColoredPen()
    {
        this(Window.topWindow);
    }

    /**
     * Creates a new colored pen on the specified window.
     *
     * @param pWindow The window for this pen.
     */
    public ColoredPen(Window pWindow){
        this.myWindow = pWindow;
        this.g = myWindow.g();
    }

    /**
     * The colored pen draws a rectangle with the specified width and height. If fill is enabled 
     * (see enableFill()), the rectangle is filled with the fill color of this pen.
     * The position of the pen is the upper left corner. The rectangle is also drawn when then pen is up.
     *
     * @param pWidth The width of the rectangle
     * @param pHeight The height of the rectangle
     * 
     * @see #enableFill()
     * @see #setFillColor()
     */
    public void drawRect(double pWidth, double pHeight){
        if (this.g != null)
        {   
            this.setState(); 
            if(this.fillEnabled){
                this.g.fillRect(this.x,this.y,pWidth,pHeight);
            }else{
                this.g.strokeRect(this.x,this.y,pWidth,pHeight);
            }
        }
    }

    /**
     * The colored pen draws a circle with the specified width and height. If fill is enabled 
     * (see enableFill()), the circle is filled with the fill color of this pen.
     * The position of the pen is the center of the circle. The circle is also drawn when then pen is up.
     *
     * @param pRadius The radius of the circle
     * 
     * @see #enableFill()
     * @see #setFillColor()
     */
    public void drawCircle(double pRadius){
        //GraphicsContext g = myWindow.g();
        if (this.g != null)
        {   
            this.setState(); 
            if(this.fillEnabled){
                this.g.fillOval(this.x-pRadius,this.y-pRadius,2*pRadius, 2*pRadius);
            }else{
                this.g.strokeOval(this.x-pRadius,this.y-pRadius,2*pRadius, 2*pRadius);
            }       

        }

    }

    /**
     * For internal use. Sets all properties for the underlying GraphicsContext
     *
     */
    private void setState(){
        if(this.g != null){
            if(this.currentMode == DRAWINGMODE){
                this.g.setStroke(this.lineColor);
                this.g.setFill(this.fillColor);
                this.g.setLineWidth(this.lineWidth);
                this.g.setFont(this.font);
            }
            if(this.currentMode == ERASEMODE){
                this.g.setStroke(this.myWindow.getColor());
                this.g.setLineWidth(this.lineWidth+2);
            }
        }

    }

    /**
     * Enable fill mode for this pen
     *
     */
    public void enableFill(){
        this.fillEnabled = true;
    }

    /**
     * Disable fill mode for this pen
     *
     */
    public void disableFill(){
        this.fillEnabled = false;
    }

    /**
     * Return true, if the filling is enabled for this pen.
     *
     * @see #enableFill()
     * @see #disableFill()
     */
    public boolean isFillEnabled(){
        return fillEnabled;
    }

    /**
    The line color of the pen will be changed to specified color

    @param pColor the new line color
     */
    public void setLineColor(Color pColor){
        this.lineColor = pColor;
        this.setState();
    }

    /**
     * The line color of the component will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setLineColor(double pRed, double pGreen, double pBlue)
    {
        this.lineColor = Color.color(pRed, pGreen, pBlue);
        this.setState();
    }

    /**
    The line color of the pen will be changed. As parameter you can use the color name 
    from the javafx.scene.paint.Color field https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html#field.summary
    For example setLineColor("ALICEBLUE") or setLineColor("blue").

    @param pColor the new line color
     */
    public void setLineColor(String pColor)
    {
        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            javafx.scene.paint.Color lColor = (javafx.scene.paint.Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.setLineColor(lColor);

            }
        }catch(Exception e){
            System.out.println("setLineColor: "+pColor+" is not a valid color.");
        }
    }
    
    
    /**
     * Return the current line color as Color object.
     *
     * @return The current line color.
     */
    public Color getLineColor(){
        try{
            return (Color) this.lineColor;
        }catch(Exception e){
            return null;
        }
    }

    /**
     * The fillcolor of the component will be changed to specified rgb-color
     *
     * @param pRed the red proportion (must be between 0 and 1)
     * @param pGreen the green proportion (must be between 0 and 1)
     * @param pBlue the blue proportion (must be between 0 and 1)
     */
    public void setFillColor(double pRed, double pGreen, double pBlue)
    {
        this.fillColor = Color.color(pRed, pGreen, pBlue);
        this.enableFill();
        this.setState();
    }

    /**
    * The fill color of the pen will be changed to the specified color. As parameter you can use the color 
    * name from the javafx.scene.paint.Color field https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html#field.summary
    * For example setFillColor("ALICEBLUE") or setFillColor("blue").
    * 
    * @param pColor the new fill color
     */
    public void setFillColor(String pColor)
    {
        try{ 
            // Get static (.get(null)) field from javafx.scene.paint.Color and convert it to Color. 
            javafx.scene.paint.Color lColor = (javafx.scene.paint.Color) javafx.scene.paint.Color.class.getDeclaredField(pColor.toUpperCase()).get(null);
            if(lColor != null){
                this.fillColor = lColor;
                this.enableFill();
                this.setState();
            }
        }catch(Exception e){
            System.out.println("setLineColor: "+pColor+" is not a valid color.");
        }
    }

    /**
     * Return the current fill color as Color object.
     *
     * @return The current fill color.
     */
    public Color getFillColor(){
        try{
            return (Color) this.fillColor;
        }catch(Exception e){
            return null;
        }
    }

    /**
    The fill color of the component will be changed to the specified color

    @param pColor the new fill color
     */
    public void setFillColor(Color pColor){
        this.lineColor = pColor;
        this.enableFill();
        this.setState();
    }

    public void setTransparency(double pValue){
        // Create a new color from current color and set opaque (1-transparency)
        try{
            this.fillColor = Color.color(
                ((Color)(this.fillColor)).getRed(),
                ((Color)(this.fillColor)).getGreen(),
                ((Color)(this.fillColor)).getBlue(),
                1-pValue);
        }catch(Exception e){
            // Skip Exception
            e.printStackTrace();
        }
        this.setState();
    }

    public double getTransparency(){
        try{
            return 1-((Color)(this.fillColor)).getOpacity();
        }catch(Exception e){
            return 0;
        }
    }

}
