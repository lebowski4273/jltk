package jltk;
/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import jltk.util.*;
/**
 * A pen belongs to a window object. Because of this, you first have to create a window. <br/>
 * A pen has a exact defined position on the window, which is defined by an x- and y-coordinate. The origin of 
 * the coordinate system is the upper left corner of the window. The x-axis points to the right, the y-axis 
 * points down. The default position is the upper left corner. The unit is pixels. <br/>
 * A pen also has a direction. 0° means east. The pen can be turn clockwise.<br/>
 * The pen can be lift up or lowered down. Is the pen down and moving across the window the pen draws a line
 * between each location. The line depends on its mode (see below).<br/>
 * If the pen is lift up, no line is drawn.
 * <p>
 * <ul>
 *   <li><b>DrawMode:</b> The pen draws a black line.</li>
 *   <li><b>EraseMode:</b> The pen draws a line in the background color. So the pen can "erase" its former drawing.</li>
 * </ul>
 * </p>
 * <p>You also can use a colored pen with some more options, see class ColoredPen for more details.</p>
 * 
 * @author Martin Schulte 
 * @version 01.06.2022
 */
public class Pen
{
    protected double x,y;
    protected boolean isUp = true;
    protected double rotation = 0;

    protected Window myWindow;
    protected int currentMode;
    protected Paint lineColor=Color.BLACK;
    protected double lineWidth = 1;
    protected Font font = Font.getDefault();

    protected static final int DRAWINGMODE = 0;
    protected static final int ERASEMODE = 1;

    GraphicsContext g;

    /**
     * Creates a new pen on the last created. Because of this, you first have to create
     * a window and the a pen.
     */
    public Pen()
    {
        this(Window.topWindow);
    }

    /**
     * Creates a new pen on the specified window.
     *
     * @param pWindow The window for this pen.
     */
    public Pen(Window pWindow){
        this.myWindow = pWindow;
        this.g = myWindow.g();
        font = Font.getDefault();
    }

    /**
     * Draws a line from the point (x1/y1) to the point (x2/y2). The line is also drawn, 
     * when then pen is up.
     *
     * @param x1 The x-coordinate of the start point
     * @param y1 The y-coordinate of the start point
     * @param x2 The x-coordinate of the stop point
     * @param y2 The y-coordinate of the stop point
     */
    private void drawLine(double x1, double y1, double x2, double y2) 
    {
        //GraphicsContext g = myWindow.g();
        if (this.g != null)
        {          
            //this.setState(g);     
            g.strokeLine(x1,y1,x2,y2);
            this.x = x2;
            this.y = y2;
            //this.myWindow.gridToFront();
        }
    }

    /**
     * For internal use. Sets all properties for the underlying GraphicsContext.
     *
     */
    private void setState(){
        if(this.g != null){
            this.g.setFont(this.font);
            if(this.currentMode == DRAWINGMODE){
                this.g.setStroke(this.lineColor);
                this.g.setFill(this.lineColor); //else BOLD is not working
                this.g.setLineWidth(this.lineWidth);
            }
            if(this.currentMode == ERASEMODE){
                this.g.setStroke(this.myWindow.getColor());
                this.g.setLineWidth(this.lineWidth+2);
            }
        }

    }

    /**
     * Delays the execution by the specified amount of milliseconds.
     *
     * @param pMilliseconds The duration of delay in milliseconds
     */
    public void delay(int pMilliseconds){
        try {
            Thread.sleep(pMilliseconds);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }
    
    
    /**
     * Return the width of the giving text using the font settings of this pen
     *
     * @param pText The text of which to calculate the width
     * @return the width of the text
     */
    public double textWidth(String pText){
        Text text = new Text(pText);
        text.setFont(this.font);
        return text.getBoundsInLocal().getWidth();
    }
    
    /**
     * Return the height of the giving text using the font settings of this pen
     *
     * @param pText The text of which to calculate the height
     * @return the height of the text
     */
    public double textHeight(String pText){
        Text text = new Text(pText);
        text.setFont(this.font);
        return text.getBoundsInLocal().getHeight();
    }

    /**
     * Moves this pen to the position (x1/y1). If the pen is down, a line is drawn.
     *
     * @param pX The x-coordinate
     * @param pY The y-coordinate
     */
    public void setPosition(double pX, double pY){
        this.setState();
        if(!this.isUp){
            this.drawLine(this.x, this.y, pX, pY);
        }else{
            this.x = pX;
            this.y = pY;
        }
    }

    /**
     * Moves this pen by the specified distance. If the pen is down, a line is drawn.
     *
     * @param pDistance The distance in pixel.
     */
    public void move(double pDistance){
        this.setState();
        double rotationRad = this.rotation * Math.PI/180;
        double xNew = this.x + pDistance*Math.cos(rotationRad);
        double yNew = this.y + pDistance*Math.sin(rotationRad);
        if(!this.isUp){
            this.drawLine(this.x, this.y, xNew, yNew);
        }else{
            this.x = xNew;
            this.y = yNew;
        }
    }

    /**
     * Set the rotation of this pen. Rotation is expressed as a degree value. Zero degrees is to the east 
     * (right-hand side of the window), and the angle increases clockwise.
     *
     * @param pRotation The angle in degrees
     * @see #turn(double)
     */
    public void setRotation(double pAngle){
        this.rotation = pAngle%360;
        if(this.rotation <0){
            this.rotation = this.rotation+360;       
        }
    }

    /**
     * Turn this pen by the specified amount (in degrees).
     *
     * @param pAmount The angle in degrees; positive values turn clockwise
     */
    public void turn(double pAngle){
        this.rotation = this.rotation + pAngle%360;
        if(this.rotation <0){
            this.rotation = this.rotation+360;       
        }
        this.rotation = this.rotation%360;
    }

    /**
     * Turn this pen to face towards the specified location.
     *
     * @param pX The x-coordinate
     * @param pY The y-coordinate
     */
    public void turnTowards(double pX, double pY){
        if(pX != this.x || pY != this.y){
            if(pX == this.x){
                if(pY > this.y){
                    this.rotation = 270;
                }else{
                    this.rotation = 90;
                }
            }else{
                if(pY == this.y){
                    if(pX > this.x){
                        this.rotation = 0;
                    }else{
                        this.rotation = 180;
                    }
                }else{
                    this.rotation = Math.atan((pY - this.y) / (this.x - pX)) * 180 / Math.PI;
                    if(pX < this.x){
                        this.rotation = this.rotation + 180;
                    }
                    if(this.rotation <0){
                        this.rotation = this.rotation+360;       
                    }
                }
            }
        }
    }

    /**
     * The pen draws a rectangle with the specified width and height. The position of the pen is the upper left
     * corner. The rectangle is also drawn when then pen is up.
     *
     * @param pWidth The width of the rectangle
     * @param pHeight The height of the rectangle
     */
    public void drawRect(double pWidth, double pHeight){
        //GraphicsContext g = myWindow.g();
        if (this.g != null)
        {          
            this.setState();       
            this.g.strokeRect(this.x,this.y,pWidth,pHeight);
            //this.myWindow.gridToFront();
        }

    }

    /**
     * The pen draws a circle with the specified radius. The position of the pen is the center of 
     * the circle. The circle is also drawn when then pen is up.
     *
     * @param pRadius The radius of the circle
     */
    public void drawCircle(double pRadius){
        //GraphicsContext g = myWindow.g();
        if (this.g != null)
        {   
            this.setState();       
            this.g.strokeOval(this.x-pRadius,this.y-pRadius,2*pRadius, 2*pRadius);
            //this.myWindow.gridToFront();
        }

    }

    /**
     * The pen writes the specified text. The lower left corner of the text is the current pen position. After
     * writing the pen position is the lower right corner of the text.
     *
     * @param pText The text
     */
    public void write(String pText){
        if (this.g != null)
        {          
            this.setState();       
            Text text = new Text(pText);
            text.setFont(this.font);
                //https://stackoverflow.com/questions/13015698/how-to-calculate-the-pixel-width-of-a-string-in-javafx
            this.g.fillText(pText, this.x, this.y);
            this.x = this.x+text.getLayoutBounds().getWidth();
        }
    }

    /**
     * The pen writes the specified double value. The lower left corner of the double value is the current pen position. After
     * writing the pen position is the lower right corner of the double value.
     *
     * @param pText The double value
     */ 
    public void write(double pNumber){
        this.write(""+pNumber);
    }

    /**
     * The pen writes the specified integer. The lower left corner of the integer is the current pen position. After
     * writing the pen position is the lower right corner of the integer.
     *
     * @param pText The integer
     */ 
    public void write(int pNumber){
        this.write(""+pNumber);
    }

    /**
     * The pen writes the specified long integer. The lower left corner of the long integer is the current pen position. After
     * writing the pen position is the lower right corner of the long integer.
     *
     * @param pText The long integer
     */ 
    public void write(long pNumber){
        this.write(""+pNumber);
    }

    /**
     * The pen writes the specified float value. The lower left corner of the float value is the current pen position. After
     * writing the pen position is the lower right corner of the float value.
     *
     * @param pText The float value
     */ 
    public void write(float pNumber){
        this.write(""+pNumber);
    }

    /**
     * The pen writes the specified character. The lower left corner of the character is the current pen position. After
     * writing the pen position is the lower right corner of the character.
     *
     * @param pText The float value
     */ 
    public void write(char pCharacter){
        this.write(""+pCharacter);
    }

    /**
     * Lifts the pen up. Moving the pen now has no visible effect, no line will be drawn.
     *
     * @see #move(double)
     * @see #setPostion(double, double)
     */
    public void up(){
        this.isUp = true;
    }

    /**
     * Lowers the pen down ("puts it on the paper"). Now, the pen draws a line while moving.
     *
     * @see #move(double)
     * @see #setPostion(double, double)
     */
    public void down(){
        this.isUp = false;
    }

    /**
     * Puts the pen into drawing mode. In this mode, the pen draws a line while moving (if pen is down).
     * 
     * @see #down()
     * @see #up()
     * @see #enableEraseMode()
     */
    public void enableDrawMode() 
    {
        this.currentMode = this.DRAWINGMODE;
        this.setState();
    }

    /**
     * Puts the pen into erase  mode. In this mode, the pen erase everything while moving (if pen is down).
     * 
     * @see #down()
     * @see #up()
     * @see #enableDrawing()
     */
    public void enableEraseMode() 
    {
        this.currentMode = this.ERASEMODE;
        this.setState();
    }
    
    /**
     * Return true, if the pen is in erase mode, return false if the pen is in draw mode
     *
     * @return true if erase mode is enabled, else false
     */
    public boolean isEraseModeEnabled(){
        return this.currentMode == this.ERASEMODE;
    }

    /**
     * Returns the x-coordinate of the pen's current location.
     *
     * @return Returns the x-coordinate of the pen's current location.
     */
    public double getX()
    {
        return this.x;
    }

    /**
     * Returns the y-coordinate of the pen's current location.
     *
     * @return Returns the y-coordinate of the pen's current location.
     */
    public double getY(){
        return this.y;
    }

    /**
     * Returns the current rotation of this pen. Rotation is expressed as a degree value, range (0..359). 
     * Zero degrees is towards the east, and the angle increases clockwise.
     *
     * @return The rotation in degrees.
     */
    public double getRotation(){
        return this.rotation;
    }   


    /**
     * Set the font, the pen should use. You can use every Font installed on your system. E.g.
     * "Calibri", "Arial", "Times New Roman", "Deja Vu Sans". You can print all available fonts of 
     * your system, if you run jltk.util.Misc.printAvailableFontNames().
     *
     * @param pFontName Ein Parameter
     * @see #getFont()
     * @see jltk.util.Misc#printAvailableFontNames()
     */
    public void setFont(String pFontName){
        this.font = Font.font(pFontName);
        this.setState();
    }

    /**
     * Return the name of the font, the pen currently use.
     *
     * @return The font name
     * @see #setFont(String)
     * @see jltk.util.Misc#printAvailableFontNames()
     */
    public String getFont(){
        return this.font.getName();
    }

    /**
     * Set the font size to the specified value.
     *
     * @param pSize The font size
     */
    public void setFontSize(double pSize){
        this.font = Font.font(this.font.getName(),pSize);
        this.setState();
    }

    /**
     * Return the size of the font the pen currently use.
     *
     * @return The font size
     * 
     * @see #setFontSize(double)
     */
    public double getFontSize(){
        return this.font.getSize();
    }

    
    /**
     * Set the font style of the pen. 
     *
     * @param pBold true if bold, else false
     * @param pItalic true if italic, else false
     * 
     * @see #write(String)
     */
    public void setFontStyle(boolean pBold, boolean pItalic){
        if(pBold && pItalic){
            this.font = Font.font(this.font.getName(),FontWeight.BOLD,FontPosture.ITALIC,this.font.getSize());
        }else{
            // only one or none is true
            if(pBold){
                this.font = Font.font(this.font.getName(),FontWeight.BOLD,this.font.getSize());
            }else{
                if(pItalic){
                    this.font = Font.font(this.font.getName(),FontPosture.ITALIC,this.font.getSize());
                }else{ // normal
                    this.font = Font.font(this.font.getName(),FontWeight.NORMAL,FontPosture.REGULAR,this.font.getSize());
                }
            }
        }
        this.setState();
    }

    /**
     * Sets the standard values for this pen. (for internal use)
     *
     */
    protected void setDefault(){
        this.x = 0;
        this.y = 0;
        this.isUp = true;
        this.rotation = 0;
        this.currentMode = this.DRAWINGMODE;
        this.enableDrawMode();
        this.font=Font.getDefault();
    }

}
