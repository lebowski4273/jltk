package jltk;

import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
/**
 * Beschreiben Sie hier die Klasse Keyboard.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Keyboard
{
    
    static private boolean hasNumLock = true;
    private final static Set<String> keysDown = new HashSet<>();
    private static String lastKeyTyped="";
    
    
    
    
    // The following methods are from the Greenfoot project (with some minor changes), liscend under GPL 2 or later
    // greenfoot\src\greenfoot\gui\input\KeyboardManger.java see http://www.greenfoot.org/download/files/source/Greenfoot-source-371.zip
    
    /**
     * Check whether a key, identified by a key name (String),
     * is currently down .
     * 
     * @param key     The name of the key to check
     * @return        True if the key is currently down, or was down since
     *                it was last checked; false otherwise.
     */
    public static boolean isKeyDown(String key)
    {
        key = key.toLowerCase();
        return keysDown.contains(key) ;
    }
    
    
    /**
     * Get the last key pressed, as a String key name identifying the key.
     */
    public synchronized static String getKey()
    {
        String r = lastKeyTyped;
        lastKeyTyped = "";
        return r;
    }
    
    /**
     * Notifies that a key has been pressed. (For internal use)
     * @param keyCode The KeyCode from KeyEvent.getCode()
     */
    protected synchronized static String keyPressed(KeyCode keyCode)
    {
        String keyName = getKeyName(keyCode);
        keysDown.add(keyName);
        return keyName;
    }
    
     /**
     * Notifies that a key has been released. (For internal use)
     * @param keyCode The KeyCode from KeyEvent.getCode()
     */
    protected synchronized static String keyReleased(KeyCode keyCode)
    {
        String keyName = getKeyName(keyCode);
        keysDown.remove(keyName);
        lastKeyTyped = keyName;
        return keyName;
    }
    
        /**
     * Translate the "key pad" directional keys according to the status of numlock,
     * and otherwise translate a KeyCode+text into a the key name.
     * 
     * @param keycode  The original keycode
     * @param keyText  The text from the original keyboard event
     * @return   The translated key name
     */
    protected static String getKeyName(KeyCode keycode){

        if (keycode.ordinal() >= KeyCode.NUMPAD0.ordinal() && keycode.ordinal() <= KeyCode.NUMPAD9.ordinal()) {
            // At least on linux, we can only get these codes if numlock is on; in that
            // case we want to map to a digit anyway.
            return "" + (char)('0' + (keycode.ordinal() - KeyCode.NUMPAD0.ordinal()));
        }

        // Seems on linux (at least) we can't get the numlock state (get an
        // UnsupportedOperationException). Update: on Java 1.7.0_03 at least,
        // we can now retrieve numlock state on linux.
        boolean numlock = true;
        if (hasNumLock)
        {
            try
            {
                numlock = java.awt.Toolkit.getDefaultToolkit().getLockingKeyState(java.awt.event.KeyEvent.VK_NUM_LOCK);
            }
            catch (UnsupportedOperationException usoe)
            {
                // Don't try to get numlock status again
                hasNumLock = false;
            }
        }

        if (numlock)
        {
            // Translate to digit
            if (keycode == KeyCode.KP_UP)
            {
                keycode = KeyCode.DIGIT8;
            }
            else if (keycode == KeyCode.KP_DOWN)
            {
                keycode = KeyCode.DIGIT2;
            }
            else if (keycode == KeyCode.KP_LEFT)
            {
                keycode = KeyCode.DIGIT4;
            }
            else if (keycode == KeyCode.KP_RIGHT)
            {
                keycode = KeyCode.DIGIT6;
            }
        }
        else
        {
            // Translate to direction
            if (keycode == KeyCode.KP_UP)
            {
                keycode = KeyCode.UP;
            }
            else if (keycode == KeyCode.KP_DOWN)
            {
                keycode = KeyCode.DOWN;
            }
            else if (keycode == KeyCode.KP_LEFT)
            {
                keycode = KeyCode.LEFT;
            }
            else if (keycode == KeyCode.KP_RIGHT)
            {
                keycode = KeyCode.RIGHT;
            }
        }

        // Handle the keys where the Greenfoot name doesn't line up with the FX KeyCode.getName():
        switch (keycode)
        {
            case ESCAPE:
                return "escape";
            case BACK_SPACE:
                return "backspace";
            case QUOTE:
                return "\'";
            case CONTROL:
                return "control";
            default:
                break;
        }
        // By default, use the key text lower-cased if present:
        /**if (!keyText.isEmpty())
        {
            // Fix a few keys which don't have text corresponding to their names:
            switch (keyText)
            {
                case "\r": case "\n":
                    return "enter";
                case "\t":
                    return "tab";
                case "\b":
                    return "backspace";
                case " ":
                    return "space";
                case "\u001B":
                    return "escape";
            }
            
            return keyText.toLowerCase();
        }
        else
        {**/
            // Otherwise fetch the JavaFX name from the KeyCode and lower-case that:
            return keycode.getName().toLowerCase();
      //  }
    
        
    }
    
  
}
