package jltk;

/*
This file is part of jltk (java learning toolkit). 
Copyright (C) 2022 Martin Schulte

This program is free software; you can redistribute it and/or 
modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 
of the License, or (at your option) any later version. 

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 

This file is subject to the Classpath exception as provided in the  
LICENSE.txt file that accompanied this code.
 */

import jltk.Window;
import jltk.App;
import javafx.scene.Node;

/**
 * abstract class Component - The base class for all elements that can be put on a
 * window (e.g. shapes an gui elements).
 * Each component can invoke a method, when it gets or losts focus. It is also possible to
 * make it visible or hide it
 * Each element has an absolute position on the window. For gui elements the position is defined 
 * by the upper-left corner of the element. For shapes the postion depends on the form of the shape.
 * Please see the corresponding documentation of the shape.
 * 
 * @author M. Schulte
 * @version 25.3.2022
 */
public abstract class Component
{

    public Node fxNode;
    protected String onFocusLost = "";
    protected String onFocusGained = "";
    protected boolean hasFocus = false;

    protected Window myWindow;

    /**
     * Basic initialisation for every component which could be placed on a window.
     *
     * @param pNode The Node should be placed on this window.
     */
    protected void init(Node pNode){
        this.fxNode = pNode;
        this.fxNode.focusedProperty().addListener(new javafx.beans.value.ChangeListener<Boolean>()
            {
                @Override
                public void changed(javafx.beans.value.ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
                {
                    if (newPropertyValue)
                    {
                        runMethodByName(onFocusGained);
                        hasFocus= true;
                    }
                    else
                    {
                        runMethodByName(onFocusLost);
                        hasFocus= false;
                    }
                }
            });
    }

    /**
     * Set the name of the method, which will be invoked,if this component gets the focus.
     */
    public void setOnFocusGained(String pMethod)
    {
        this.onFocusGained = pMethod;
    }

    /**
     * Set the name of the method, which will be invoked, if this component lost the focus.
     */
    public void setOnFocusLost(String pMethod)
    {
        this.onFocusLost = pMethod;
    }


    /**
     * Runs the method pMethodenname. The method must be located in a subclass of the class App. 
     * In the class App is an static attribut theApp which points to last initialized instance of App.
     * Because of this, there should be only one instance of class App.
     *
     * @param pMethodname The name of the method (must be located in a subclass of App)
     */
    protected void runMethodByName(String pMethodname){
        Class theApp;
        java.lang.reflect.Method method;
        if (pMethodname != null && pMethodname.length() > 0)
        {
            try
            {
                theApp = App.theApp.getClass();
                try
                {
                    method = theApp.getDeclaredMethod(pMethodname, null);
                    method.setAccessible(true);
                    method.invoke(App.theApp, null);         
                }
                catch (java.lang.reflect.InvocationTargetException e0)
                {
                    System.out.println("Error in method \"" + pMethodname + "\" of an "+this.getClass().getSimpleName()+": " + e0.getTargetException().toString());
                    e0.printStackTrace();
                }
                catch (Exception e1)
                {
                    System.out.println("Error: Method \"" + pMethodname + "\" of an "+this.getClass().getSimpleName()+" not found.");
                    e1.printStackTrace();
                }
            }
            catch (Exception e4)
            {
                System.out.println("jltk: " + e4.toString());
            }
        }
        /**else{
        System.out.println("jltk: There is no method defined for this event."); 

        }**/
    }
    
     public void toFront(){
        javafx.application.Platform.runLater(
            () -> {
                this.fxNode.toFront();
                //this.myWindow.gridToFront();
            }
        );
        
    }
    
    public void toBack(){
        javafx.application.Platform.runLater(
            () -> {
                this.fxNode.toBack();
            }
        );
    }
    
    /**
    The component is not visible any more
     */
    public void hide(){
        this.fxNode.setVisible(false);
    }

    /**
     * The component will be hiden.
     *
     */
    public void show(){
        this.fxNode.setVisible(true);
    }

    /**
    Return true, if the component is visible
     */
    public boolean isVisible(){
        return this.fxNode.isVisible();
    }
    
    /**
    The component gets the input focus.
     */
    public void focus()
    {
        this.fxNode.requestFocus();
    }
    
    /**
    Returns wether the component has focus or not.
    @return true, if this component has the focus
     */
    public boolean hasFocus()
    {
        return this.hasFocus;
    }
    
    
    /**
    Deactivate the component (grey out).
     */
    public void deactivate()
    {
        this.fxNode.setDisable(true);
    }

    /**
    Activate the component.
     */
    public void activate()
    {
        this.fxNode.setDisable(false);
    }

    /**
    Returns wether the component is active or not.
    @return true, if the component ist active
     */
    public boolean istActive()
    {
        return !this.fxNode.isDisabled();
    }

   /**
    Return the x-coordinate of the top left corner of this component
    @return The x-coordinate of this component.
    @see #setX(double)
     */
    public double getX()
    {
        return this.fxNode.getLayoutX();
    }

    /**
    Returns the y-coordinate of the top left corner of this component
    @return the y-coordinate of this component
    @see #setY(double)
     */
    public double getY()
    {
        return this.fxNode.getLayoutY();
    }
    
  

    /**
    The component gets a new position (top left corner)
    @param pX new horizontal position of the top left corner.
    @param pY new vertical position of the top left corner.
     */
    public void setPosition(double pX, double pY)
    {
        this.fxNode.setLayoutX(pX);
        this.fxNode.setLayoutY(pY);

    }

}
