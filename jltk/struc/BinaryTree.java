package jltk.struc;


/**
 * Beschreiben Sie hier die Klasse BinaryTree.
 * 
 * @author Schulte  
 * @version 1.0
 */
public class BinaryTree<ContentType>
{
    BinaryTree<ContentType> leftTree;
    BinaryTree<ContentType> rightTree;
    ContentType content;
    
    
    public BinaryTree()
    {
        
    }
    
    public BinaryTree(ContentType pContent, BinaryTree<ContentType> pLeftTree, BinaryTree<ContentType> pRightTree){
        this.content = pContent;
        this.leftTree = pLeftTree;
        this.rightTree = pRightTree;
    }
    
    public boolean isEmpty(){
        return this.content == null;
    }
    
    public void setContent(ContentType pContent){
        this.content = pContent;
    }
    
    public ContentType getContent(){
        return this.content;
    }
    
    public BinaryTree<ContentType> getLeftTree(){
        return this.leftTree;
    }
    
    public BinaryTree<ContentType> getRightTree(){
        return this.rightTree;
    }
    
    public void setLeftTree(BinaryTree<ContentType> pLeftTree){
        this.leftTree = pLeftTree;
    }
    
    public void setRightTree(BinaryTree<ContentType> pRightTree){
        this.rightTree = pRightTree;
    }

    
}