package jltk.struc;

public class List<ContentType>
{
    Node<ContentType> first, last, current;
    public String seperator = ";";

    private class Node<ContentType>
    {
        ContentType content;
        Node next;

        /**
         * A new node with the specified content will be create. 
         * The successor is empty
         *
         * @param pContent The content of the new Node 
         */
        public Node(ContentType pContent)
        {
            this.content = pContent;
        }

        /**
         * Return the successor of this node.
         *
         * @return The successor of this node
         */
        public Node getNext(){
            return this.next;   
        }

        /**
         * Set the successor of this node to the specified Node.
         *
         * @param pNode The new successor of this node
         */
        public void setNext(Node pNode){
            this.next = pNode;
        }

        /**
         * Set the content of this node to the specified content.
         *
         * @param pContent The new content of this node.
         */
        public void setContent(ContentType pContent){
            this.content = pContent;
        }

        /**
         * Return the content of this node.
         *
         * @return the content of this node.
         */
        public ContentType getContent(){
            return this.content;
        }
    }

    /**
     * Create an empty list
     *
     */
    public List()
    {
        first = null;
        last = null;
        current = null;
    }

    /**
     * Return true, if the list is empty, else true,
     *
     * @return true, if this list is empty, else false.
     */
    public boolean isEmpty()
    { 
        return first ==  null;
    }

    /**
     * Return true, if a current object exists, else false.
     *
     * @return true, if access ist prossible, else false.
     */
    public boolean hasAccess()
    {
        return current != null;
    }

    /**
     * Set the current object to the first object.
     *
     */
    public void toFirst()
    {
        this.current = this.first;
    }

    /**
     * Set the current object to the last object.
     *
     */
    public void toLast()
    {
        this.current = this.last;
    }

    /**
     * Return the previous node of the current node, or null if there is no previous node.
     *
     * @return The previous node of the current node or null.
     */
    private Node getPrevious(){
        return this.getPrevious(this.current);
    }

    /**
     * Return the previous node of pNode, or null if there is no previous node or pNode is null
     *
     * @param pNode The node for which to find the previous.
     * @return The previous node of node or null
     */
    private Node getPrevious(Node pNode){
        if (this.first != null && pNode != null && pNode != this.first) {
            Node tmp = this.first;
            Node previous;
            while (tmp != null && tmp.getNext() != pNode) {
                tmp = tmp.getNext();
            }
            return tmp;
        }else{
            return null;
        }
    }

    /**
     * If the list is not empty an hasAccess() is true and the current object 
     * is not the last object, the current object is set to the successor of this
     * object. Otherwise there is no current object, that means hasAccess() 
     * returns false.
     */
    public void next()
    {
        if(current != null){
            current = current.getNext(); 
        }
    }

    /**
     * If a current object exists (hasAccess() == true), the current object is
     * returned, otherwise null is returned.
     *
     * @return the current object or null.
     */
    public ContentType getContent()
    {
        if(this.current != null){
            return this.current.getContent();    
        }
        return null;
    }

    /**
     * Return the last object of this list, or null if the list is empty,
     *
     * @return the last object or null.
     */
    public ContentType getLast()
    {
        if(this.last != null){
            return this.last.getContent();
        }else{
            return null;
        }
    }

    /**
     * If a current object exists (hasAccess() == true) and pContent is not null,
     * the current object is set to pContent. Otherwise nothing happens.
     * 
     * @param pContent The new content for the current object.
     */
    public void setContent(ContentType pContent)
    {
        if (pContent != null && this.hasAccess()) { 
            this.current.setContent(pContent);
        }
    }

    public void insert (ContentType pContent)
    {
        if(pContent != null){
            Node newNode = new Node<ContentType>(pContent);
            if(this.first == this.current){
                Node tmp = new Node(pContent);
                tmp.setNext(this.first);
                this.first = tmp;
            }else{
                if(hasAccess() && !isEmpty()){
                    Node previous = this.getPrevious();
                    previous.setNext(newNode);
                    newNode.setNext(this.current);
                }
                if(hasAccess() && isEmpty()){
                    this.append(pContent);
                }
            }
        }
    }

    public void append (ContentType pContent)
    {
        if (pContent != null) { 
            Node newNode = new Node<ContentType>(pContent);
            if(this.isEmpty()){
                this.first = newNode;
                this.last = newNode;
            }else{
                this.last.setNext(newNode);
                this.last = newNode;
            }

        }
    }

    public void remove()
    {   
        if (this.hasAccess() && !this.isEmpty()) { 
            Node previous = this.getPrevious();
            if(previous != null){ // not the first node
                if (this.current == this.last) {// last node delete
                    this.last = previous;
                }
                previous.setNext(this.current.getNext());
            }else{ // first node
                this.first = this.first.getNext();
                if(this.isEmpty()){ // did we remove the last one
                    this.last = null;
                }
            }
            Node tmp = this.current.getNext();
            // Let the gc do is work
            this.current.setContent(null);
            this.current.setNext(null);
            this.current = tmp;
        }

    }

    public void concat(List<ContentType> pList) {
        if (pList != this && pList != null && !pList.isEmpty()) { 
            if (this.isEmpty()) { // empty list
                this.first = pList.first;
                this.last = pList.last;
            } else {
                this.last.setNext(pList.first);
                this.last = pList.last;
            }
            // delete pList
            pList.first = null;
            pList.last = null;
            pList.current = null;
        }
    }

    public String toString()
    {
        if (this.isEmpty())
            return "";
        else
        {
            String text = "";
            Node tmp = this.first;
            while (tmp != null)
            {
                text += tmp.getContent().toString()+this.seperator;

                tmp = tmp.getNext();
            }
            return text;
        }
    }

}