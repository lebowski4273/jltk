package jltk.struc;

/**
 * The generic class Queue is a linear data structure that stores elements in a 
 * FIFO (First In, First Out) order. Elements a added at the end of the queue and
 * removed from the front.

 * 
 * @author M. Schulte   
 * @version 23.08.2022
 */
public class Queue<ContentType>
{
    Node<ContentType> head, tail;
    public String separator = ";";
    // inner class Node
    private class Node<ContentType>
    {
        ContentType content;
        Node next;

        /**
         * A new node with the specified content will be create. 
         * The successor is empty
         *
         * @param pContent The content of the new Node 
         */
        public Node(ContentType pContent)
        {
            this.content = pContent;
        }

        /**
         * Return the successor of this node.
         *
         * @return The successor of this node
         */
        public Node getNext(){
            return this.next;   
        }

        /**
         * Set the successor of this node to the specified Node.
         *
         * @param pNode The new successor of this node
         */
        public void setNext(Node pNode){
            this.next = pNode;
        }

        /**
         * Return the content of this node.
         *
         * @return the content of this node.
         */
        public ContentType getContent(){
            return this.content;
        }


    }
    /**
     * Objects managed by this queue must be of type ContentType   
     *
     */
    public Queue()
    {
        this.head = null;
        this.tail = null;
    }

    /**
     * The specified content of type ContentType is append to this
     * queue. If pContent is null, othing happen.
     *
     * @param pContent The new content.
     */
    public void enqueue(ContentType pContent){
        if (pContent != null) {
            Node tmp = new Node(pContent);
            if (this.head == null) {
                head = tmp;
                tail = tmp;
            } else {
                tail.setNext(tmp);
                tail = tmp;
            }
        }
    }

    /**
     *  Remove the first node from queue.
     */
    public void dequeue(){
        if(this.head != null){
            this.head = this.head.getNext();
            if(this.head == null){ // we removed the last element, so tail muss be set to null
                this.tail = null;
            }
        }
    }

    /**
     * Return the content of the first node. If the queue is empty
     * will return null.
     *
     * @return The content of the first node.
     */
    public ContentType front(){
        if(this.head != null){
            return this.head.getContent();
        }else{
            return null;
        }
    }

    /**
     * Return true, if the queue is empty, else true,
     *
     * @return true, if this queue is empty, else false.
     */
    public boolean isEmpty(){
        return this.head == null;
    }

    /**
     * Return the string representation of this queue. To represent
     * an element, the toString() method pf the content is used. 
     * Each element is seperated by a semicolon. You can change the
     * seperator by changing the attribut separator.
     *
     * @return The string representation of this queue.
     */
    public String toString(){
        String text = "";
        Node tmp = this.head;
        while(tmp != null){
            text = text + tmp.getContent().toString()+this.separator;
            tmp = tmp.getNext();
        }
        return text;
    }

}

