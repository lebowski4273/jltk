package jltk.testing;
import jltk.*;

/**
 * Beschreiben Sie hier die Klasse AppTester.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class AppTester extends App
{
    Pen pen;

    /**
     * Konstruktor für Objekte der Klasse AppTester
     */
    public AppTester()
    {
        super(400,500);
        pen = new Pen();
        pen.setPosition(100,100);
        this.window.setColor("blue");
    }

    public void onMouseMoved(int pX, int pY){
       // System.out.println("moved");
    }
    
    public void onMouseDoubleClicked(int pX, int pY){
        System.out.println("was double click");
    }
    
    public void onMouseClicked(int pX, int pY){
        System.out.println("was single click");
    }

    
    public void onKeyPressed(String pKey){
        System.out.println(pKey);
    }

    public void onKeyReleased(String pKey){
        System.out.println(pKey);
    }

    public void act(){ 
        pen.enableEraseMode();
        pen.drawCircle(10);
        pen.turn(5);
        pen.move(1);
        pen.enableDrawMode();
        pen.drawCircle(10);
        pen.delay(1);
        System.out.println("Hier");
    }

    public void onFocusGained(){
        System.out.println("Got focus"+this.getWindow().getHeight());

    }

    public void onFocusLost(){
        System.out.println("Lost focus"+this.getWindow().getWidth());
    }

}
