package jltk.testing;
import jltk.App;
import jltk.shape.*;

/**
 * Beschreiben Sie hier die Klasse ShapeTests.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class ShapeTests extends App
{
    Circle circ, circ1;
    Rectangle rect;

    /**
     * Konstruktor für Objekte der Klasse ShapeTests
     */
    public ShapeTests()
    {
       super(500,600);
       
       circ1 = new Circle(240,240,50);
       rect = new Rectangle(100,100,40,80);
       circ1.setFillColor("Green");
       circ = new Circle(350,400,60);
       
       circ1.toFront();
    this.window.showGrid();
    }
    
    
    public void circleSetPosition(int pX, int pY){
        circ.setPosition(pX, pY);
    }
    
     public void circleSetCenterPosition(int pX, int pY){
        circ.setCenterPosition(pX, pY);
    }
    
    public void circleColorMoveTransparency(){
        this.circ.setFillColor(1.0,0.5,0.5);
        this.circ.setCenterPosition(300,150);
        this.circ.setTransparancy(0.5);
    }
    
    public void circleHide(){
        this.circ.hide();
    }
    
    public void circleShow(){
        this.circ.show();
    }
    
    public void circleMove(double pDistance){
        this.circ.move(pDistance);
    }
    
    public void circleMove(double pX, double pY){
        this.circ.move(pX,pY);
    }
    
    public void circScale(double pX, double pY){
        this.circ.scale(pX, pY);
    }
    
     public void circTurn(int pRotation){
        circ.turn(pRotation);
    }
    
    public void circTurn(double pX, double pY, double pRotation){
        circ.turn(pX,pY,pRotation);
    }
    
    public void circleLineColor(String pColor){
        circ.setLineColor(pColor);
    }
    
    public void circleLineWidth(int pWidth){
        circ.setLineWidth(pWidth);
    }
    
    public void circleFillColor(String pColor){
        circ.setFillColor(pColor);
    }
    
    
    public boolean circleIntersect(){
        return circ.intersects(circ1);
    }
    
    public boolean circleContains(){
        return circ.contains(circ1);
    }
    
    public void rectFlip(){
        rect.flipHorizontal();
    }
    
    public void rectSetPosition(int pX, int pY){
        rect.setPosition(pX, pY);
    }
    
    public void rectSetCenterPosition(int pX, int pY){
        rect.setCenterPosition(pX, pY);
    }
    
    public void rectScale(double pX, double pY){
        this.rect.scale(pX, pY);
    }
    
    public void rectScaleTo(double pWidth, double pHeight){
        this.rect.scaleTo(pWidth, pHeight);
    }
    
    public void rectSetRotation(int pRotation){
        rect.setRotation(pRotation);
    }
    
    public void rectTurn(int pAngle){
        rect.turn(pAngle);
    }
    
    public boolean contains(int pX, int pY){
        return circ.contains(pX,pY);
    }
    
    public void animation(){
        while(true){
            this.rect.turn(20);
           // this.delay(100);
        }
    }
    
    public void backAndforth(){
        int speed = 4;
        while(!jltk.Mouse.wasDoubleclick()){
            System.out.println("x: "+circ.getCenterX()+" y: "+circ.getCenterY()+" width:"+circ.getWidth()+" window:"+this.window.getWidth());
            if(circ.getCenterX()+circ.getWidth()/2>this.window.getWidth()){
                 //speed = -4;
                 circ.setRotation(180);
            }
            if(circ.getCenterX()-circ.getWidth()/2<0){
                //speed = 4;
                circ.setRotation(0);
            }
            System.out.println(circ.getRotation()+"");
            this.delay(1);
            circ.move(speed);
        }
    }
    
    public void circPrint(){
        System.out.println("Width: "+this.circ.getWidth());
        System.out.println("Height: "+this.circ.getHeight());
        System.out.println("getCenterX: "+this.circ.getCenterX());
        System.out.println("getCenterY: "+this.circ.getCenterY());
        System.out.println("getCenterLocaleX: "+this.circ.fxShape.getBoundsInLocal().getCenterX());
        System.out.println("getCenterLocaleY: "+this.circ.fxShape.getBoundsInLocal().getCenterY());
        System.out.println("getX: "+this.circ.getX());
        System.out.println("getY: "+this.circ.getY());
        System.out.println("getRotation: "+this.circ.getRotation());
        System.out.println("Transparency: "+this.circ.getTransparancy());
        System.out.println("is visible: "+String.valueOf(this.circ.isVisible()));
    }
    
     public void rectPrint(){
        System.out.println("Width: "+this.rect.getWidth());
        System.out.println("Height: "+this.rect.getHeight());
        System.out.println("getCenterX: "+this.rect.getCenterX());
        System.out.println("getCenterY: "+this.rect.getCenterY());
                System.out.println("getCenterLocaleX: "+this.rect.fxShape.getBoundsInLocal().getCenterX());
        System.out.println("getCenterLocaleY: "+this.rect.fxShape.getBoundsInLocal().getCenterY());
        System.out.println("getX: "+this.rect.getX());
        System.out.println("getY: "+this.rect.getY());
        System.out.println("getRotation: "+this.rect.getRotation());
        System.out.println("Scale x: "+this.rect.scale.getX());
         System.out.println("Scale y: "+this.rect.scale.getY());
        System.out.println("Transparency: "+this.rect.getTransparancy());
        System.out.println("is visible: "+String.valueOf(this.circ.isVisible()));
    }

    
}

