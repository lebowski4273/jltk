package jltk.testing;
import jltk.*;

/**
 * Beschreiben Sie hier die Klasse Test.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class GuiTest extends App
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    jltk.gui.RadioButtonGroup group;
    jltk.Window window2;

    /**
     * Konstruktor für Objekte der Klasse Test
     */
    jltk.gui.ComboBox c;
    jltk.gui.Button btn;
    jltk.gui.CheckBox chk;
    jltk.gui.RadioButton rbtn, rbtn2;
    jltk.gui.Label lbl;
    jltk.gui.Spinner spn;
    jltk.gui.Slider sld;
    jltk.gui.TextField txt;
    jltk.gui.PasswordField pwd;
    jltk.gui.TextArea txA;
    jltk.gui.ListView lst;
    jltk.gui.ProgressIndicator pgi;
    jltk.gui.ProgressBar pgb;
    jltk.gui.Table tbl;
    jltk.gui.Image img, img2;
    jltk.gui.Video vid1;
    jltk.gui.Separator sep;

    public GuiTest()
    {
        super(600,800);
        window2 = new jltk.Window(500,500);
        // Combobox
        c = new jltk.gui.ComboBox(20,60,120,25);
        c.append("Hallo Welt");
        c.append("Hello World");
        c.append(new Integer(54));
        c.add(2,"Hello World1");
        c.set(1,"abc");
        c.remove(2);
        c.select(2);
        c.setOnItemSelected("abc");
        c.setOnFocusGained("abc2");

        // Button
        btn = new jltk.gui.Button(60,60,100,25,"Hallo Welt");
        // btn.setColor(0.5,0.2,0.7);
        btn.setOnButtonClicked("videoInfo");
        btn.setUnderline(true);
        btn.setTooltip("Hallo Welt");
        c.focus();
        btn.setColor("red");

        //CheckBox
        chk = new jltk.gui.CheckBox(20,90,400,25,"Hake \n mich an");
        chk.setOnSelected("a1");
        chk.setOnDeselected("a2");

        // Radiobutton
        rbtn = new jltk.gui.RadioButton(20,110,200,25,"Hake mich an");
        rbtn2 = new jltk.gui.RadioButton(20,140,200,25,"Hake mich nicht an");
        group = new jltk.gui.RadioButtonGroup();
        group.add(rbtn);
        group.add(rbtn2);

        rbtn.setOnSelected("a1");
        rbtn.setOnDeselected("a2");

        lbl = new jltk.gui.Label(window2,20,170,200,25,"Ich bin ein Label");
        System.out.println("lbl: "+lbl.getWidth()+"-"+lbl.getHeight());
        lbl.setFontColor("blue");
        lbl.setTextAlignment("");
        jltk.gui.Label lbl1 = new jltk.gui.Label(window2,20,200,200,25,"Ich bin kein Label");
        lbl1.setColor("green");
        lbl1.setFontColor(1, 0, 0.2);

        // Spinner
        spn = new jltk.gui.Spinner(300,20,50,25,1,100,4,2);
        //Slider
        sld = new jltk.gui.Slider(300,50,200,25,1,20,4,2);
        sld.setLabledTickDistance(50);
        sld.setUnlabledTickCount(5);
        sld.enableSnaping();
        sld.setOnChanged("abc");

        txt = new jltk.gui.TextField(20,250,200,25,"Hallo");
        txt.setFont("OpenDyslexicMono");
        txt.setFontSize(6);
        txt.setOnSelectionChanged("textFieldAction");
        txt.disableEdit();

        pwd = new jltk.gui.PasswordField(20,280,200,25,"");

        txA = new jltk.gui.TextArea(250,250,200,100);
        String[] arr = {"a","b","c","d"};
        txA.setContent(arr);
        txA.setFontStyle(true,true);

        lst = new jltk.gui.ListView(20,375,80,100);
        lst.append("Hallo");
        String[] items = {"A","B","C"};
        Integer[] numbers = {1,2,3,4,5};
        lst.append(items);
        lst.append(numbers);
        lst.enableMultiselect();
        lst.enableEditable();
        lst.setOnItemSelected("listViewAction");
        lst.setOnItemEdited("abc");
        lst.select(3);

        pgi = new jltk.gui.ProgressIndicator(300,70,30,30);

        pgb = new jltk.gui.ProgressBar(300,110,100,30,50,100);

    
         img = new jltk.gui.Image(400,310,100,300,"https://www.bluej.org/bluej-icon-256-2x.png");
         img.setOnImageClicked("imageClicked");
         img2 = new jltk.gui.Image(400,650,100,"https://www.bluej.org/bluej-icon-256-2x.png");
         img2.setOnImageClicked("image2Clicked");
        System.out.println("img: "+img.getWidth()+"-"+img.getHeight()+"  img2: "+img2.getWidth()+"-"+img2.getHeight());
        //vid1 = new jltk.gui.Video(100,650,100,100,"https://download.samplelib.com/mp3/sample-3s.mp3");
        vid1 = new jltk.gui.Video(100,650,100,-1,"https://download.samplelib.com/mp4/sample-5s.mp4");
        //vid1 = new jltk.gui.Video(100,650,100,-1,"sample-5s.mp4");
        vid1.autoplay();
        //vid1.setTime(4);
        System.out.println(vid1.getDuration()+"V:"+vid1.getVolume());
        //this.fxMedia = new javafx.scene.media.Media("https://download.samplelib.com/mp4/sample-5s.mp4"); 
        //    this.fxMediaPlayer = new javafx.scene.media.MediaPlayer(this.fxMedia);  
         //   this.fxMediaView = new javafx.scene.media.MediaView(this.fxMediaPlayer);
          //  this.window.addComponent(this.fxMediaView);
          this.sep = new jltk.gui.Separator(95,645,20,100);
    }
    
    public void videoInfo(){
        System.out.println("Duration:" +vid1.getDuration());
        System.out.println("Volume:" +vid1.getVolume());
        System.out.println("CurrentTime:" +vid1.getCurrentTime());
    }

    public void image2Clicked(){
        this.img2.setImage("img2.png");
        this.img.show();
    }

    public void imageClicked(){
        this.img.hide();
    }

    public void pgbSetValue(double pValue ){
        System.out.print(""+pgb.getValue());
        pgb.setValue(pValue);
        System.out.println(":"+pgb.getValue());
    }

    public void pgbDecrement(int pValue){
        pgb.decrement(pValue);
    }

    public void pgbIncrement(int pValue){
        pgb.increment(pValue);
    }

    public void listViewSelect(int pIndex){
        lst.select(pIndex);
    }

    public void listDeselect(int pIndex){
        lst.deselect(pIndex);
    }

    public void listDeselectAll(){
        lst.deselectAll();
    }

    public boolean listIsSelected(int pIndex){
        return lst.isSelected(pIndex);
    }

    public void listViewAction(){
        int[]lSelectedIndices = lst.getSelectedIndices();
        System.out.println("getSelectedIndices");
        for(int i: lSelectedIndices){
            System.out.print(i+";");
        }
        System.out.println("");
        int lSelectedIndex = lst.getSelectedIndex();
        System.out.println("getSelectedIndex: "+lSelectedIndex);
        String[] lSelectedItems = lst.getSelectedItems();
        System.out.println("getSelectedItems");
        for(String s : lSelectedItems){
            System.out.print(s+";");
        }
        System.out.println("");
        String lSelectedItem = lst.getSelectedItem();
        System.out.println("getSelectedItem: "+lSelectedItem);
        String[] lAllItems = lst.getAllItems();
        System.out.println("getAllItems");
        for(String s : lAllItems){
            System.out.print(s+";");
        }
        System.out.println("");
        System.out.println("#################################");
        System.out.println("#################################");
    }

    public void spinnerAction(){
        lbl.setText(spn.getValueAsInteger()+" - - "+spn.getValueAsDouble());
        spn.increment(4);

    }

    public void sliderAction(){
        sld.showTickMarks();
        sld.showTickLabels();

    }

    public void textFieldAction(){
        //txt.setContent("Ich wurde geändert");
        System.out.println(txt.getSelectedText());
        System.out.println(txt.getSelectionStart()+":"+txt.getSelectionEnd());

    }

    public void textFieldMarkierungSetzen(){
        //txt.selectText(2,4);
        System.out.println("Markierung im Textfeld setzen");
    }

    public void abc(){
        System.out.println("Wurde geklickt");
       // sld.setValue(90);
        pwd.setWidth(20);

    }

    public void abc2(){
        System.out.println("1234rer");
        btn.setText("Geändert");
    }

    public void a1(){
        System.out.println("1");
    }

    public void a2(){
        System.out.println("2");
    }

}
