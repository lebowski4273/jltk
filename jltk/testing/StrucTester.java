package jltk.testing;
import jltk.struc.*;
import java.util.Random; 

/**
 * Beschreiben Sie hier die Klasse StrucTester.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class StrucTester
{
    Queue queue1;
    List list1;
    Random rand;
    public StrucTester()
    {
       this.rand = new Random();
    }

    
    public void generateQueue(int pAmountElement){
        queue1 = new Queue<String>();
        for(int i=0; i<pAmountElement;i++){
            queue1.enqueue( ((char)(rand.nextInt(26) + 65))+"" );
        }
        System.out.println(this.queue1.toString());
        //queue1.seperator = "\n";
        //System.out.println(this.queue1.toString());
    }
    
    public void dequeue(){
        this.queue1.dequeue();
        System.out.println(this.queue1.toString());
    }
    
    public boolean isEmpty(){
        return this.queue1.isEmpty();
    }
    
    public void generateList(int pAmountElement){
        list1 = new List<String>();
        for(int i=0; i<pAmountElement;i++){
            list1.append( ((char)(rand.nextInt(26) + 65))+"" );
        }
        System.out.println(this.list1.toString()+"\nCurrent: "+list1.getContent()+" Last: "+this.list1.getLast().toString());
    }
    
    public void listRemove(){
        list1.remove();
        System.out.println(this.list1.toString()+"\nCurrent: "+list1.getContent()+" Last: "+this.list1.getLast());
    }
    
    public void listAppend(String pString){
        list1.append(pString);
        System.out.println(this.list1.toString()+"\nCurrent: "+list1.getContent()+" Last: "+this.list1.getLast().toString());
    }
    
    public void listNext(){
        list1.next();
        System.out.println(this.list1.toString()+"\nCurrent: "+list1.getContent()+" Last: "+this.list1.getLast().toString());
    }
    
     public void listInsert(String pString){
        list1.insert(pString);
        System.out.println(this.list1.toString()+"\nCurrent: "+list1.getContent()+" Last: "+this.list1.getLast().toString());
    }
    
     public void listToFirst(){
        list1.toFirst();
        System.out.println(this.list1.toString()+"\nCurrent: "+list1.getContent()+" Last: "+this.list1.getLast().toString());
    }
    
     public void listToLast(){
        list1.toLast();
        System.out.println(this.list1.toString()+"\nCurrent: "+list1.getContent()+" Last: "+this.list1.getLast().toString());
    }
    
}
