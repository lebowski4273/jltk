package jltk.testing;
import jltk.*;
public class TabellenTest extends App
{
    jltk.gui.Table tbl;
    /**
     * Konstruktor für Objekte der Klasse TabellenTest
     */
    public TabellenTest()
    {
        super(600,600);
        tbl = new jltk.gui.Table(20,20,500,550);
        tbl.setOnCellSelected("eventSelected");
        tbl.setOnCellEdited("eventEdited");
        tbl.setOnContentChanged("eventChanged");
        tbl.appendColumn("a");
        tbl.appendColumn("b");
        String[] a = {"1","2"};
        tbl.appendRow(a);
    }

    // Selction
    public void select(int pRow, int pColumn){
        tbl.select(pRow, pColumn);
    }

    public void select(int pRow){
        tbl.select(pRow);
    }

    public void selectAll(){
        tbl.selectAll();
    }

    public void deselect(int pRow, int pColumn){
        tbl.deselect(pRow, pColumn);
    }

    public void deselect(int pRow){
        tbl.deselect(pRow);
    }

    public void deselectAll(){
        tbl.deselectAll();
    }

    public void disableSelection(){
        tbl.disableSelection();
    }

    public void enableMultiSelection(){
        tbl.enableMultiSelection();
    }

    public void enableSingleSelection(){
        tbl.enableSingleSelection();
    }

    public void enableCellSelection(){
        tbl.enableCellSelection();
    }

    public void enableRowSelection(){
        tbl.enableRowSelection();
    }

    // get data
    public String[] getRow(int pIndex){
        return tbl.getRow(pIndex);
    }

    public String[] getColumn(int pIndex){
        return tbl.getColumn(pIndex);
    }

    public String getItem(int pRow, int pColumn){
        return tbl.getItem(pRow, pColumn);
    }

    // whole data
    public String[][] getItems(){
        return tbl.getItems();
    }

    // change data
    public void setItem(int pRow, int pColumn, String pText){
        tbl.setItem(pRow, pColumn, pText);
    }
    
    public void setItems(){
        String[][] c = {{"1","2","3","4"},{"5","6","7","8"},{"9","10","11","12"},{"13","14","15","16"},{"17","18","19","20"}};
        tbl.setItems(c);
    }
    
    public void setItems(String[] pTitle){
        String[][] c = {{"1","2","3","4"},{"5","6","7","8"},{"9","10","11","12"},{"13","14","15","16"},{"17","18","19","20"}};
        tbl.setItems(c,pTitle);
    }

    // columns
    public void appendColumn(String pText){
        tbl.appendColumn(pText);
    }

    public void appendColumn(String pText, String[] pContent){
        tbl.appendColumn(pText,pContent);
    }

    public void insertColumn(int pIndex, String pText){
        tbl.insertColumn(pIndex, pText);
    }

    public void insertColumn(int pIndex, String pText, String[] pContent){
        tbl.insertColumn(pIndex, pText, pContent);
    }

    public void removeColumns(int pIndex){
        tbl.removeColumn(pIndex);
    }
    
    public void createColumns(int pNumber){
        tbl.createColumns(pNumber);
    }
    
    public void createColumns(String[] pTitle){
        tbl.createColumns(pTitle);
    }
    
    public void createColumn(int pIndex, String pTitle){
        tbl.setColumnTitle(pIndex,pTitle);
    }

    //rows
    public void appendRow(){
        tbl.appendRow();
    }

    public void appendRow(Object[] pContent){
        tbl.appendRow(pContent);
    }

    public void insertRow(int pIndex, Object[] pContent){
        tbl.insertRow(pIndex,pContent);
    }

    public void insertRow(int pIndex){
        tbl.insertRow(pIndex);
    }

    public void removeRow(int pIndex){
        tbl.removeRow(pIndex);
    }

     
    
    // get selections
    public int getSelectedRowIndex(){
        return tbl.getSelectedRowIndex();
    }

    public int getSelectedColumnIndex(){
        return tbl.getSelectedColumnIndex();
    }

    public int[] getSelectedRowsIndices(){
        return tbl.getSelectedRowsIndices();
    }

    public int[][] getSelectedCellsIndices(){
        return tbl.getSelectedCellsIndices();
    }

    public String[] getSelectedRow(){
        return tbl.getSelectedRow();
    }

    public String[][] getSelectedRows(){
        return tbl.getSelectedRows();
    }

    public String[] getSelectedColumn(){
        return tbl.getSelectedColumn();
    }

    public String[][] getSelectedColumns(){
        return tbl.getSelectedColumns();
    }

    public String[] getSelectedItems(){
        return tbl.getSelectedItems();
    }

    public String getSelectedItem(){
        return tbl.getSelectedItem();
    }

    // test selections
    public boolean isSelected(int pRow){
        return tbl.isSelected(pRow);
    }

    public boolean isSelected(int pRow, int pColumn){
        return tbl.isSelected(pRow, pColumn);
    }

    //Eventhandler
    public void eventSelected(){
        System.out.println(tbl.getSelectedRowIndex()+"-"+tbl.getSelectedColumnIndex());
    }

    public void eventEdited(){
        System.out.println("Geändert");
    }

    public void eventChanged(){
        System.out.println("Daten geändert");
    }

    // Edit
    public void enableEditable(int pRow){
        tbl.enableEditable(pRow);
    }

    public void enableEditable(){
        tbl.enableEditable();
    }

    public void disableEditable(){
        tbl.disableEditable(); 
    }
    
    // refresh
    public void refresh(){
        tbl.refresh();
    }
    
    // clear
    public void clear(){
        tbl.clearAll();
    }
    
    
    public void autosize(){
        tbl.autosize();
    }

}
