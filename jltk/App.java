package jltk;



/**
 * The class App is a prototype of an eventhandler, which handles basic mouse and keyboard events. It also 
 * provides a window (object of class Window), on which you can draw with an Pen or where you can place
 * gui components like button, textfields, ... <br/>
 * To use this class you have to create a subclass of it, like
 *        <center><code>public class myClass extends App{ ...}</code></center><br/>
 * For each event (mouse moved, mouse pressed, et cetera) there is an method (eventhandler), which will be
 * called, if this event occurs. E.g. the method <i>onMouseMoved(int pX, int pY)</i> is called, if the mouse moved.
 * The Parameter will be set to the current x- and y-coordinates. In your subclass you have to overwrite this
 * method and fit it to your needs.<br/>
 * <br/>
 * To use gui-components like buttons or textfields, you only have to create an object of the corresponding class
 * and pass the coordinates and size to the constructor. Remember, the origin of the coorinate system is the 
 * upper left corner of the window. The x-axis points to the right, the y-axis points down.<br/>
 * For example: <code>btn = new Button(60,80,100,25,"Hello World")</code> creates a button with width 100 and 
 * height 25. The upper left corner of the button has the coordinates (60/80). The button is labled with "Hello
 * World". Dont forget to import <i>jltk.gui.*</i>.
 * To handle a button pressed event, you can define a method, which will be called, if the button is pressed. 
 * For example: If you add
 * <br/>
 *         <center><code>btn.onButtonClicked("doSomething");</code></center><br/>
 * the method <i>doSomething()</i> is called, every time the button is clicked. Of course, you have to implement the method 
 * <i>doSomething()</i> in your subclass of App.<br/>
 * There are a lot of gui components, check the package jltk.gui for all components.<br/>
 * <br/>
 * Mention: The can be only on subclass of App in each project.
 * 
 * 
 * @author Martin Schulte
 * @version 12.06.2022
 */
public class App 
{
    public Window window;
    private Thread jltkThread;
    public static App theApp; 
    private boolean running = false;

    public App(int pWidth, int pHeight)
    {
        theApp = this;
        this.window = new Window(pWidth, pHeight);
        this.window.setAppInstance(this);
    }

    /**
     * Return the window object which belongs to this app instance. You also can use this.window
     * to get this object.
     *
     * @return The window object.
     */
    public Window getWindow(){
        return this.window;
    }

    // Events. This methods will be invoked, if the corresponding event occurs.
    // Overwrite this methods in your subclass of App

    /**
     * Will be called, if the mouse was moved. You have to overwrite this method in your subclass
     *
     * @param pX The x-coordinate of the mouse position
     * @param pY The y-coordinate of the mouse position
     */
    public void onMouseMoved(int pX, int pY){
    }

    /**
     * Will be called, if the mouse was clicked. You have to overwrite this method in your subclass
     *
     * @param pX The x-coordinate of the mouse position
     * @param pY The y-coordinate of the mouse position
     */
    public void onMouseClicked(int pX, int pY){
    }

    /**
     * Will be called, if the mouse was pressed. You have to overwrite this method in your subclass
     *
     * @param pX The x-coordinate of the mouse position
     * @param pY The y-coordinate of the mouse position
     */
    public void onMousePressed(int pX, int pY){
    }

    /**
     * Will be called, if the mouse was released. You have to overwrite this method in your subclass
     *
     * @param pX The x-coordinate of the mouse position
     * @param pY The y-coordinate of the mouse position
     */
    public void onMouseReleased(int pX, int pY){
    }

    /**
     * Will be called, if the mouse was dragged (pressed and moved). You have to overwrite this method in your subclass
     *
     * @param pX The x-coordinate of the mouse position
     * @param pY The y-coordinate of the mouse position
     */
    public void onMouseDragged(int pX, int pY){
    }
    
    /**
     * Will be called, if the mouse was double clicked. You have to overwrite this method in your subclass
     *
     * @param pX The x-coordinate of the mouse position
     * @param pY The y-coordinate of the mouse position
     */
    public void onMouseDoubleClicked(int pX, int pY){
    }

    /**
     * Will be called, if a key on the keyboard was pressed. The parameter pKey is the corresponding
     * key name. 
     * <p>The following key names are recognized:<p>
     * <ul>
     *    <li> a-z</li>
     *    <li> 0-1</li>
     *    <li> up, down, left, right (cursor keys)</li>
     *    <li> enter, space, tab, escape, backspace</li>
     *    <li> F1-F12 (function keys)</li>
     * </ul>
     * <p>You have to overwrite this method in your subclass.</p>
     *
     * @param pKey The name of the pressed key
     */
    public void onKeyPressed(String pKey){
    }

    /**
     * Will be called, if a key on the keyboard was released. The parameter pKey is the corresponding
     * key name
     * <p>The following key names are recognized:<p>
     * <ul>
     *    <li> a-z</li>
     *    <li> 0-1</li>
     *    <li> up, down, left, right (cursor keys)</li>
     *    <li> enter, space, tab, escape, backspace</li>
     *    <li> F1-F12 (function keys)</li>
     * </ul>
     * <p>You have to overwrite this method in your subclass.</p>
     *
     * @param pKey The name of the pressed key
     */
    public void onKeyReleased(String pKey){
    }

    /**
     * This method is called, every time no other eventhandler is called (on idle). You have to overwrite
     * this method in your subclass.
     *
     */
    public void act(){

    }

    /**
     * This method is called if the window of this app get the focus. You have to overwrite
     * this method in your subclass.
     *
     */
    public void onFocusGained(){

    }

    /**
     * This method is called if the window of this app lost the focus. You have to overwrite
     * this method in your subclass.
     *
     */
    public void onFocusLost(){

    }

     /**
     * Delays the execution by the specified amount of milliseconds.
     *
     * @param pMilliseconds The duration of delay in milliseconds
     */
    public void delay(int pMilliseconds){
        try {
            Thread.sleep(pMilliseconds);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
        }
    }

}
